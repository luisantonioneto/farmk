package br.com.farmk.controle;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.primefaces.event.SelectEvent;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.annotation.SessionScope;

import br.com.farmk.entidade.UnidadeMedida;
import br.com.farmk.persistencia.UnidadeMedidaDao;
import br.com.farmk.util.UtilFaces;

@Controller
@SessionScope
public class UnidadeMedidaControl implements Crud, Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private UnidadeMedidaDao unidadeMedidaDao;
	private UnidadeMedida unidadeMedida = new UnidadeMedida();
	private UnidadeMedida unSelecionada = new UnidadeMedida();
	private List<UnidadeMedida> unidadeMedidas = new ArrayList<UnidadeMedida>();
	
	@Override
	@PostConstruct
	public void carregar(){
		try {
			unidadeMedida = new UnidadeMedida();
			unidadeMedidas = new ArrayList<UnidadeMedida>();
			unidadeMedidas = unidadeMedidaDao.listarPorNome();
		} catch (Exception e) {
			UtilFaces.falhaAoCarregar(e);
		}
	}
	
	@Override
	public void salvar(){
		try {
			if (unidadeMedida.getCodUnidadeMedida() == 0) {
				unidadeMedidaDao.salvar(unidadeMedida);
			} else {
				unidadeMedidaDao.atualizar(unidadeMedida);
			}
			UtilFaces.salvoComSucesso("Unidade Medida");
		} catch (Exception e) {
			UtilFaces.falhaAoSalvar(e);
		} finally {
			carregar();
		}
	}

	@Override
	public void excluir() {
		try {
			unidadeMedidaDao.excluir(unidadeMedida);
			UtilFaces.excluidoComSucesso();
		} catch (Exception e) {
			UtilFaces.falhaAoExcluir(e);
		} finally {
			carregar();
		}
	}

	@Override
	public void novo() {
		unidadeMedida = new UnidadeMedida();
	}

	@Override
	public void selecionarLinha(SelectEvent event) {
		unidadeMedida = (UnidadeMedida) event.getObject();
	}

	public UnidadeMedida getUnidadeMedida() {
		return unidadeMedida;
	}

	public void setUnidadeMedida(UnidadeMedida unidadeMedida) {
		this.unidadeMedida = unidadeMedida;
	}

	public UnidadeMedida getUnSelecionada() {
		return unSelecionada;
	}

	public void setUnSelecionada(UnidadeMedida unSelecionada) {
		this.unSelecionada = unSelecionada;
	}

	public List<UnidadeMedida> getUnidadeMedidas() {
		return unidadeMedidas;
	}
}
