package br.com.farmk.controle;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.primefaces.event.SelectEvent;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.annotation.SessionScope;

import br.com.farmk.entidade.PerfilUsuario;
import br.com.farmk.entidade.Permissao;
import br.com.farmk.entidade.Regra;
import br.com.farmk.persistencia.PermissaoDao;
import br.com.farmk.util.UtilFaces;

@Controller
@SessionScope
public class PermissaoControl implements Crud, Serializable{

	private static final long serialVersionUID = 1L;
	
	@Inject
	private PermissaoDao permissaoDao;
	private Permissao permissao = new Permissao();
	private Permissao permissaoSelecionada = new Permissao();
	private List<Permissao> permissoes = new ArrayList<Permissao>();
	
	private List<Regra> regras = new ArrayList<Regra>();
	
	@Override
	@PostConstruct
	public void carregar(){
		try {
			permissoes = new ArrayList<Permissao>();
			permissoes = permissaoDao.listar();
			permissao = new Permissao();
			
			regras = new ArrayList<Regra>();
		} catch (Exception e) {
			UtilFaces.mostrarExcecao("Falha ao carregar permissões", e);
		}
	}
	
	public void salvar(PerfilUsuario perfil){
		for (Regra regra : regras) {
			try {
				permissao = permissaoDao.buscarPorPerfilERegra(perfil, regra);
				if (permissao == null) {
					permissao = new Permissao();
					permissao.setPerfilUsuario(perfil);
					permissao.setRegra(regra);
					salvar();					
				}
			} catch (Exception e) {
				UtilFaces.falhaAoCarregar(e);
			} finally{
				carregar();
			}
		}
	}
	
	@Override
	public void salvar(){
		try {
			if (permissao.getCodPermissao() == 0) {
				permissaoDao.salvar(permissao);				
			} else {
				permissaoDao.atualizar(permissao);
			}
			UtilFaces.salvoComSucesso("Permissão");
		} catch (Exception e) {
			UtilFaces.mostrarExcecao("Falha ao salvar permissão", e);
		} 
	}
	
	@Override
	public void excluir(){
		try {
			permissaoDao.excluir(permissao);
			UtilFaces.excluidoComSucesso("Permissão");
		} catch (Exception e) {
			UtilFaces.falhaAoExcluir(e);
		} finally{
			carregar();
		}
	}
	
	public void excluir(Permissao permissao){
		try {
			permissaoDao.excluir(permissao);
			UtilFaces.excluidoComSucesso();
		} catch (Exception e) {
			UtilFaces.falhaAoExcluir(e);
		} finally{
			carregar();
		}
	}
	
	@Override
	public void novo() {
		carregar();
	}

	@Override
	public void selecionarLinha(SelectEvent event) {
		permissao = (Permissao) event.getObject();
	}	

	public void removerPermissao(Permissao permissaoSelecionada) throws Exception{
		if ((permissaoSelecionada != null) && (permissaoSelecionada.getCodPermissao() != 0)) {
			permissaoDao.excluir(permissaoSelecionada);
			UtilFaces.excluidoComSucesso();
		}
		permissoes.remove(permissaoSelecionada);
	}

	public Permissao getPermissao() {
		return permissao;
	}

	public void setPermissao(Permissao permissao) {
		this.permissao = permissao;
	}

	public List<Permissao> getPermissoes() {
		return permissoes;
	}

	public void setPermissoes(List<Permissao> permissoes) {
		this.permissoes = permissoes;
	}

	public Permissao getPermissaoSelecionada() {
		return permissaoSelecionada;
	}

	public void setPermissaoSelecionada(Permissao permissaoSelecionada) {
		this.permissaoSelecionada = null;
		try {
			removerPermissao(permissaoSelecionada);
		} catch (Exception e) {
			UtilFaces.mostrarExcecao("Falha ao remover permissão", e);
		}
	}

	public List<Regra> getRegras() {
		return regras;
	}

	public void setRegras(List<Regra> regras) {
		this.regras = regras;
	}
}