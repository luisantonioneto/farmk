package br.com.farmk.controle;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.primefaces.event.SelectEvent;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.annotation.SessionScope;

import br.com.farmk.entidade.Regra;
import br.com.farmk.persistencia.RegraDao;
import br.com.farmk.util.UtilFaces;

@Controller
@SessionScope
public class RegraControl implements Crud, Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private RegraDao regraDao;
	private Regra regra = new Regra();
	private Regra regraSelecionada = new Regra();
	private List<Regra> regras = new ArrayList<Regra>();
	private List<Regra> regrasSelecionadas = new ArrayList<Regra>();

	@Override
	@PostConstruct
	public void carregar() {
		try {
			regras = new ArrayList<Regra>();
			regras = regraDao.listarPorNome();
			regra = new Regra();
		} catch (Exception e) {
			UtilFaces.falhaAoCarregar(e);
		}
	}

	@Override
	public void salvar() {
		try {
			if (regra.getCodRegra() == 0) {
				regraDao.salvar(regra);
			} else {
				regraDao.atualizar(regra);
			}
			UtilFaces.salvoComSucesso("Regra");
		} catch (Exception e) {
			UtilFaces.falhaAoSalvar(e);
		} finally {
			carregar();
		}
	}

	@Override
	public void excluir() {
		try {
			regraDao.excluir(regra);
			UtilFaces.excluidoComSucesso();
		} catch (Exception e) {
			UtilFaces.falhaAoExcluir(e);
		}
	}

	@Override
	public void novo() {
		carregar();
	}

	@Override
	public void selecionarLinha(SelectEvent event) {
		regra = (Regra) event.getObject();
	}

	public Regra getRegra() {
		return regra;
	}

	public void setRegra(Regra regra) {
		this.regra = regra;
	}
	
	public Regra getRegraSelecionada() {
		return regraSelecionada;
	}

	public void setRegraSelecionada(Regra regraSelecionada) {
		this.regraSelecionada = regraSelecionada;
	}

	public List<Regra> getRegrasSelecionadas() {
		return regrasSelecionadas;
	}

	public void setRegrasSelecionadas(List<Regra> regrasSelecionadas) {
		this.regrasSelecionadas = regrasSelecionadas;
	}

	public List<Regra> getRegras() {
		return regras;
	}

	public void setRegras(List<Regra> regras) {
		this.regras = regras;
	}
}
