package br.com.farmk.controle;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.primefaces.event.SelectEvent;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.annotation.SessionScope;

import br.com.farmk.entidade.Tecnologia;
import br.com.farmk.persistencia.TecnologiaDao;
import br.com.farmk.util.UtilFaces;

@Controller
@SessionScope
public class TipoAreaControl implements Crud, Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private TecnologiaDao tipoAreaDao;
	private Tecnologia tipoArea = new Tecnologia();
	private Tecnologia tipoAreaSelecionada = new Tecnologia();
	private List<Tecnologia> tipoAreas = new ArrayList<Tecnologia>();

	@Override
	@PostConstruct
	public void carregar() {
		try {
			tipoAreas = new ArrayList<Tecnologia>();
			tipoAreas = tipoAreaDao.listarPorNome();
			tipoArea = new Tecnologia();
		} catch (Exception e) {
			UtilFaces.falhaAoCarregar(e);
		}
	}

	@Override
	public void salvar() {
		try {
			if (tipoArea.getCodTecnologia() == 0) {
				tipoAreaDao.salvar(tipoArea);
			} else {
				tipoAreaDao.atualizar(tipoArea);
			}
			UtilFaces.salvoComSucesso("Tipo Área");
		} catch (Exception e) {
			UtilFaces.falhaAoSalvar(e);
		} finally {
			carregar();
		}
	}

	@Override
	public void excluir() {
		try {
			tipoAreaDao.excluir(tipoArea);
			UtilFaces.excluidoComSucesso();
		} catch (Exception e) {
			UtilFaces.falhaAoExcluir(e);
		} finally {
			carregar();
		}
	}

	@Override
	public void novo() {
		carregar();
	}

	@Override
	public void selecionarLinha(SelectEvent event) {
		tipoArea = (Tecnologia) event.getObject();
	}

	public Tecnologia getTipoArea() {
		return tipoArea;
	}

	public void setTipoArea(Tecnologia tipoArea) {
		this.tipoArea = tipoArea;
	}

	public Tecnologia getTipoAreaSelecionada() {
		return tipoAreaSelecionada;
	}

	public void setTipoAreaSelecionada(Tecnologia tipoAreaSelecionada) {
		this.tipoAreaSelecionada = tipoAreaSelecionada;
	}

	public List<Tecnologia> getTipoAreas() {
		return tipoAreas;
	}

	public void setTipoAreas(List<Tecnologia> tipoAreas) {
		this.tipoAreas = tipoAreas;
	}
}