package br.com.farmk.controle;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.annotation.SessionScope;

import br.com.farmk.entidade.Empresa;
import br.com.farmk.entidade.EmpresasPermitidas;
import br.com.farmk.entidade.Pessoa;
import br.com.farmk.entidade.Usuario;
import br.com.farmk.persistencia.EmpresaDao;
import br.com.farmk.persistencia.EmpresasPermitidasDao;
import br.com.farmk.persistencia.UsuarioDao;
import br.com.farmk.util.UtilFaces;
import br.com.farmk.util.UtilUsuario;

@Controller
@SessionScope
public class UsuarioControl implements Crud, Serializable {

	private static final long serialVersionUID = 1L;
	
    @Autowired
    private PasswordEncoder passwordEncoder;	
	
	@Inject
	private UsuarioDao usuarioDao;
	private Usuario usuario = new Usuario();
	private Usuario usuarioLogado = new Usuario();
	private Usuario usuarioSelecionado = new Usuario();
	private List<Usuario> usuarios = new ArrayList<Usuario>();
	
	private Pessoa pessoa = new Pessoa();
	
	@Inject
	private EmpresaDao empresaDao;
	private Empresa empresa = new Empresa();
	private Empresa empresaSelecionada = new Empresa();
	private List<Empresa> empresas = new ArrayList<Empresa>();
	private List<Empresa> empresasSelecionadas = new ArrayList<Empresa>();
	
	@Inject
	private EmpresasPermitidasDao empresasPermitidasDao;
	
	@Inject
	private UtilUsuario utilUsuario;
	
	private String senhaCriptografada = "";
	
	@Override
	@PostConstruct
	public void carregar(){
		try {			
			usuarios = new ArrayList<Usuario>();
			usuarios = usuarioDao.listar();
			
			usuarioLogado = utilUsuario.pegarUsuarioLogado();
			
			empresaSelecionada = new Empresa();
			empresas = new ArrayList<Empresa>();
			
			empresasSelecionadas = new ArrayList<Empresa>();
		} catch (Exception e) {
			UtilFaces.falhaAoCarregar(e);
		}
	}
	
	@Transactional(value=TxType.REQUIRES_NEW)
	public void salvar(Pessoa pessoa){
		this.pessoa = pessoa;
		usuario.setPessoa(pessoa);
		salvar();
	}
	
	@Override
	public void salvar(){
		if (pessoa.getCodPessoa() == 0) {
			if (usuario.getPessoa() != null) {
				pessoa = usuario.getPessoa();				
			}
		}
		if (pessoa.getCodPessoa() != 0){
			try {
				Usuario usuarioAInserir = new Usuario();
				usuarioAInserir = usuarioDao.consultarLoginExiste(usuario);
				if (usuario.getSenha() != "") {
					senhaCriptografada = passwordEncoder.encode(usuario.getSenha());
				}
				usuario.setSenha(senhaCriptografada);
				if(usuarioAInserir == null){
					if(usuario.isAtivo() && usuario.getLogin().trim() != ""){
						usuarioDao.salvar(usuario);
						UtilFaces.salvoComSucesso("Usuário");
					} else{
						if(usuario.getCodUsuario() != 0){
							usuarioDao.salvar(usuario);					
						}
					}				
				} else{
					try {
						usuarioDao.atualizar(usuario);
						UtilFaces.mostrarMensagem("O login foi atualizado");
					} catch (Exception e) {
						UtilFaces.mostrarExcecao("Falha ao atualizar usuário", e);
					}
				}
				
				// Incluir EmpresasPermitidas
				if (empresasSelecionadas != null) {
					for (Empresa empresa : empresasSelecionadas) {
						EmpresasPermitidas empresaPermitida = empresasPermitidasDao.buscarPorEmpresaEUsuario(empresa, usuario);
						if (empresaPermitida == null) {
							empresaPermitida = new EmpresasPermitidas();
							empresaPermitida.setEmpresa(empresa);
							empresaPermitida.setUsuario(usuario);
							empresasPermitidasDao.salvar(empresaPermitida);							
						}
					}					
				}
				
				// Remover Empresas desmarcadas
				if (usuario.getCodUsuario() != 0) {
					List<Empresa> empresasRemover = empresasPermitidasDao.listarEmpresasPorUsuario(usuario);
					if (empresasSelecionadas != null) {
						empresasRemover.removeAll(empresasSelecionadas);
					}
					
					for (Empresa empresa : empresasRemover) {
						EmpresasPermitidas empresaPermitida = empresasPermitidasDao.buscarPorEmpresaEUsuario(empresa, usuario);
						empresasPermitidasDao.excluir(empresaPermitida);
					}
				}
			} catch (Exception e) {
				UtilFaces.mostrarExcecao("Falha ao salvar usuário", e);
			} finally{
				carregar();
			}
		} else {
			UtilFaces.mostrarAlerta("Informe a pessoa !");
		}
	}
	
	@Override
	public void excluir(){
		try {
			usuarioDao.excluir(usuario);
			UtilFaces.excluidoComSucesso();
		} catch (Exception e) {
			UtilFaces.falhaAoExcluir(e);
		} finally{
			usuario = new Usuario();
			carregar();
		}
	}
	
	@Override
	public void novo(){
		usuario = new Usuario();
		usuario.setAtivo(true);
		pessoa = new Pessoa();	
		
		carregar();
	}
	
	@Override
	public void selecionarLinha(SelectEvent event){
		try {
			usuario = (Usuario) event.getObject();
			//
			empresasSelecionadas = new ArrayList<Empresa>();
			List<EmpresasPermitidas> empresasPermitidas = empresasPermitidasDao.listarPorUsuario(usuario); 
			for (EmpresasPermitidas empresaPermitida : empresasPermitidas) {
				empresasSelecionadas.add(empresaPermitida.getEmpresa());
			}
		} catch (Exception e) {
			UtilFaces.falhaAoCarregar(e);
		}
	}
	
	public void itemSelectPessoa(SelectEvent event){
		pessoa = new Pessoa();
		usuario = new Usuario();
		pessoa = (Pessoa)event.getObject();
		try {
			usuario = usuarioDao.buscarPorObjeto(pessoa);
			if (usuario == null) {
				usuario = new Usuario();
			}
		} catch (Exception e) {
			UtilFaces.mostrarExcecao("Não foi possível buscar o usuário", e);
		}
	}
	
	public List<Empresa> buscarEmpresa(String nome) {
		try {
			empresas = new ArrayList<Empresa>();
			empresas = empresaDao.listarPorRazaoSocialOuFantasia(nome, false);
			
			List<Empresa> empresasPermitidas = utilUsuario.pegarEmpresasPermitidas();
			List<Empresa> empresasFinal = new ArrayList<Empresa>();
			
			for (Empresa empresa : empresas) {
				if (empresasPermitidas.contains(empresa)) {
					empresasFinal.add(empresa);
				}
			}
			
			return empresasFinal;
		} catch (Exception e) {
			UtilFaces.falhaAoConsultar(e);
			return null;
		}
	}
	
	public void selecionarEmpresa(SelectEvent event){
		try {
			empresa = (Empresa) event.getObject();
			if (!empresasSelecionadas.contains(empresa)) {
				empresasSelecionadas.add(empresa);				
			}
		} catch (Exception e) {
			UtilFaces.mostrarExcecao(e);
		}
	}
	
	public void buscarPorPessoa(Pessoa pessoa){
		try {
			if ((pessoa != null) && (pessoa.getCodPessoa() != 0)) {
				usuario = usuarioDao.buscarPorObjeto(pessoa);
				if (usuario == null) {
					usuario = new Usuario();
				} else {
					senhaCriptografada = usuario.getSenha();					
				}
			}
		} catch (Exception e) {
			UtilFaces.mostrarExcecao("Falha ao buscar Usuário", e);
		}
	}
	
	/*
	 * gets e sets
	 */

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public List<Usuario> getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public Usuario getUsuarioSelecionado() {
		return usuarioSelecionado;
	}

	public void setUsuarioSelecionado(Usuario usuarioSelecionado) {
		this.usuarioSelecionado = usuarioSelecionado;
	}

	public Empresa getEmpresaSelecionada() {
		return empresaSelecionada;
	}

	public void setEmpresaSelecionada(Empresa empresaSelecionada) {
		this.empresaSelecionada = empresaSelecionada;
	}

	public List<Empresa> getEmpresas() {
		return empresas;
	}

	public void setEmpresas(List<Empresa> empresas) {
		this.empresas = empresas;
	}

	public List<Empresa> getEmpresasSelecionadas() {
		return empresasSelecionadas;
	}

	public void setEmpresasSelecionadas(List<Empresa> empresasSelecionadas) {
		this.empresasSelecionadas = empresasSelecionadas;
	}

	public Usuario getUsuarioLogado() {
		return usuarioLogado;
	}

	public void setUsuarioLogado(Usuario usuarioLogado) {
		this.usuarioLogado = usuarioLogado;
	}
}
