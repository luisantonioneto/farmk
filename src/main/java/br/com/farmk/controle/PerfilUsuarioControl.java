package br.com.farmk.controle;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.primefaces.event.SelectEvent;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.annotation.SessionScope;

import br.com.farmk.entidade.PerfilUsuario;
import br.com.farmk.entidade.Permissao;
import br.com.farmk.entidade.Regra;
import br.com.farmk.persistencia.PerfilUsuarioDao;
import br.com.farmk.persistencia.PermissaoDao;
import br.com.farmk.util.UtilFaces;

@Controller
@SessionScope
public class PerfilUsuarioControl implements Crud, Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private PerfilUsuarioDao perfilUsuarioDao;
	private PerfilUsuario perfilUsuario = new PerfilUsuario();
	private PerfilUsuario perfilSelecionado = new PerfilUsuario();
	private List<PerfilUsuario> perfisUsuarios = new ArrayList<PerfilUsuario>();
	
	@Inject
	private PermissaoDao permissaoDao;
	private List<Permissao> permissoes = new ArrayList<Permissao>();
	
	private List<Regra> regras = new ArrayList<Regra>();
	private List<Regra> regrasSelecionadas = new ArrayList<Regra>();

	@Override
	@PostConstruct
	public void carregar() {
		try {
			perfilUsuario = new PerfilUsuario();
			perfisUsuarios = new ArrayList<PerfilUsuario>();
			perfisUsuarios = perfilUsuarioDao.listarPorNome();
			
			regras = new ArrayList<Regra>();
			regrasSelecionadas = new ArrayList<Regra>();
		} catch (Exception e) {
			UtilFaces.falhaAoCarregar(e);
		}
	}

	@Override
	public void salvar() {
		try {
			if (perfilUsuario.getNome().trim() != "") {
				if (perfilUsuario.getCodPerfilUsuario() == 0) {
					perfilUsuarioDao.salvar(perfilUsuario);
				} else {
					perfilUsuarioDao.atualizar(perfilUsuario);
				}
				
				// Incluir Permissoes
				for (Regra regra : regrasSelecionadas) {
					Permissao permissao = permissaoDao.buscarPorPerfilERegra(perfilUsuario, regra);
					if (permissao == null) {
						permissao = new Permissao();
						permissao.setPerfilUsuario(perfilUsuario);
						permissao.setRegra(regra);
						permissaoDao.salvar(permissao);
					}
				}
				
				// Remover Permissoes que foram desmarcadas
				permissoes = permissaoDao.listar(perfilUsuario);
				regras = permissaoDao.listarRegrasPorPerfil(perfilUsuario);
				regras.removeAll(regrasSelecionadas);
				
				for (Regra regra : regras) {
					Permissao permissao = permissaoDao.buscarPorPerfilERegra(perfilUsuario, regra);
					if (permissao != null) {
						permissaoDao.excluir(permissao);
					}
				}
				
				UtilFaces.salvoComSucesso("Perfil Usuário");				
			} else {
				UtilFaces.mostrarAlerta("Informe o nome do perfil");
			}
		} catch (Exception e) {
			UtilFaces.falhaAoSalvar(e);
		} finally {
			carregar();
		}
	}

	@Override
	public void excluir() {
		try {
			perfilUsuarioDao.excluir(perfilUsuario);
			UtilFaces.excluidoComSucesso("Perfil Usuário");
		} catch (Exception e) {
			UtilFaces.falhaAoExcluir(e);
		} finally {
			carregar();
		}
	}

	@Override
	public void novo() {
		carregar();
	}

	@Override
	public void selecionarLinha(SelectEvent event) {
		try {
			regrasSelecionadas = new ArrayList<Regra>();
			perfilUsuario = (PerfilUsuario) event.getObject();
			permissoes = permissaoDao.listar(perfilUsuario);
			for (Permissao permissao : permissoes) {
				regrasSelecionadas.add(permissao.getRegra());
			}
		} catch (Exception e) {
			UtilFaces.falhaAoConsultar(e);
		}		
	}
	
	/* 
	 * gets e sets
	 */		

	public PerfilUsuario getPerfilUsuario() {
		return perfilUsuario;
	}

	public void setPerfilUsuario(PerfilUsuario perfilUsuario) {
		this.perfilUsuario = perfilUsuario;
	}

	public List<PerfilUsuario> getPerfisUsuarios() {
		return perfisUsuarios;
	}

	public void setPerfisUsuarios(List<PerfilUsuario> perfisUsuarios) {
		this.perfisUsuarios = perfisUsuarios;
	}

	public PerfilUsuario getPerfilSelecionado() {
		return perfilSelecionado;
	}

	public void setPerfilSelecionado(PerfilUsuario perfilSelecionado) {
		this.perfilSelecionado = perfilSelecionado;
	}

	public List<Permissao> getPermissoes() {
		return permissoes;
	}

	public void setPermissoes(List<Permissao> permissoes) {
		this.permissoes = permissoes;
	}

	public List<Regra> getRegras() {
		return regras;
	}

	public void setRegras(List<Regra> regras) {
		this.regras = regras;
	}

	public List<Regra> getRegrasSelecionadas() {
		return regrasSelecionadas;
	}

	public void setRegrasSelecionadas(List<Regra> regrasSelecionadas) {
		this.regrasSelecionadas = regrasSelecionadas;
	}
}