package br.com.farmk.controle;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.transaction.Transactional;

import br.com.farmk.entidade.AplicacaoTecnologia;
import br.com.farmk.entidade.Orcamento;
import br.com.farmk.entidade.Planejamento;
import br.com.farmk.entidade.PlanejamentoProduto;
import br.com.farmk.entidade.Produto;
import br.com.farmk.entidade.Produto_Uso;
import br.com.farmk.entidade.Tecnologia;
import br.com.farmk.entidade.UsoProduto;
import br.com.farmk.util.UtilFaces;
import br.com.farmk.util.UtilLog;

public class CalculoPlanejamento implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Tecnologia tipoAreaTotal;
	
	private UsoProduto usoProdutoSemente;
	
	private List<PlanejamentoProduto> planejamentoProdutos = new ArrayList<PlanejamentoProduto>();
	private List<UsoProduto> categorias = new ArrayList<UsoProduto>();	
	
	private Map<Produto, BigDecimal> totalPorProduto = new HashMap<Produto, BigDecimal>();	
	private Map<UsoProduto, BigDecimal> totalPorCategoria = new HashMap<UsoProduto, BigDecimal>();
	
	private Map<Tecnologia, BigDecimal> totalPorTecnologia = new HashMap<Tecnologia, BigDecimal>();
	private Map<Produto, BigDecimal> totalAreaPorProduto = new HashMap<Produto, BigDecimal>();
	
	private BigDecimal areaTotal = BigDecimal.ZERO;
	
	private BigDecimal totalGeralInsumos = 				BigDecimal.ZERO;
	private BigDecimal totalGeralInsumosBrl =			BigDecimal.ZERO;
	private BigDecimal totalDespesasOperacionais = 		BigDecimal.ZERO;
	private BigDecimal totalDespesasOperacionaisBrl = 	BigDecimal.ZERO;
	private BigDecimal totalGeralPorArea = 				BigDecimal.ZERO;
	private BigDecimal totalGeralPorAreaBrl = 			BigDecimal.ZERO;
	private BigDecimal totalGeralCustos = 				BigDecimal.ZERO;
	private BigDecimal totalGeralCustosBrl = 			BigDecimal.ZERO;
	private BigDecimal totalOperacionalEInsumos = 		BigDecimal.ZERO;
	private BigDecimal totalOperacionalEInsumosBrl = 	BigDecimal.ZERO;
	private BigDecimal custoTotalPorArea	=			BigDecimal.ZERO;
	private BigDecimal custoTotalPorAreaBrl	=			BigDecimal.ZERO;
	private BigDecimal custoTotalUnidadeArea = 			BigDecimal.ZERO;
	private BigDecimal custoTotalUnidadeAreaBrl = 		BigDecimal.ZERO;
	private BigDecimal receitaPorHa = 					BigDecimal.ZERO;
	private BigDecimal receitaPorHaBrl = 				BigDecimal.ZERO;
	private BigDecimal receitaPrecoPorArea = 			BigDecimal.ZERO;
	private BigDecimal receitaPrecoPorAreaBrl = 		BigDecimal.ZERO;
	private BigDecimal receitaTotal =	 				BigDecimal.ZERO;
	private BigDecimal receitaTotalBrl = 				BigDecimal.ZERO;
	private BigDecimal receitaLiquida = 				BigDecimal.ZERO;
	private BigDecimal receitaLiquidaBrl = 				BigDecimal.ZERO;
	
	public CalculoPlanejamento comAreaTotal(Tecnologia tipoAreaTotal) {
		this.tipoAreaTotal = tipoAreaTotal;
		return this;
	}
	
	public CalculoPlanejamento comUsoProdutoSemente(UsoProduto usoProdutoSemente) {
		this.usoProdutoSemente = usoProdutoSemente;
		return this;
	}
	
	public CalculoPlanejamento calcularTudo(Orcamento orcamento, List<PlanejamentoProduto> pp){
		
		List<PlanejamentoProduto> ppModificar = new ArrayList<>(pp);
		
		for (PlanejamentoProduto planejamentoProduto : pp) {
			incluirProduto(orcamento, ppModificar, planejamentoProduto);
		}
		
		this.setPlanejamentoProdutos(ppModificar);
		
		return this;
	}
	
	public CalculoPlanejamento incluirProduto(Orcamento orcamento, List<PlanejamentoProduto> pp, PlanejamentoProduto planejamentoProduto){
		try {
			Produto produto = planejamentoProduto.getProduto();
			if (produto != null) {
				// Filtrando categorias dos produtos selecionados
				for (Produto_Uso produto_uso : produto.getProduto_usos()) {
					if (produto_uso.getUsoProduto().isEhCategoria()) {
						if (!categorias.contains(produto_uso.getUsoProduto())) {
							produto.setCategoria(produto_uso.getUsoProduto());
						}
					}
				}
				
				pp = atualizarListaDePlanejamentos(pp, planejamentoProduto);
				
				validarValoresPlanejamentoProduto(planejamentoProduto);
				
				calcularTotalPorTecnologia(pp);
				
				calcularValoresDaLinha(orcamento, planejamentoProduto);
				
				if (ehSemente(produto)) {
					for (PlanejamentoProduto ppr : pp) {
						calcularValoresDaLinha(orcamento, ppr);
					}
				}
				
				// Calcular Total por Categoria
				this.totalPorCategoria = calcularTotalPorCategoria(pp);
				
				calcularTotalGeral();
				totalGeralCustosBrl = totalGeralCustos.multiply(orcamento.getCotacaoMoeda());
				
				if (planejamentoProduto.getPlanejamento() != null) {
					calcularTotais(planejamentoProduto.getPlanejamento(), planejamentoProduto.getPlanejamento().getOrcamento());				
				} else {
					UtilFaces.mostrarAlerta("Não foi possível calcular os totais");
				}
				
				this.setPlanejamentoProdutos(pp);
				
				return this;	
			} else {
				UtilFaces.mostrarAlerta("O produto não foi selecionado");
				return null;
			}
		} catch (Exception e) {
			UtilLog.getLogger().error(CalculoPlanejamento.class, e);
			UtilFaces.mostrarExcecao("Falha ao incluir produto",e);
			return null;
		}
	}
	
	public PlanejamentoProduto calcularValoresDaLinha(Orcamento orcamento, PlanejamentoProduto planejamentoProduto) {
		
		Produto produto = planejamentoProduto.getProduto();
		
		calcularAreaAplicacao(planejamentoProduto);
		
		BigDecimal areaPlanejamentoProduto = calcularAreaAplicacaoPlanejamentoProduto(planejamentoProduto);
		
		planejamentoProduto.setAreaAplicacao(areaPlanejamentoProduto);
		
		BigDecimal qtdTotal = calcularQtdTotalProduto(planejamentoProduto);
		
		planejamentoProduto.setQtdTotal(qtdTotal);
		
		BigDecimal custoPorHectare = calcularCustoPorHectare(planejamentoProduto);
		
		planejamentoProduto.setCustoPorArea(custoPorHectare);
		
		// Cálculo Custo Total
		BigDecimal custoTotal = BigDecimal.ZERO;
		if (planejamentoProduto.getAreaAplicacao() != null) {
			custoTotal = planejamentoProduto.getCustoPorArea().multiply(planejamentoProduto.getAreaAplicacao());			
		}
		planejamentoProduto.setCustoTotal(custoTotal);
		this.totalPorProduto.put(produto, custoTotal);
		this.totalAreaPorProduto.put(produto, planejamentoProduto.getArea());
		
		// Preço Moeda Cotação
		planejamentoProduto.setMoedaCotacao(orcamento.getMoeda());
		BigDecimal precoMoedaCotacao = BigDecimal.ZERO;
		if (orcamento.getCotacaoMoeda().compareTo(BigDecimal.ZERO) == 1) {
			precoMoedaCotacao = planejamentoProduto.getPreco().multiply(orcamento.getCotacaoMoeda());
			planejamentoProduto.setPrecoMoedaCotacao(precoMoedaCotacao);
		} else {
			planejamentoProduto.setPrecoMoedaCotacao(planejamentoProduto.getPreco());
		}
		
		// Custo Total Moeda Cotação
		BigDecimal custoTotalMoedaCotacao = BigDecimal.ZERO;
		custoTotalMoedaCotacao = planejamentoProduto.getCustoTotal().multiply(orcamento.getCotacaoMoeda());
		planejamentoProduto.setCustoTotalMoedaCotacao(custoTotalMoedaCotacao);
		
		return planejamentoProduto;
	}
	
	@Transactional
	private void calcularAreaAplicacao(PlanejamentoProduto planejamentoProduto) {
		
		BigDecimal areaAplicacao = BigDecimal.ZERO;
		
		Produto produto = planejamentoProduto.getProduto();
		
		if (!ehSemente(produto)) {
			
			if ((planejamentoProduto.getArea() != null) && (!planejamentoProduto.getAplicacaoesTecnologia().isEmpty())) {
				
				for (AplicacaoTecnologia aplicacaoTecnologia : planejamentoProduto.getAplicacaoesTecnologia()) {
					
					areaAplicacao = BigDecimal.ZERO;
					
					if (totalPorTecnologia.containsKey(aplicacaoTecnologia.getTecnologia())) {
						
						areaAplicacao = totalPorTecnologia.get(aplicacaoTecnologia.getTecnologia());
						
					} else {
						
						areaAplicacao = areaTotal;
						
					}
					
					calcularValoresAplicacaoTecnologia(aplicacaoTecnologia, areaAplicacao);
					
				}
			}
			
		} else {
			
			if (permitidoParaAreaTotal(planejamentoProduto) && planejamentoProduto.getArea() != null) {
				
				areaAplicacao = planejamentoProduto.getArea();
				
				for (AplicacaoTecnologia aplicacaoTecnologia : planejamentoProduto.getAplicacaoesTecnologia()) {
					
					calcularValoresAplicacaoTecnologia(aplicacaoTecnologia, areaAplicacao);
					
				}
				
			}
			
		}
	}
	
	private BigDecimal calcularAreaAplicacaoPlanejamentoProduto(PlanejamentoProduto planejamentoProduto) {
		
		BigDecimal areaAplicacaoPlanejamentoProduto = BigDecimal.ZERO;
		
		if (!ehSemente(planejamentoProduto.getProduto())) {

			for (AplicacaoTecnologia aplicacaoTecnologia : planejamentoProduto.getAplicacaoesTecnologia()) {
				
				if (planejamentoProduto.getArea() != null) {

					areaAplicacaoPlanejamentoProduto = areaAplicacaoPlanejamentoProduto.add(aplicacaoTecnologia.getAreaAplicada());
					
				}
			}			
		} else {
		
			areaAplicacaoPlanejamentoProduto = planejamentoProduto.getArea();
			
		}

		return areaAplicacaoPlanejamentoProduto;
		
	}
	
	private void validarValoresPlanejamentoProduto(PlanejamentoProduto pp) {
		
		if (pp.getAplicacaoesTecnologia() != null) {
			for (AplicacaoTecnologia aplicacao : pp.getAplicacaoesTecnologia()) {
				if (aplicacao.getDosePorHectare() == null) {
					aplicacao.setDosePorHectare(BigDecimal.ZERO);
				}
				if (aplicacao.getNumVezes() == null) {
					aplicacao.setNumVezes(BigDecimal.ZERO);
				}
			}					
		}
		
	}
	
	private void calcularValoresAplicacaoTecnologia(AplicacaoTecnologia aplicacaoTecnologia, BigDecimal areaTotalTecnologia) {
		
		if (areaTotalTecnologia.compareTo(BigDecimal.ZERO) > 0) {
			
			BigDecimal areaAplicada = areaTotalTecnologia.multiply(aplicacaoTecnologia.getNumVezes());
			
			aplicacaoTecnologia.setAreaAplicada(areaAplicada);
			
			// Calcular Quantidade da Tecnologia
			BigDecimal quantidade = areaAplicada
					.multiply(aplicacaoTecnologia.getDosePorHectare());
			
			aplicacaoTecnologia.setQuantidade(quantidade);
			
			BigDecimal custoTotal = quantidade.multiply(aplicacaoTecnologia.getPlanejamentoProduto().getPreco());								
			
			aplicacaoTecnologia.setCustoTotal(custoTotal);
			
			BigDecimal custoPorHectare = custoTotal.divide(areaAplicada, 2, RoundingMode.HALF_UP);
			
			aplicacaoTecnologia.setCustoPorHectare(custoPorHectare);
			
		} else {
			
			aplicacaoTecnologia.setAreaAplicada(BigDecimal.ZERO);
			
		}
		
	}
	
	private BigDecimal calcularQtdTotalProduto(PlanejamentoProduto planejamentoProduto) {
		BigDecimal qtdTotal = BigDecimal.ZERO;
		if ((!planejamentoProduto.getAplicacaoesTecnologia().isEmpty()) && (planejamentoProduto.getArea() != null)) {
			// A qtdTotal deve ser calculada de acordo com a área de aplicão da tecnologia.
			// Vai selecionar em quais Tecnologias o produto será aplicado.
			// Deve-se subtrair a área total das tecnologias NÃO selecionadas, da área total de plantio.
			
			// O numVezes deve ser separado para cada tecnologia,
			// O cáculo, deve multiplicar o numVezes da Tecnologia selecionada.
			
			for (AplicacaoTecnologia aplicacaoTecnologia : planejamentoProduto.getAplicacaoesTecnologia()) {
				
				if (aplicacaoTecnologia.getQuantidade() != null) {

					qtdTotal = qtdTotal.add(aplicacaoTecnologia.getQuantidade());
					
				}
				
			}
			
		}
		return qtdTotal;
	}
	
	private BigDecimal calcularCustoPorHectare(PlanejamentoProduto planejamentoProduto) {
		// Cáculo custo por área
		BigDecimal custoPorHectare = BigDecimal.ZERO;
		if (planejamentoProduto.getAreaAplicacao() != null && 
				planejamentoProduto.getAreaAplicacao().compareTo(BigDecimal.ZERO) > 0 &&
				planejamentoProduto.getAplicacaoesTecnologia().size() > 0) {
			
			custoPorHectare = planejamentoProduto.getPreco().multiply(planejamentoProduto.getQtdTotal()).divide(planejamentoProduto.getAreaAplicacao(), 4, RoundingMode.HALF_UP);
			
		}
		return custoPorHectare;
	}
	
	public CalculoPlanejamento calcularTotalPorTecnologia(List<PlanejamentoProduto> pp) {
		
		Map<Tecnologia, BigDecimal> areaTotalPorTecnologia = new HashMap<Tecnologia, BigDecimal>();
		try {
			
			for (PlanejamentoProduto planejamentoProduto : pp) {
				
				if (permitidoParaAreaTotal(planejamentoProduto)) {
					
					for (AplicacaoTecnologia aplicacaoTecnologia : planejamentoProduto.getAplicacaoesTecnologia()) {
						
						if (areaTotalPorTecnologia.containsKey(aplicacaoTecnologia.getTecnologia())) {
								
							BigDecimal areaAplicacao = areaTotalPorTecnologia.get(aplicacaoTecnologia.getTecnologia());
							
							areaAplicacao = areaAplicacao.add(planejamentoProduto.getArea());
							
							areaTotalPorTecnologia.replace(aplicacaoTecnologia.getTecnologia(), areaAplicacao);
								
						} else {
								
							areaTotalPorTecnologia.put(aplicacaoTecnologia.getTecnologia(), planejamentoProduto.getArea());	
								
						}
					}					
				}
			}
		} catch (Exception e) {
			UtilLog.getLogger().error(CalculoPlanejamento.class, e);
			UtilFaces.mostrarExcecao("Falha ao Calcular Total Por Tecnologia", e);
		}
		
		this.totalPorTecnologia = areaTotalPorTecnologia;
		
		BigDecimal areaTotal = BigDecimal.ZERO;
		for (Map.Entry<Tecnologia, BigDecimal> entry : totalPorTecnologia.entrySet()) {
			areaTotal = areaTotal.add(entry.getValue());
		}		
		this.areaTotal = areaTotal;
		
		return this;
	}
	
	private List<PlanejamentoProduto> atualizarListaDePlanejamentos(List<PlanejamentoProduto> pp, PlanejamentoProduto planejamentoProduto){
		
		boolean estaNaLista = false;
		int i = 0;
		for (i = 0; i < pp.size(); i++) {
			if (pp.get(i).getProduto().equals(planejamentoProduto.getProduto())) {
				estaNaLista = true;
				break;
			}
		}
		
		if (estaNaLista) {
			pp.set(i, planejamentoProduto);
		} else {
			pp.add(planejamentoProduto);
		}
		
		return pp;
	}
	
	private Map<UsoProduto, BigDecimal> calcularTotalPorCategoria(List<PlanejamentoProduto> pp){
		Map<UsoProduto, BigDecimal> totalPorCategoria = new HashMap<UsoProduto, BigDecimal>();
		try {
			for (PlanejamentoProduto p : pp) {
				
				if (p.getCustoTotal() == null) {
					p.setCustoTotal(BigDecimal.ZERO);
				}
				
				if (totalPorCategoria.containsKey(p.getProduto().getCategoria())) {
					BigDecimal total = totalPorCategoria.get(p.getProduto().getCategoria());
					total = total.add(p.getCustoTotal());
					totalPorCategoria.replace(p.getProduto().getCategoria(), total);
				} else {
					totalPorCategoria.put(p.getProduto().getCategoria(), p.getCustoTotal());
				}
			}
		} catch (Exception e) {
			UtilLog.getLogger().error(CalculoPlanejamento.class, e);
			UtilFaces.mostrarExcecao("Falha ao Calcular Total Por Categoria", e);
		}
		return totalPorCategoria;
	}
	
	private boolean permitidoParaAreaTotal(PlanejamentoProduto planejamentoProduto) {
		
		Produto produto = planejamentoProduto.getProduto();
		
		if (planejamentoProduto.getArea() == null) {
			return false;
		}
		
		if (planejamentoProduto.getAplicacaoesTecnologia().isEmpty()) {
			return false;
		}
		
		if (produto.getTecnologia() == null) {
			return false;
		}
		
		if (produto.getTecnologia().equals(tipoAreaTotal)) {
			return false;
		}
		
		if (ehSemente(produto)) {
			return true;
		}
		
		return false;
		
	}
	
	private boolean ehSemente(Produto produto) {
		
		for (Produto_Uso produto_uso : produto.getProduto_usos()) {
			if (produto_uso.getUsoProduto().equals(usoProdutoSemente)) {
				return true;
			}
		}
		
		return false;
		
	}
	
	private void calcularTotalGeral(){
		// Calcular Total Geral
		totalGeralCustos = BigDecimal.ZERO;
		for (Map.Entry<UsoProduto, BigDecimal> mapCategoria : totalPorCategoria.entrySet()) {
			totalGeralCustos = totalGeralCustos.add(mapCategoria.getValue());
		}
	}
	
	// Calcular Totais
	private void calcularTotais(Planejamento planejamento, Orcamento orcamento){
		try {
			// Total Geral Insumos
			if (areaTotal.compareTo(BigDecimal.ZERO) > 0) {
				totalGeralInsumos = totalGeralCustos.divide(areaTotal, 4, RoundingMode.HALF_UP);
			} else {
				totalGeralInsumos = BigDecimal.ZERO;
			}
			totalGeralInsumosBrl = totalGeralInsumos.multiply(orcamento.getCotacaoMoeda()); 
			
			// Total Geral Custos
			if (planejamento.getOutrosCustos() != null) {
				BigDecimal outrosCustosBrl = planejamento.getOutrosCustos();
				
				// Total Despesas Operacionais
				totalDespesasOperacionaisBrl = areaTotal.multiply(outrosCustosBrl);
			} else {
				totalDespesasOperacionais = BigDecimal.ZERO;
			}
			totalGeralCustosBrl = totalGeralCustos.multiply(orcamento.getCotacaoMoeda());
			totalDespesasOperacionais = totalDespesasOperacionaisBrl.divide(orcamento.getCotacaoMoeda(), 4, RoundingMode.HALF_UP);
			
			// Total Geral Por Area
			if (areaTotal.compareTo(BigDecimal.ZERO) != 0) {
				totalGeralPorArea = totalGeralCustos.divide(areaTotal, 4, RoundingMode.HALF_UP);			
			}
			totalGeralPorAreaBrl = totalGeralPorArea.multiply(orcamento.getCotacaoMoeda());
			
			// Total Operacional + Insumos
			totalOperacionalEInsumos = totalDespesasOperacionais.add(totalGeralCustos);
			totalOperacionalEInsumosBrl = totalOperacionalEInsumos.multiply(orcamento.getCotacaoMoeda());
			
			// Custo total por área (ha)
			custoTotalPorArea = BigDecimal.ZERO;
			if (areaTotal.compareTo(BigDecimal.ZERO) > 0) {
				custoTotalPorArea = totalOperacionalEInsumos.divide(areaTotal, 4, RoundingMode.HALF_UP);
			}
			custoTotalPorAreaBrl = custoTotalPorArea.multiply(orcamento.getCotacaoMoeda());
			
			// Ponto de Equilibrio
			if (planejamento.getProdutividadeEsperada() != null) {
				custoTotalUnidadeArea = custoTotalPorArea.divide(planejamento.getProdutividadeEsperada(), 4, RoundingMode.HALF_UP);				
			} else {
				custoTotalUnidadeArea = BigDecimal.ZERO;
			}
			custoTotalUnidadeAreaBrl = custoTotalUnidadeArea.multiply(orcamento.getCotacaoMoeda());
			
			// Receita Arroba Por Area
			if ((planejamento.getProdutividadeEsperada() != null) && (planejamento.getPrecoCultura() != null)) {
				receitaPorHaBrl = planejamento.getProdutividadeEsperada().multiply(planejamento.getPrecoCultura());
				receitaPorHa = receitaPorHaBrl.divide(orcamento.getCotacaoMoeda(), 4, RoundingMode.HALF_UP);
			} else {
				receitaPorHaBrl = BigDecimal.ZERO;
				receitaPorHa = BigDecimal.ZERO;
			}
			
			// Receita Total (Em Real R$)
			if (orcamento.getCotacaoMoeda() != null) {
				receitaTotalBrl = receitaPorHaBrl.multiply(areaTotal);
				if ((receitaTotalBrl != BigDecimal.ZERO) && (orcamento.getCotacaoMoeda() != null)) {
					receitaTotal = receitaTotalBrl.divide(orcamento.getCotacaoMoeda(), 4, RoundingMode.HALF_UP);

					// Receita Liquida (Em Real R$)		
					receitaLiquida = receitaTotal.subtract(totalOperacionalEInsumos);
					receitaLiquidaBrl = receitaLiquida.multiply(orcamento.getCotacaoMoeda());
					
					// Receita Preco Por Area
					receitaPrecoPorArea = BigDecimal.ZERO;
					if (areaTotal.compareTo(BigDecimal.ZERO) > 0) {
						receitaPrecoPorArea = receitaLiquida.divide(areaTotal, 4, RoundingMode.HALF_UP);
					}
					receitaPrecoPorAreaBrl = receitaPrecoPorArea.multiply(orcamento.getCotacaoMoeda());
				}
			}
		} catch (Exception e) {
			UtilLog.getLogger().error(CalculoPlanejamento.class, e);
			UtilFaces.mostrarExcecao("Falha ao Calcular Totais", e);
		}
	}
	
	/* 
	 * gets e sets
	 */	
	
	public BigDecimal getTotalGeralInsumos() {
		return totalGeralInsumos;		
	}	
	
	public BigDecimal getTotalGeralInsumosBrl() {
		return totalGeralInsumosBrl;
	}

	public BigDecimal getTotalGeralPorArea() {
		return totalGeralPorArea;
	}
	
	public BigDecimal getTotalGeralCustos() {
		return totalGeralCustos;
	}
	
	public BigDecimal getCustoTotalUnidadeArea() {
		return custoTotalUnidadeArea;
	}	

	public BigDecimal getReceitaPrecoPorArea() {
		return receitaPrecoPorArea;
	}

	public BigDecimal getReceitaTotalBrl() {
		return receitaTotalBrl;
	}
	
	public BigDecimal getTotalGeralPorAreaBrl() {
		return totalGeralPorAreaBrl;
	}

	public BigDecimal getTotalGeralCustosBrl() {
		return totalGeralCustosBrl;
	}

	public BigDecimal getCustoTotalUnidadeAreaBrl() {
		return custoTotalUnidadeAreaBrl;
	}
	
	public BigDecimal getReceitaPorHa() {
		return receitaPorHa;
	}
	
	public BigDecimal getReceitaPorHaBrl() {
		return receitaPorHaBrl;
	}	
	
	public BigDecimal getReceitaPrecoPorAreaBrl() {
		return receitaPrecoPorAreaBrl;
	}
	
	public BigDecimal getReceitaTotal() {
		return receitaTotal;
	}
	
	public BigDecimal getReceitaLiquida() {
		return receitaLiquida;
	}
	
	/*
	 * Outos gets e sets
	 */

	public List<PlanejamentoProduto> getPlanejamentoProdutos() {
		return planejamentoProdutos;
	}

	public void setPlanejamentoProdutos(List<PlanejamentoProduto> planejamentoProdutos) {
		this.planejamentoProdutos = planejamentoProdutos;
	}

	public List<UsoProduto> getCategorias() {
		return categorias;
	}

	public void setCategorias(List<UsoProduto> categorias) {
		this.categorias = categorias;
	}

	public Map<UsoProduto, BigDecimal> getTotalPorCategoria() {
		return totalPorCategoria;
	}

	public void setTotalPorCategoria(Map<UsoProduto, BigDecimal> totalPorCategoria) {
		this.totalPorCategoria = totalPorCategoria;
	}

	public Map<Produto, BigDecimal> getTotalPorProduto() {
		return totalPorProduto;
	}

	public void setTotalPorProduto(Map<Produto, BigDecimal> totalPorProduto) {
		this.totalPorProduto = totalPorProduto;
	}

	public List<Map.Entry<Tecnologia, BigDecimal>> getTotalPorTecnologia() {
		Set<Map.Entry<Tecnologia, BigDecimal>> tecnologia = totalPorTecnologia.entrySet();
		return new ArrayList<Map.Entry<Tecnologia, BigDecimal>>(tecnologia);
	}

	public void setTotalPorTecnologia(Map<Tecnologia, BigDecimal> totalPorTipoArea) {
		this.totalPorTecnologia = totalPorTipoArea;
	}

	public Map<Produto, BigDecimal> getTotalAreaPorProduto() {
		return totalAreaPorProduto;
	}

	public void setTotalAreaPorProduto(Map<Produto, BigDecimal> totalAreaPorProduto) {
		this.totalAreaPorProduto = totalAreaPorProduto;
	}

	public BigDecimal getAreaTotal() {
		return areaTotal;
	}

	public void setAreaTotal(BigDecimal areaTotal) {
		this.areaTotal = areaTotal;
	}

	public Tecnologia getTipoAreaTotal() {
		return tipoAreaTotal;
	}

	public void setTipoAreaTotal(Tecnologia tipoAreaTotal) {
		this.tipoAreaTotal = tipoAreaTotal;
	}

	public BigDecimal getReceitaLiquidaBrl() {
		return receitaLiquidaBrl;
	}

	public BigDecimal getTotalDespesasOperacionais() {
		return totalDespesasOperacionais;
	}

	public void setTotalDespesasOperacionais(BigDecimal totalDespesasOperacionais) {
		this.totalDespesasOperacionais = totalDespesasOperacionais;
	}

	public BigDecimal getTotalDespesasOperacionaisBrl() {
		return totalDespesasOperacionaisBrl;
	}

	public void setTotalDespesasOperacionaisBrl(BigDecimal totalDespesasOperacionaisBrl) {
		this.totalDespesasOperacionaisBrl = totalDespesasOperacionaisBrl;
	}

	public BigDecimal getTotalOperacionalEInsumos() {
		return totalOperacionalEInsumos;
	}

	public void setTotalOperacionalEInsumos(BigDecimal totalOperacionalEInsumos) {
		this.totalOperacionalEInsumos = totalOperacionalEInsumos;
	}

	public BigDecimal getTotalOperacionalEInsumosBrl() {
		return totalOperacionalEInsumosBrl;
	}

	public void setTotalOperacionalEInsumosBrl(BigDecimal totalOperacionalEInsumosBrl) {
		this.totalOperacionalEInsumosBrl = totalOperacionalEInsumosBrl;
	}

	public void setCustoTotalUnidadeArea(BigDecimal custoTotalUnidadeArea) {
		this.custoTotalUnidadeArea = custoTotalUnidadeArea;
	}

	public void setCustoTotalUnidadeAreaBrl(BigDecimal custoTotalUnidadeAreaBrl) {
		this.custoTotalUnidadeAreaBrl = custoTotalUnidadeAreaBrl;
	}

	public BigDecimal getCustoTotalPorArea() {
		return custoTotalPorArea;
	}

	public BigDecimal getCustoTotalPorAreaBrl() {
		return custoTotalPorAreaBrl;
	}
	
	

}