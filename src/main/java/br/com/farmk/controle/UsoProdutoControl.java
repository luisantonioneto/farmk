package br.com.farmk.controle;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.primefaces.event.SelectEvent;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.annotation.SessionScope;

import br.com.farmk.entidade.UsoProduto;
import br.com.farmk.persistencia.UsoProdutoDao;
import br.com.farmk.util.UtilFaces;

@Controller
@SessionScope
public class UsoProdutoControl implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private UsoProdutoDao usoProdutoDao;
	private UsoProduto usoProduto = new UsoProduto();
	private UsoProduto cultura = new UsoProduto();
	private UsoProduto usoProdutoSelecionado = new UsoProduto();
	private List<UsoProduto> usoProdutos = new ArrayList<UsoProduto>();
	
	@PostConstruct
	public void carregar(){
		try {
			usoProdutos = new ArrayList<UsoProduto>();
			usoProdutos = usoProdutoDao.listarPorNome();
			usoProduto = new UsoProduto();
			
			cultura = usoProdutoDao.buscarCultura();
		} catch (Exception e) {
			UtilFaces.falhaAoCarregar(e);
		}
	}
	
	public void salvar(){
		try {
			if (usoProduto.getCodUsoProduto() == 0) {
				usoProdutoDao.salvar(usoProduto);
			} else {
				usoProdutoDao.atualizar(usoProduto);
			}
			UtilFaces.salvoComSucesso("Uso Produto");
		} catch (Exception e) {
			UtilFaces.falhaAoSalvar(e);
		} finally {
			carregar();
		}
	}
	
	public void excluir(){
		try {
			usoProdutoDao.excluir(usoProduto);
			UtilFaces.excluidoComSucesso();
		} catch (Exception e) {
			UtilFaces.falhaAoExcluir(e);				
		} finally {
			carregar();
		}
	}
	
	public void novo(){
		carregar();
	}
	
	public void selecionarLinha(SelectEvent event){
		usoProduto = (UsoProduto)event.getObject();
	}

	public UsoProduto getUsoProduto() {
		return usoProduto;
	}

	public void setUsoProduto(UsoProduto usoProduto) {
		this.usoProduto = usoProduto;
	}

	public UsoProduto getUsoProdutoSelecionado() {
		return usoProdutoSelecionado;
	}

	public void setUsoProdutoSelecionado(UsoProduto usoProdutoSelecionado) {
		this.usoProdutoSelecionado = usoProdutoSelecionado;
	}

	public List<UsoProduto> getUsoProdutos() {
		return usoProdutos;
	}

	public UsoProduto getCultura() {
		return cultura;
	}

	public void setCultura(UsoProduto cultura) {
		this.cultura = cultura;
	}
}