package br.com.farmk.controle;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.primefaces.event.SelectEvent;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.annotation.SessionScope;

import br.com.farmk.entidade.Endereco;
import br.com.farmk.entidade.Pessoa;
import br.com.farmk.persistencia.PessoaDao;
import br.com.farmk.util.UtilFaces;

@Controller
@SessionScope
public class PessoaControl implements Crud, Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private PessoaDao pessoaDao;
	private List<Pessoa> pessoas = new ArrayList<Pessoa>();
	private Pessoa pessoa = new Pessoa();
	private Pessoa pessoaSelecionada = new Pessoa();
	
	private String nomePessoa = new String();
	
	@Override
	@PostConstruct
	public void carregar(){
		pessoas = new ArrayList<Pessoa>();
		pessoas = pessoaDao.listar();
	}
	
	public void salvar(Endereco endereco){
		pessoa.setEndereco(endereco);
		salvar();
	}
	
	
	@Override
	public void salvar(){
		try {
			if (pessoa.getEmail() != null && !pessoa.getEmail().isEmpty()) {
				if (!ehEmailValido(pessoa.getEmail())) {
					throw new Exception("E-mail Inválido");
				}
			}
			
			if (pessoa.getCodPessoa() == 0) {
				pessoa.setDataCadastro(new Date());
				pessoaDao.salvar(pessoa);				
			} else {
				pessoaDao.atualizar(pessoa);
			}				
			UtilFaces.salvoComSucesso("Pessoa");				
		} catch (Exception e) {
			UtilFaces.falhaAoSalvar(e);
		} finally {
			carregar();
		}
	}
	
	@Override
	public void excluir(){
		try {
			pessoaDao.excluir(pessoa);
			UtilFaces.excluidoComSucesso();
		} catch (Exception e) {
			UtilFaces.falhaAoExcluir(e);
		} finally{
			pessoa = new Pessoa();			
			carregar();
		}
	}
	
	@Override
	public void novo(){
		pessoa = new Pessoa();
		carregar();
	}
	
	@Override
	public void selecionarLinha(SelectEvent event) {
		try {
			pessoa = (Pessoa) event.getObject();
		} catch (Exception e) {
			UtilFaces.falhaAoCarregar(e);
		}
	}
	
	public List<Pessoa> completeText(String nome) throws Exception{
		pessoas = new ArrayList<Pessoa>();
		pessoas = pessoaDao.listar(nome);
		return pessoas;
	}
	
	public List<Pessoa> listarPorNome() throws Exception{
		return completeText(nomePessoa);
	}	
	
	public static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

	public static boolean ehEmailValido(String emailStr) {
	    Matcher matcher = VALID_EMAIL_ADDRESS_REGEX .matcher(emailStr);
	    return matcher.find();
	}	
	
	public List<Pessoa> getPessoas() {
		return pessoas;
	}

	public void setPessoas(List<Pessoa> pessoas) {
		this.pessoas = pessoas;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public Pessoa getPessoaSelecionada() {
		return pessoaSelecionada;
	}

	public void setPessoaSelecionada(Pessoa pessoaSelecionada) {
		this.pessoaSelecionada = pessoaSelecionada;
	}

	public String getNomePessoa() {
		return nomePessoa;
	}

	public void setNomePessoa(String nomePessoa) {
		this.nomePessoa = nomePessoa;
	}
}