package br.com.farmk.controle;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.primefaces.event.SelectEvent;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.annotation.SessionScope;

import br.com.farmk.entidade.Estado;
import br.com.farmk.persistencia.EstadoDao;
import br.com.farmk.util.UtilFaces;

@Controller
@SessionScope
public class EstadoControl implements Crud, Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private EstadoDao estadoDao;
	private Estado estado = new Estado();
	private Estado estadoSelecionado = new Estado();
	private List<Estado> estados = new ArrayList<Estado>();
	
	@PostConstruct
	public void carregar() {
		try {
			estados = new ArrayList<Estado>();
			estados = estadoDao.listarPorNome();
			estado = new Estado();
		} catch (Exception e) {
			UtilFaces.falhaAoCarregar(e);
		}
	}	
	
	public void salvar(){
		try {
			if (estado.getCodEstado() == 0) {
				estadoDao.salvar(estado);				
			} else {
				estadoDao.atualizar(estado);
			}
			UtilFaces.salvoComSucesso("Estado");
		} catch (Exception e) {
			UtilFaces.falhaAoSalvar(e);
		} finally{
			carregar();
		}
	}
	
	public void excluir(){
		try {
			estadoDao.excluir(estado);
			UtilFaces.excluidoComSucesso();
		} catch (Exception e) {
			UtilFaces.falhaAoExcluir(e);
		} finally{
			carregar();
		}
	}
	
	public void novo(){
		carregar();
	}	
	
	@Override
	public void selecionarLinha(SelectEvent event) {
		estado = (Estado) event.getObject();
	}	
	
	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public List<Estado> getEstados() {
		return estados;
	}

	public void setEstados(List<Estado> estados) {
		this.estados = estados;
	}

	public Estado getEstadoSelecionado() {
		return estadoSelecionado;
	}

	public void setEstadoSelecionado(Estado estadoSelecionado) {
		this.estadoSelecionado = estadoSelecionado;
	}
}
