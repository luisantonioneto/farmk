package br.com.farmk.controle;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.primefaces.event.SelectEvent;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.annotation.SessionScope;

import br.com.farmk.entidade.TipoEmpresa;
import br.com.farmk.persistencia.TipoEmpresaDao;
import br.com.farmk.util.UtilFaces;

@Controller
@SessionScope
public class TipoEmpresaControl implements Crud, Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private TipoEmpresaDao tipoEmpresaDao;
	private TipoEmpresa tipoEmpresa = new TipoEmpresa();
	private TipoEmpresa tipoEmpresaSelecionada = new TipoEmpresa();
	private List<TipoEmpresa> tipoEmpresas = new ArrayList<TipoEmpresa>();
	
	@Override
	@PostConstruct
	public void carregar() {
		try {
			tipoEmpresa = new TipoEmpresa();
			tipoEmpresas = new ArrayList<TipoEmpresa>();
			tipoEmpresas = tipoEmpresaDao.listarPorNome();
		} catch (Exception e) {
			UtilFaces.falhaAoCarregar(e);
		}
	}	
	
	@Override
	public void salvar(){
		try {
			if (tipoEmpresa.getCodTipoEmpresa() == 0) {
				tipoEmpresaDao.salvar(tipoEmpresa);				
			} else {
				tipoEmpresaDao.atualizar(tipoEmpresa);
			}
			UtilFaces.salvoComSucesso("Tipo Empresa");
		} catch (Exception e) {
			UtilFaces.falhaAoSalvar(e);
		} finally{
			carregar();
		}
	}
	
	@Override
	public void excluir(){
		try {
			tipoEmpresaDao.excluir(tipoEmpresa);
			UtilFaces.excluidoComSucesso();
		} catch (Exception e) {
			UtilFaces.falhaAoExcluir(e);
		} finally{
			carregar();
		}
	}
	
	@Override
	public void novo(){
		carregar();
	}
	
	@Override
	public void selecionarLinha(SelectEvent event) {
		tipoEmpresa = (TipoEmpresa) event.getObject();
	}	

	public TipoEmpresa getTipoEmpresa() {
		return tipoEmpresa;
	}

	public void setTipoEmpresa(TipoEmpresa tipoEmpresa) {
		this.tipoEmpresa = tipoEmpresa;
	}

	public List<TipoEmpresa> getTipoEmpresas() {
		return tipoEmpresas;
	}

	public void setTipoEmpresas(List<TipoEmpresa> tipoEmpresas) {
		this.tipoEmpresas = tipoEmpresas;
	}

	public TipoEmpresa getTipoEmpresaSelecionada() {
		return tipoEmpresaSelecionada;
	}

	public void setTipoEmpresaSelecionada(TipoEmpresa tipoEmpresaSelecionada) {
		this.tipoEmpresaSelecionada = tipoEmpresaSelecionada;
	}
}
