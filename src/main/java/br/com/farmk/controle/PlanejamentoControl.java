package br.com.farmk.controle;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.transaction.Transactional;

import org.primefaces.event.SelectEvent;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.event.TabCloseEvent;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.annotation.SessionScope;

import br.com.farmk.entidade.Empresa;
import br.com.farmk.entidade.Orcamento;
import br.com.farmk.entidade.Permissao;
import br.com.farmk.entidade.Planejamento;
import br.com.farmk.entidade.PlanejamentoProduto;
import br.com.farmk.entidade.Produto;
import br.com.farmk.entidade.Regra;
import br.com.farmk.entidade.Tecnologia;
import br.com.farmk.entidade.UsoProduto;
import br.com.farmk.entidade.Usuario;
import br.com.farmk.persistencia.PlanejamentoDao;
import br.com.farmk.persistencia.ProdutoDao;
import br.com.farmk.persistencia.TecnologiaDao;
import br.com.farmk.persistencia.UsoProdutoDao;
import br.com.farmk.util.UtilFaces;
import br.com.farmk.util.UtilLog;
import br.com.farmk.util.UtilUsuario;

@Controller
@SessionScope
public class PlanejamentoControl implements Crud, Serializable{

	private static final long serialVersionUID = 1L;
	
	@Inject
	private PlanejamentoDao planejamentoDao;
	private Planejamento planejamento = new Planejamento();
	private Planejamento planejamentoSelecionado = new Planejamento();
	private List<Planejamento> planejamentos = new ArrayList<Planejamento>();
	
	@Inject
	private TecnologiaDao tipoAreaDao;
	private Tecnologia tipoArea = new Tecnologia();
	private List<Tecnologia> tipoAreas = new ArrayList<Tecnologia>();
	
	@Inject
	private ProdutoDao produtoDao;
	private List<Produto> culturas = new ArrayList<Produto>();
	private Produto culturaSelecionada = new Produto();
	private Produto culturaSelecionadaAlterar = new Produto();
	private List<Produto> culturasSelecionadas = new ArrayList<Produto>();
	
	@Inject
	private UsoProdutoDao usoProdutoDao;
	private UsoProduto cultura = new UsoProduto();
	
	@Inject
	private UtilUsuario utilUsuario;
	
	@Inject
	private OrcamentoControl orcamentoControl;
	
	@Inject
	private PlanejamentoProdutoControl planejamentoProdutoControl;

	@Override
	@PostConstruct
	public void carregar() {
		try {
			planejamentos = new ArrayList<Planejamento>();
			planejamento = new Planejamento();
			//
			Usuario usuarioLogado = utilUsuario.pegarUsuarioLogado();
			Regra regraAdm = utilUsuario.pegarRegraAdministrador();
			if (usuarioLogado != null) {
				List<Regra> regras = new ArrayList<Regra>();
				if (usuarioLogado.getPerfil() != null) {
					for (Permissao permissao : usuarioLogado.getPerfil().getPermissoes()) {
						regras.add(permissao.getRegra());
					}					
				} else {
					UtilFaces.mostrarAlerta("O usuário está sem perfil");
				}
				//
				if (regras.contains(regraAdm)) {
					planejamentos = planejamentoDao.listar();
				} else {
					List<Empresa> empresas = utilUsuario.pegarEmpresasPermitidas();
					if ((empresas != null) && (!empresas.isEmpty())) {
						planejamentos = planejamentoDao.listarPorEmpresas(empresas);						
					}
				}
			}
			
			tipoAreas = new ArrayList<Tecnologia>();
			tipoAreas = tipoAreaDao.listarPorNome();
			
			listarCulturas();
		} catch (Exception e) {
			UtilLog.getLogger().error(PlanejamentoControl.class, e);
			UtilFaces.falhaAoCarregar(e);
		}
	}

	@Override
	public void salvar() {
		try {
			if (planejamento.getCodPlanejamento() == 0) {
				planejamentoDao.salvar(planejamento);
			} else {
				planejamentoDao.atualizar(planejamento);
			}
		} catch (Exception e) {
			UtilLog.getLogger().error(PlanejamentoControl.class, e);
			UtilFaces.falhaAoSalvar(e);
		}
	}
	
	@Transactional
	public void salvar(Orcamento orcamento){
		try {
			orcamentoControl.salvar(orcamento);
			if (orcamento.getCodOrcamento() != 0) {
				planejamento.setOrcamento(orcamento);
				salvar();
			}
		} catch (Exception e) {
			UtilLog.getLogger().error(PlanejamentoControl.class, e);
			UtilFaces.falhaAoSalvar(e);
		}
	}

	@Override
	public void excluir() {
		try {
			planejamentoDao.excluir(planejamento);
			UtilFaces.excluidoComSucesso();
		} catch (Exception e) {
			UtilLog.getLogger().error(PlanejamentoControl.class, e);
			UtilFaces.falhaAoExcluir(e);
		} finally {
			carregar();
		}
	}

	@Override
	public void novo() {
		planejamento = new Planejamento();
		planejamentoSelecionado = new Planejamento();
		planejamentos = new ArrayList<Planejamento>();
	}

	@Override
	public void selecionarLinha(SelectEvent event) {
		planejamento = (Planejamento) event.getObject();
	}
	
    public void onTabChange(TabChangeEvent event) {
    		planejamento = (Planejamento) event.getData();
    		
    		try {
			 	
    			List<PlanejamentoProduto> pp = planejamentoProdutoControl.listarPorPlanejamento(planejamento);
				
				planejamento.setPlanejamentoProdutos(pp);
				
			} catch (Exception e) {
				UtilLog.getLogger().error(PlanejamentoControl.class, e);
				UtilFaces.mostrarExcecao("Falha ao listar planejamento", e);
			}
    }
    
    public void onTabClose(TabCloseEvent event) {
    		try {
			Planejamento planejamentoRemover = (Planejamento) event.getData();
			planejamentoDao.excluirEmCascata(planejamentoRemover);
			planejamentos.remove(planejamentoRemover);
			UtilFaces.excluidoComSucesso("Planejamento");
		} catch (Exception e) {
			UtilLog.getLogger().error(PlanejamentoControl.class, e);
			UtilFaces.falhaAoExcluir(e);
		}
    }
	
	public void listarCulturas(){
		try {
			cultura = usoProdutoDao.buscarCultura();
			if (cultura == null) {
				UtilFaces.mostrarAlerta("Cadastre Uso Produto com nome Cultura");
			} else {
				culturas.clear();
				culturas = produtoDao.listarPorUsoProduto(cultura);			
			}			
		} catch (Exception e) {
			UtilLog.getLogger().error(PlanejamentoControl.class, e);
			UtilFaces.falhaAoCarregar(e);
		}
	}
	
	public void incluirCultura(Orcamento orcamento){
		culturasSelecionadas.clear();
		for (Planejamento planejamento : planejamentos) {
			if (!culturasSelecionadas.contains(planejamento.getCultura())) {
				culturasSelecionadas.add(planejamento.getCultura());				
			}
		}
		if (culturaSelecionada != null && culturaSelecionada.getCodProduto() != 0) {
			if (!culturasSelecionadas.contains(culturaSelecionada)) {
				Planejamento planejamento = new Planejamento();
				planejamento.setOrcamento(orcamento);
				planejamento.setCultura(culturaSelecionada);
				this.planejamento = planejamento;
				salvar(orcamento);
				
				planejamentos.add(this.planejamento);
			} else {
				UtilFaces.mostrarAlerta(culturaSelecionada.getNome(), "Já está selecionada");
			}			
		} else {
			UtilFaces.mostrarAlerta("Selecione a cultura");
		}
		culturaSelecionada = new Produto();
	}
	
	public void alterarCultura(Orcamento orcamento){
		if (culturaSelecionadaAlterar != null && culturaSelecionadaAlterar.getCodProduto() != 0) {
			if (!culturasSelecionadas.contains(culturaSelecionadaAlterar)) {
				this.planejamento.setCultura(culturaSelecionadaAlterar);
				salvar(orcamento);
			} else {
				UtilFaces.mostrarAlerta(culturaSelecionadaAlterar.getNome(), "Já está selecionada");
			}	
		} else {
			UtilFaces.mostrarAlerta("Selecione a cultura");
		}
		culturaSelecionadaAlterar = new Produto();
	}
	
	public List<Tecnologia> listarTipoAreas(String nome){
		try {
			tipoAreas = new ArrayList<Tecnologia>();
			tipoAreas = tipoAreaDao.listarPorNome(nome);
			return tipoAreas;			
		} catch (Exception e) {
			UtilLog.getLogger().error(PlanejamentoControl.class, e);
			UtilFaces.falhaAoConsultar(e);
			return null;
		}
	}
	
	public List<Planejamento> listarPorOrcamento(Orcamento orcamento) throws Exception{
		List<Planejamento> planejamentos = planejamentoDao.listarPorOrcamento(orcamento);
		this.planejamentos = planejamentos;
		this.planejamento = this.planejamentos.get(0);		
		return this.planejamentos;
	}
	
	public void mostrarAlerta(){
		if (planejamento.getCultura() == null) {
			UtilFaces.mostrarAlerta("Selecione a cultura");
		}
		
		if (planejamento.getPrecoCultura() == null) {
			UtilFaces.mostrarAlerta("Informe o preço da cultura");
		}
		
		if (planejamento.getOutrosCustos() == null) {
			UtilFaces.mostrarAlerta("Informe o valor para outros custos");
		}
		
		if (planejamento.getProdutividadeEsperada() == null) {
			UtilFaces.mostrarAlerta("Informe a produtividade esperada");
		}
	}
	
	/* gets e sets */
	
	public int getPlanejamentosSize(){
		if (planejamentos != null) {
			return planejamentos.size();			
		} else {
			return 0;
		}
	}

	public Planejamento getPlanejamento() {
		return planejamento;
	}

	public void setPlanejamento(Planejamento planejamento) {
		this.planejamento = planejamento;
	}

	public Planejamento getPlanejamentoSelecionado() {
		return planejamentoSelecionado;
	}

	public void setPlanejamentoSelecionado(Planejamento planejamentoSelecionado) {
		this.planejamentoSelecionado = planejamentoSelecionado;
	}

	public List<Planejamento> getPlanejamentos() {
		return planejamentos;
	}

	public void setPlanejamentos(List<Planejamento> planejamentos) {
		this.planejamentos = planejamentos;
	}

	public Tecnologia getTipoArea() {
		return tipoArea;
	}

	public void setTipoArea(Tecnologia tipoArea) {
		this.tipoArea = tipoArea;
	}

	public List<Tecnologia> getTipoAreas() {
		return tipoAreas;
	}

	public void setTipoAreas(List<Tecnologia> tipoAreas) {
		this.tipoAreas = tipoAreas;
	}

	public UsoProduto getCultura() {
		return cultura;
	}

	public void setCultura(UsoProduto cultura) {
		this.cultura = cultura;
	}

	public List<Produto> getCulturas() {
		return culturas;
	}

	public void setCulturas(List<Produto> culturas) {
		this.culturas = culturas;
	}

	public List<Produto> getCulturasSelecionadas() {
		return culturasSelecionadas;
	}

	public void setCulturasSelecionadas(List<Produto> culturasSelecionadas) {
		this.culturasSelecionadas = culturasSelecionadas;
	}

	public Produto getCulturaSelecionada() {
		return culturaSelecionada;
	}

	public void setCulturaSelecionada(Produto culturaSelecionada) {
		this.culturaSelecionada = culturaSelecionada;
	}

	public Produto getCulturaSelecionadaAlterar() {
		return culturaSelecionadaAlterar;
	}

	public void setCulturaSelecionadaAlterar(Produto culturaSelecionadaAlterar) {
		this.culturaSelecionadaAlterar = culturaSelecionadaAlterar;
	}
}