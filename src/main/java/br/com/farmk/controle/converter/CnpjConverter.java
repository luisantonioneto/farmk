package br.com.farmk.controle.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.springframework.stereotype.Service;

import br.com.farmk.util.FormatMask;

@Service
public class CnpjConverter implements Converter {

	@Override
	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {
		if(value != null || value != ""){
			value = value.replace(".", "");
			value = value.replace("-", "");
			value = value.replace("/", "");
		}
		return value;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		
		String cnpj = value.toString();
		if(!cnpj.equals("")){
			return FormatMask.format("##.###.###/####-##", cnpj);	
		} else{
			return "";
		}		
	}
}