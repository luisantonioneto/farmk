package br.com.farmk.controle.converter;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.springframework.stereotype.Service;

import br.com.farmk.util.UtilFaces;

@Service
public class DataConverter implements Converter {

	@Override
	@SuppressWarnings("unused")	
	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {
		String dataNascimento = "";
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");		
		if(value != null || value != ""){
			dataNascimento = sdf.format(value);
			Date date = new Date();
			try {
				date = sdf.parse(dataNascimento);			
			} catch (Exception e) {
				UtilFaces.mostrarExcecao("Falha ao converter data", e);
				return null;
			}

			return date;			
		} else{
			return null;
		}
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		String dataNascimento = null;
		if(value != null || value != ""){
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			dataNascimento = sdf.format(value);
		}
		return dataNascimento;
	}
}
