package br.com.farmk.controle;

import org.primefaces.event.SelectEvent;

public interface Crud {
	
	public void carregar();

	public void salvar();
	
	public void excluir();
	
	public void novo();
	
	public void selecionarLinha(SelectEvent event);
}
