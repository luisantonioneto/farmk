package br.com.farmk.controle;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;

import org.primefaces.event.SelectEvent;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.annotation.SessionScope;

import br.com.farmk.entidade.AplicacaoTecnologia;
import br.com.farmk.entidade.Orcamento;
import br.com.farmk.entidade.Planejamento;
import br.com.farmk.entidade.PlanejamentoProduto;
import br.com.farmk.entidade.Produto;
import br.com.farmk.entidade.Tecnologia;
import br.com.farmk.entidade.UsoProduto;
import br.com.farmk.persistencia.AplicacaoTecnologiaDao;
import br.com.farmk.persistencia.MoedaDao;
import br.com.farmk.persistencia.PlanejamentoProdutoDao;
import br.com.farmk.persistencia.ProdutoDao;
import br.com.farmk.persistencia.TecnologiaDao;
import br.com.farmk.persistencia.UsoProdutoDao;
import br.com.farmk.util.UtilFaces;
import br.com.farmk.util.UtilLog;

@Controller
@SessionScope
public class PlanejamentoProdutoControl implements Crud, Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private OrcamentoControl orcamentoControl;
	
	@Inject 
	private PlanejamentoControl planejamentoControl;
	
	@Inject
	private PlanejamentoProdutoDao planejamentoProdutoDao;
	private PlanejamentoProduto planejamentoProduto = new PlanejamentoProduto();
	private PlanejamentoProduto planejamentoProdutoSelecionado = new PlanejamentoProduto();
	private AplicacaoTecnologia aplicacaoPorTecnologia = new AplicacaoTecnologia();
	private List<PlanejamentoProduto> planejamentoProdutos = new ArrayList<PlanejamentoProduto>();
	
	@Inject
	private ProdutoDao produtoDao;
	private Produto produto = new Produto();
	private List<Produto> produtos = new ArrayList<Produto>();
	
	private UsoProduto categoriaSemente = new UsoProduto();
	private List<UsoProduto> categorias = new ArrayList<UsoProduto>();
	
	private BigDecimal areaTotal = BigDecimal.ZERO;
	private BigDecimal totalGeral = BigDecimal.ZERO;
	
	@Inject
	private TecnologiaDao tipoAreaDao;
	private Tecnologia tipoAreaTotal;
	private List<Tecnologia> tecnologias = new ArrayList<Tecnologia>();
	
	@Inject
	private AplicacaoTecnologiaDao aplicacaoDao;
	
	@Inject
	private MoedaDao moedaDao;
	
	@Inject
	private UsoProdutoDao usoProdutoDao;
	
	private CalculoPlanejamento calculo = new CalculoPlanejamento();

	@Override
	@PostConstruct
	public void carregar() {
		try {
			tipoAreaTotal = tipoAreaDao.buscarTipoAreaTotal();
			categoriaSemente = usoProdutoDao.buscarSemente();
			
			calculo = new CalculoPlanejamento();
			calculo.setTipoAreaTotal(tipoAreaTotal);
			
			planejamentoProduto = new PlanejamentoProduto();
			planejamentoProdutoSelecionado = new PlanejamentoProduto();
			
			produtos = new ArrayList<Produto>();
			produtos = produtoDao.listarPorNome();
			
			tecnologias = tipoAreaDao.listarPorNome();
		} catch (Exception e) {
			UtilLog.getLogger().error(PlanejamentoProdutoControl.class, e);
			UtilFaces.falhaAoCarregar(e);
		}
	}
	
	public void salvar(Planejamento planejamento, List<PlanejamentoProduto> planejamentoProdutos){
		if (planejamento.getCodPlanejamento() != 0) {
			if (planejamentoProdutos != null) {
				for (PlanejamentoProduto planejamentoProduto : planejamentoProdutos) {
					planejamentoProduto.setPlanejamento(planejamento);
					try {
						if (planejamentoProduto.getCodigo() == 0) {
							planejamentoProdutoDao.salvar(planejamentoProduto);
						} else {
							planejamentoProdutoDao.atualizar(planejamentoProduto);
						}
					} catch (EntityNotFoundException e) {
						UtilLog.getLogger().error(PlanejamentoProdutoControl.class, e);
					} catch (Exception e) {
						UtilLog.getLogger().error(PlanejamentoProdutoControl.class, e);
						UtilFaces.falhaAoSalvar(e);
					}
				}				
			}
		}			
	}

	@Override
	public void salvar() {

	}
	
	@Transactional
	public void add(Orcamento orcamento, Planejamento planejamento){
		try {
			boolean podeVer = true;
			planejamentoProduto.setProduto(produto);
			planejamentoProduto.setPlanejamento(planejamento);
			planejamentoProduto.setMoeda(moedaDao.buscarDolar());
			PlanejamentoProduto pp = planejamentoProduto;
			
			if (orcamento.getCotacaoMoeda() == null || orcamento.getCotacaoMoeda() == BigDecimal.ZERO) {
				podeVer = false;
				UtilFaces.mostrarAlerta("Informe um valor para cotação");
			}
			
			if (podeVer) {
				verificarSeCalculoExiste();
				
				planejamentoProdutos = calculo
						.incluirProduto(orcamento, planejamentoProdutos, pp)
						.getPlanejamentoProdutos();
				
				// Incluir no banco de dados
				if (orcamento.getCodOrcamento() == 0) {
					orcamentoControl.salvar(orcamento);					
				}
				
				planejamentoControl.salvar(orcamento);
				salvar(planejamento, planejamentoProdutos);
				
				orcamentoControl.salvarResponsaveis(orcamento);
				
				UtilFaces.incluidoComSucesso(produto.getNome());
			}
			
			planejamentoProduto = new PlanejamentoProduto();
			produto = new Produto();
		} catch (Exception e) {
			UtilLog.getLogger().error(PlanejamentoProdutoControl.class, e);
			UtilFaces.mostrarExcecao("Falha ao incluir Produto", e);
		}
	}
	
	public void addAplicacaoTecnologia(PlanejamentoProduto planejamentoProduto) {
		if (planejamentoProduto.getAplicacaoesTecnologia() == null) {
			planejamentoProduto.setAplicacaoesTecnologia(new ArrayList<AplicacaoTecnologia>());
		}
		AplicacaoTecnologia aplicacao = new AplicacaoTecnologia();
		aplicacao.setPlanejamentoProduto(planejamentoProduto);
		planejamentoProduto.getAplicacaoesTecnologia().add(aplicacao);
	}
	
	public void removerAplicacaoTecnologia(AplicacaoTecnologia aplicacaoTecnologia, List<PlanejamentoProduto> pp) {
		try {
			if (aplicacaoTecnologia.getCodigo() != 0) {
				aplicacaoDao.excluir(aplicacaoTecnologia);
			}
			planejamentoProduto.getAplicacaoesTecnologia().remove(aplicacaoTecnologia);
			UtilFaces.excluidoComSucesso();
			//
			pp.forEach(p -> {
				if (planejamentoProduto.equals(p)) {
					p.getAplicacaoesTecnologia().remove(aplicacaoTecnologia);
				}
			});
			calculo = calculo.calcularTotalPorTecnologia(pp);
		} catch (Exception e) {
			UtilLog.getLogger().error(PlanejamentoProdutoControl.class, e);
			UtilFaces.mostrarExcecao("Falha ao excluir AplicacaoTecnologia", e);
		}
	}
	
	public void removerProduto(Orcamento orcamento, PlanejamentoProduto ppSelecionado){
		try {
			if (ppSelecionado != null){
				if (ppSelecionado.getCodigo() != 0) {
					
					// Garantir que o objeto não foi alterado na tela
					PlanejamentoProduto ppARemover = planejamentoProdutoDao.buscarPorId(ppSelecionado.getCodigo());					
					//
					planejamentoProdutoDao.excluir(ppARemover);
					UtilFaces.excluidoComSucesso("Produto");
				}
				
				if (planejamentoProdutos.contains(ppSelecionado)) {
					planejamentoProdutos.remove(ppSelecionado);
				}
				
				calculo = calculo.calcularTudo(orcamento, planejamentoProdutos);
				
				planejamentoProdutos = calculo.getPlanejamentoProdutos(); 
				
			} else {
				UtilFaces.mostrarAlerta("Não foi possível excluir");
			}
		} catch (Exception e) {
			UtilLog.getLogger().error(PlanejamentoProdutoControl.class, e);
			UtilFaces.mostrarExcecao(e);
		}
	}
	
	public BigDecimal getTotalPorCategoria(UsoProduto categoria){
		if (categoria != null) {
			verificarSeCalculoExiste();
			if ((calculo.getTotalPorCategoria() != null) && (calculo.getTotalPorCategoria().containsKey(categoria))) {
				return calculo.getTotalPorCategoria().get(categoria);
			} else {
				return BigDecimal.ZERO;
			}			
		} else {
			UtilFaces.mostrarAlerta("Falha ao buscar Total Por Categoria");
			return BigDecimal.ZERO;
		}
	}

	@Override
	public void excluir() {
		try {
			planejamentoProdutoDao.excluir(planejamentoProduto);
		} catch (Exception e) {
			UtilLog.getLogger().error(PlanejamentoProdutoControl.class, e);
			UtilFaces.falhaAoExcluir(e);
		}
	}

	@Override
	public void novo() {
		carregar();
		planejamentoProdutos = new ArrayList<PlanejamentoProduto>();
	}

	@Override
	public void selecionarLinha(SelectEvent event) {
		planejamentoProduto = (PlanejamentoProduto) event.getObject();
		produto = planejamentoProduto.getProduto();
	}
	
	public List<Produto> listarProdutos(String nome){
		try {
			produtos = new ArrayList<Produto>();
			produtos = produtoDao.consultarPorNome(nome);
			return produtos;
		} catch (Exception e) {
			UtilLog.getLogger().error(PlanejamentoProdutoControl.class, e);
			UtilFaces.falhaAoConsultar(e);
			return null;
		}
	}	
	
    public void selecionarProduto(SelectEvent event){
    	produto = (Produto) event.getObject();
    	planejamentoProduto.setProduto(produto);
    	if (planejamentoProduto.getArea() == null) {			
    		planejamentoProduto.setArea(BigDecimal.ZERO);
		}
    	if (produto.getTecnologia() == null) {
    		planejamentoProduto.setArea(calculo.getAreaTotal());
		} else if (produto.getTecnologia().equals(tipoAreaTotal)) {
			planejamentoProduto.setArea(calculo.getAreaTotal());
			
			aplicacaoPorTecnologia.setTecnologia(produto.getTecnologia());
			aplicacaoPorTecnologia.setPlanejamentoProduto(planejamentoProduto);
		}
    }
    
    public void listarPorPlanejamento(SelectEvent event){
    	try {
    		Planejamento planejamento = (Planejamento) event.getObject();
    		planejamentoProdutos = planejamentoProdutoDao.listarPorPlanejamento(planejamento);
    		calculo = calculo.calcularTudo(planejamento.getOrcamento(), planejamentoProdutos);
		} catch (Exception e) {

			UtilLog.getLogger().error(PlanejamentoProdutoControl.class, e);
			UtilFaces.falhaAoConsultar(e);
		}
    }
    
    private void verificarSeCalculoExiste(){
    	if (calculo == null) {
			calculo = new CalculoPlanejamento()
					.comAreaTotal(tipoAreaTotal)
					.comUsoProdutoSemente(categoriaSemente);
		}
    }
    
    @Transactional
    public List<PlanejamentoProduto> listarPorPlanejamento(Planejamento planejamento) throws Exception{
	    	planejamentoProdutos = planejamentoProdutoDao.listarPorPlanejamento(planejamento);
	    	calculo
	    		.comAreaTotal(tipoAreaTotal)
	    		.comUsoProdutoSemente(categoriaSemente)
	    		.calcularTudo(planejamento.getOrcamento(), planejamentoProdutos);
	    	return planejamentoProdutos;
    }
	
	/*
	 * gets e sets
	 */

	public PlanejamentoProduto getPlanejamentoProduto() {
		return planejamentoProduto;
	}

	public void setPlanejamentoProduto(PlanejamentoProduto planejamentoProduto) {
		this.planejamentoProduto = planejamentoProduto;
	}

	public List<PlanejamentoProduto> getPlanejamentoProdutos() {
		return planejamentoProdutos;
	}

	public void setPlanejamentoProdutos(List<PlanejamentoProduto> planejamentoProdutos) {
		this.planejamentoProdutos = planejamentoProdutos;
	}

	public PlanejamentoProduto getPlanejamentoProdutoSelecionado() {
		return planejamentoProdutoSelecionado;
	}

	public void setPlanejamentoProdutoSelecionado(PlanejamentoProduto planejamentoProdutoSelecionado) {
		this.planejamentoProdutoSelecionado = planejamentoProdutoSelecionado;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public List<Produto> getProdutos() {
		return produtos;
	}

	public void setProdutos(List<Produto> produtos) {
		this.produtos = produtos;
	}

	public List<UsoProduto> getCategorias() {
		return categorias;
	}

	public void setCategorias(List<UsoProduto> categorias) {
		this.categorias = categorias;
	}

	public BigDecimal getTotalGeral() {
		return totalGeral;
	}

	public void setTotalGeral(BigDecimal totalGeral) {
		this.totalGeral = totalGeral;
	}

	public BigDecimal getAreaTotal() {
		return areaTotal;
	}

	public void setAreaTotal(BigDecimal areaTotal) {
		this.areaTotal = areaTotal;
	}

	public CalculoPlanejamento getCalculo() {
		return calculo;
	}

	public void setCalculo(CalculoPlanejamento calculo) {
		this.calculo = calculo;
	}

	public Tecnologia getTipoAreaTotal() {
		return tipoAreaTotal;
	}

	public void setTipoAreaTotal(Tecnologia tipoAreaTotal) {
		this.tipoAreaTotal = tipoAreaTotal;
	}

	public List<Tecnologia> getTecnologias() {
		return tecnologias;
	}

	public void setTecnologias(List<Tecnologia> tecnologias) {
		this.tecnologias = tecnologias;
	}
}