package br.com.farmk.controle;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

import org.primefaces.event.SelectEvent;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.annotation.SessionScope;

import br.com.farmk.entidade.Empresa;
import br.com.farmk.entidade.Empresa_RamoAtividade;
import br.com.farmk.entidade.Empresa_Tipo;
import br.com.farmk.entidade.Endereco;
import br.com.farmk.entidade.RamoAtividade;
import br.com.farmk.entidade.TipoEmpresa;
import br.com.farmk.persistencia.EmpresaDao;
import br.com.farmk.persistencia.Empresa_RamoAtividadeDao;
import br.com.farmk.persistencia.Empresa_TipoDao;
import br.com.farmk.persistencia.RamoAtividadeDao;
import br.com.farmk.persistencia.TipoEmpresaDao;
import br.com.farmk.util.UtilFaces;
import br.com.farmk.util.UtilUsuario;

@Controller
@SessionScope
public class EmpresaControl implements Crud, Serializable{

	private static final long serialVersionUID = 1L;
	
	@Inject
	private EmpresaDao empresaDao;
	private List<Empresa> empresas = new ArrayList<Empresa>();
	private Empresa empresa = new Empresa();
	private Empresa empresaSelecionada = new Empresa();
	
	@Inject
	private TipoEmpresaDao tipoEmpresaDao;
	private List<TipoEmpresa> tipoEmpresas = new ArrayList<TipoEmpresa>();
	private List<TipoEmpresa> tipoEmpresasSelecionados = new ArrayList<TipoEmpresa>();
	
	@Inject
	private RamoAtividadeDao ramoAtividadeDao;
	private List<RamoAtividade> ramoAtividades = new ArrayList<RamoAtividade>();
	private List<RamoAtividade> ramoAtividadesSelecionados = new ArrayList<RamoAtividade>();
	
	@Inject
	private Empresa_TipoDao empresa_tipoDao;
	
	@Inject
	private Empresa_RamoAtividadeDao empresa_ramoAtividadeDao;
	
	@Inject
	private UtilUsuario utilUsuario;
	
	@PostConstruct
	public void carregar(){
		try {
			empresas = utilUsuario.pegarEmpresasPermitidas();
			empresa = new Empresa();
			empresa.setAtivo(true);
			
			listarTipoEmpresas();
			listarRamoAtividades();
		} catch (Exception e) {
			UtilFaces.falhaAoCarregar(e);
		}
	}
	
	@Transactional(value=TxType.REQUIRES_NEW)
	public void listarTipoEmpresas() throws Exception{
		tipoEmpresas = new ArrayList<TipoEmpresa>();
		tipoEmpresas = tipoEmpresaDao.listarPorNome();
		tipoEmpresasSelecionados = new ArrayList<TipoEmpresa>();
	}
	
	@Transactional(value=TxType.REQUIRES_NEW)
	public void listarRamoAtividades() throws Exception{
		ramoAtividades = new ArrayList<RamoAtividade>();
		ramoAtividades = ramoAtividadeDao.listarPorNome();
		ramoAtividadesSelecionados = new ArrayList<RamoAtividade>();
	}
	
	@Transactional(value=TxType.REQUIRES_NEW)
	public void salvar(Endereco endereco){
		empresa.setEndereco(endereco);
		salvar();
	}
	
	public void salvar(){
		try {
			if (empresa.getEmpresaSuperior() == null || 
				(empresa.getCodEmpresa() != empresa.getEmpresaSuperior().getCodEmpresa())) {
				
				// Salvar Empresa
				try {
					if (empresa.getCodEmpresa() == 0) {
						empresaDao.salvar(empresa);						
					} else {
						empresaDao.salvar(empresa);
					}
					UtilFaces.salvoComSucesso("Empresa");
					
					// Salvar Tipo Empresa
					salvarTipoEmpresa();
					
					// Salvar Ramo Atividade
					salvarRamoAtividade();
				} catch (Exception e) {
					UtilFaces.falhaAoSalvar(e);
				}
			} else{
				UtilFaces.mostrarAlerta("A empresa superior deve ser diferente.");
			}
		} catch (Exception e) {
			UtilFaces.falhaAoSalvar(e);
		} finally{
			carregar();
		}
	}
	
	public void salvarTipoEmpresa(){
		try {
			for (TipoEmpresa tipoEmpresa : tipoEmpresasSelecionados) {
				if (empresa_tipoDao.buscar(empresa, tipoEmpresa) == null) {
					Empresa_Tipo empresa_tipo = new Empresa_Tipo();
					empresa_tipo.setEmpresa(empresa);
					empresa_tipo.setTipoEmpresa(tipoEmpresa);
					empresa_tipoDao.salvar(empresa_tipo);
				}
			}
			// Remover do banco os que foram desmarcados
			tipoEmpresas = tipoEmpresaDao.listar();
			tipoEmpresas.removeAll(tipoEmpresasSelecionados);
			for (TipoEmpresa tipoEmpresa : tipoEmpresas) {
				Empresa_Tipo empresa_tipo = empresa_tipoDao.buscar(empresa, tipoEmpresa);
				if (empresa_tipo != null) {
					empresa_tipoDao.excluir(empresa_tipo);
				}
			}
			UtilFaces.salvoComSucesso("Tipo Empresa");
		} catch (Exception e) {
			UtilFaces.falhaAoSalvar(e);
		}
	}
	
	public void salvarRamoAtividade(){
		try {
			if (ramoAtividadesSelecionados == null) {
				ramoAtividadesSelecionados = new ArrayList<RamoAtividade>();
			}
			for (RamoAtividade ramoAtividade : ramoAtividadesSelecionados) {
				if (empresa_ramoAtividadeDao.buscar(empresa, ramoAtividade) == null) {
					Empresa_RamoAtividade empresa_ramoAtividade = new Empresa_RamoAtividade();
					empresa_ramoAtividade.setEmpresa(empresa);
					empresa_ramoAtividade.setRamoAtividade(ramoAtividade);
					empresa_ramoAtividadeDao.salvar(empresa_ramoAtividade);
				}
			}
			// Remover do banco os que foram desmarcados
			ramoAtividades = ramoAtividadeDao.listar();
			ramoAtividades.removeAll(ramoAtividadesSelecionados);
			for (RamoAtividade ramoAtividade : ramoAtividades) {
				Empresa_RamoAtividade empresa_ramoAtividade = empresa_ramoAtividadeDao.buscar(empresa, ramoAtividade);
				if (empresa_ramoAtividade != null) {
					empresa_ramoAtividadeDao.excluir(empresa_ramoAtividade);
				}
			}
			UtilFaces.salvoComSucesso("Ramo Atividade");
		} catch (Exception e) {
			UtilFaces.falhaAoSalvar(e);
		}		
	}

	public void excluir(){
		try {
			empresaDao.excluir(empresa);
			UtilFaces.excluidoComSucesso();
		} catch (Exception e) {
			UtilFaces.falhaAoExcluir(e);
		} finally{
			empresa = new Empresa();
			carregar();
		}
	}
	
	public void novo(){
		carregar();
	}
	
	public void imprimir(){
		String nomeRelatorio = "empresas";
		Map<String, Object> parametros = new HashMap<>();
		UtilFaces.gerarArquivoPDF(nomeRelatorio, nomeRelatorio, parametros, empresas);
	}
	
	public void selecionarLinha(SelectEvent event){
		try {
			empresa = (Empresa)event.getObject();
			
			// Tipo Empresa
			List<Empresa_Tipo> empresa_tipos = empresa_tipoDao.listarPorEmpresa(empresa);
			for (Empresa_Tipo empresa_Tipo : empresa_tipos) {
				tipoEmpresasSelecionados.add(empresa_Tipo.getTipoEmpresa());
			}
			
			// Ramo Atividade
			ramoAtividadesSelecionados = new ArrayList<RamoAtividade>();
			List<Empresa_RamoAtividade> empresa_ramos = new ArrayList<Empresa_RamoAtividade>(); 
			empresa_ramos = empresa_ramoAtividadeDao.listarPorEmpresa(empresa);
			for (Empresa_RamoAtividade empresa_ramo : empresa_ramos) {
				ramoAtividadesSelecionados.add(empresa_ramo.getRamoAtividade());
			}
		} catch (Exception e) {
			UtilFaces.mostrarExcecao("Falha ao consultar empresa", e);
		}
	}
	
    public void onRowSelect(SelectEvent event) throws Exception {
    	empresa = (Empresa) event.getObject();
    }
    
    public List<Empresa> completeText(String razaoSocial) {
    	try {
        	empresas = new ArrayList<Empresa>();
        	empresas = empresaDao.listarPorRazaoSocialOuFantasia(razaoSocial);
        	return pegarEmpresasPermitidas(empresas);        	
		} catch (Exception e) {
			UtilFaces.falhaAoConsultar(e);
			return null;
		}

    }
    
    public void listar(){
    	try {        	
        	empresas = pegarEmpresasPermitidas(empresaDao.listar());
		} catch (Exception e) {
			UtilFaces.falhaAoConsultar(e);
		}
    }
    
    public List<Empresa> listarPorNomeFantasia(String nomeFantasia){
    	try {
        	empresas = new ArrayList<Empresa>();
        	empresas = empresaDao.listarPorNomeFantasia(nomeFantasia);
        	return pegarEmpresasPermitidas(empresas);			
		} catch (Exception e) {
			UtilFaces.mostrarExcecao("Falha ao carregar empresas", e);
			return null;
		}
    }
    
    public List<Empresa> listarPorRazaoSocialOuFantasia(String nome){
    	try {
        	empresas = new ArrayList<Empresa>();
        	empresas = empresaDao.listarPorRazaoSocialOuFantasia(nome);
        	return pegarEmpresasPermitidas(empresas);			
		} catch (Exception e) {
			UtilFaces.mostrarExcecao("Falha ao consultar empresa", e);
			return null;
		}
    }
    
    public List<Empresa> listarPorRazaoSocialOuFantasia(String nome, boolean incluirInativos){
    	try {
        	empresas = new ArrayList<Empresa>();
        	empresas = empresaDao.listarPorRazaoSocialOuFantasia(nome, incluirInativos);
        	return pegarEmpresasPermitidas(empresas);	
		} catch (Exception e) {
			UtilFaces.mostrarExcecao("Falha ao consultar empresa", e);
			return null;
		}
    }
    
    private List<Empresa> pegarEmpresasPermitidas(List<Empresa> empresas) throws Exception{
    	List<Empresa> empresasPermitidas = utilUsuario.pegarEmpresasPermitidas();
    	List<Empresa> empresasFinal = new ArrayList<Empresa>();
    	for (Empresa empresa : empresas) {
			if (empresasPermitidas.contains(empresa)) {
				empresasFinal.add(empresa);
			}
		}
    	return empresasFinal;			
    }
    
	// gets e sets	

	public List<Empresa> getEmpresas() {
		return empresas;
	}

	public void setEmpresas(List<Empresa> empresas) {
		this.empresas = empresas;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public Empresa getEmpresaSelecionada() {
		return empresaSelecionada;
	}

	public void setEmpresaSelecionada(Empresa empresaSelecionada) {
		this.empresaSelecionada = empresaSelecionada;
	}

	public List<TipoEmpresa> getTipoEmpresas() {
		return tipoEmpresas;
	}

	public void setTipoEmpresas(List<TipoEmpresa> tipoEmpresas) {
		this.tipoEmpresas = tipoEmpresas;
	}

	public List<TipoEmpresa> getTipoEmpresasSelecionados() {
		return tipoEmpresasSelecionados;
	}

	public void setTipoEmpresasSelecionados(List<TipoEmpresa> tipoEmpresasSelecionados) {
		this.tipoEmpresasSelecionados = tipoEmpresasSelecionados;
	}

	public List<RamoAtividade> getRamoAtividades() {
		return ramoAtividades;
	}

	public void setRamoAtividades(List<RamoAtividade> ramoAtividades) {
		this.ramoAtividades = ramoAtividades;
	}

	public List<RamoAtividade> getRamoAtividadesSelecionados() {
		return ramoAtividadesSelecionados;
	}

	public void setRamoAtividadesSelecionados(List<RamoAtividade> ramoAtividadesSelecionados) {
		this.ramoAtividadesSelecionados = ramoAtividadesSelecionados;
	}
}
