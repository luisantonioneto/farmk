package br.com.farmk.controle;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.primefaces.event.SelectEvent;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.annotation.SessionScope;

import br.com.farmk.entidade.Cidade;
import br.com.farmk.persistencia.CidadeDao;
import br.com.farmk.util.UtilFaces;

@Controller
@SessionScope
public class CidadeControl implements Crud, Serializable{

	private static final long serialVersionUID = 1L;
	
	@Inject
	private CidadeDao cidadeDao;
	private Cidade cidade = new Cidade();
	private Cidade cidadeSelecionada = new Cidade();
	private List<Cidade> cidades = new ArrayList<Cidade>();
	
	@Override
	@PostConstruct	
	public void carregar(){
		cidade = new Cidade();
		cidades = new ArrayList<Cidade>();
		cidades = cidadeDao.listar();
	}
	
	@Override
	public void salvar(){
		if (cidade.getNome() != "") {
			try {
				if (cidade.getCodCidade() == 0) {
					cidadeDao.salvar(cidade);					
				} else {
					cidadeDao.atualizar(cidade);
				}
				UtilFaces.salvoComSucesso("Cidade");
			} catch (Exception e) {
				UtilFaces.falhaAoSalvar(e);
			} finally{
				carregar();
			}
		} else {
			UtilFaces.mostrarAlerta("Informe o nome da cidade");
		}
	}
	
	@Override
	public void excluir(){
		try {
			cidadeDao.excluir(cidade);
			UtilFaces.excluidoComSucesso();
		} catch (Exception e) {
			UtilFaces.falhaAoExcluir(e);
		} finally{
			carregar();
		}
	}
	
	@Override
	public void novo(){
		carregar();
	}
	
	@Override
	public void selecionarLinha(SelectEvent event) {
		cidade = (Cidade)event.getObject();
	}	
	
	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	public Cidade getCidadeSelecionada() {
		return cidadeSelecionada;
	}

	public void setCidadeSelecionada(Cidade cidadeSelecionada) {
		this.cidadeSelecionada = cidadeSelecionada;
	}

	public List<Cidade> getCidades() {
		return cidades;
	}

	public void setCidades(List<Cidade> cidades) {
		this.cidades = cidades;
	}
}
