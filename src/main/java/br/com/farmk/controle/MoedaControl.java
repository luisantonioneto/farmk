package br.com.farmk.controle;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.primefaces.event.SelectEvent;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.annotation.SessionScope;

import br.com.farmk.entidade.Moeda;
import br.com.farmk.persistencia.MoedaDao;
import br.com.farmk.util.UtilFaces;

@Controller
@SessionScope
public class MoedaControl implements Crud, Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private MoedaDao moedaDao;
	private Moeda moeda = new Moeda();
	private Moeda moedaSelecionada = new Moeda();
	private List<Moeda> moedas = new ArrayList<Moeda>();

	@Override
	@PostConstruct
	public void carregar() {
		try {
			moedas = new ArrayList<Moeda>();
			moedas = moedaDao.listarPorNome();
			moeda = new Moeda();
		} catch (Exception e) {
			UtilFaces.falhaAoCarregar(e);
		}
	}

	@Override
	public void salvar() {
		try {
			if (moeda.getCodMoeda() == 0) {
				moedaDao.salvar(moeda);
			} else {
				moedaDao.atualizar(moeda);
			}
			UtilFaces.salvoComSucesso("Moeda");
		} catch (Exception e) {
			UtilFaces.falhaAoSalvar(e);
		} finally {
			carregar();
		}
	}

	@Override
	public void excluir() {
		try {
			moedaDao.excluir(moeda);
			UtilFaces.excluidoComSucesso("Moeda");
		} catch (Exception e) {
			UtilFaces.falhaAoExcluir(e);
		} finally {
			carregar();
		}
	}

	@Override
	public void novo() {
		carregar();
	}

	@Override
	public void selecionarLinha(SelectEvent event) {
		moeda = (Moeda) event.getObject();
	}

	public Moeda getMoeda() {
		return moeda;
	}

	public void setMoeda(Moeda moeda) {
		this.moeda = moeda;
	}

	public Moeda getMoedaSelecionada() {
		return moedaSelecionada;
	}

	public void setMoedaSelecionada(Moeda moedaSelecionada) {
		this.moedaSelecionada = moedaSelecionada;
	}

	public List<Moeda> getMoedas() {
		return moedas;
	}

	public void setMoedas(List<Moeda> moedas) {
		this.moedas = moedas;
	}
}