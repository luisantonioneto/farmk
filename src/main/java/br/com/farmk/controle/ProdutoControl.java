package br.com.farmk.controle;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

import org.primefaces.event.SelectEvent;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.annotation.SessionScope;

import br.com.farmk.entidade.Empresa;
import br.com.farmk.entidade.Produto;
import br.com.farmk.entidade.Produto_Uso;
import br.com.farmk.entidade.UsoProduto;
import br.com.farmk.persistencia.EmpresaDao;
import br.com.farmk.persistencia.ProdutoDao;
import br.com.farmk.persistencia.Produto_UsoDao;
import br.com.farmk.persistencia.UsoProdutoDao;
import br.com.farmk.util.UtilFaces;

@Controller
@SessionScope
public class ProdutoControl implements Crud, Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private ProdutoDao produtoDao;
	private Produto produto = new Produto();
	private Produto produtoSelecionado = new Produto();
	private List<Produto> produtos = new ArrayList<Produto>();
	
	@Inject
	private EmpresaDao empresaDao;
	private Empresa fabricanteSelecionado = new Empresa();
	private List<Empresa> fabricantes = new ArrayList<Empresa>();
	
	@Inject
	private UsoProdutoDao usoProdutoDao;
	private List<UsoProduto> usoProdutos = new ArrayList<UsoProduto>();
	private List<UsoProduto> usoProdutosSelecionados = new ArrayList<UsoProduto>();
	
	@Inject
	private Produto_UsoDao produto_UsoDao;
	

	@Override
	@PostConstruct
	public void carregar() {
		try {
			produtos = new ArrayList<Produto>();
			produtos = produtoDao.listarPorNome();
			produto = new Produto();
			
			fabricanteSelecionado = new Empresa();
			
			listarUsoProdutos();
		} catch (Exception e) {
			UtilFaces.mostrarExcecao("Falha ao carregar", e);
		}
	}
	
	@Transactional(value=TxType.REQUIRES_NEW)	
	public void listarUsoProdutos() throws Exception{
		usoProdutos = new ArrayList<UsoProduto>();
		usoProdutos = usoProdutoDao.listarPorNome();
		usoProdutosSelecionados = new ArrayList<UsoProduto>();		
	}

	@Override
	public void salvar() {
		// As variedades de produto serão subprodutos
		try {
			if (produto.getProdutoSuperior() == null || 
					(produto.getCodProduto() != produto.getProdutoSuperior().getCodProduto())) {

				// Salvar Produto
				Produto p = produtoDao.consultarPorNomeExato(produto.getNome().trim());
				if ((p == null) || (produto.getCodProduto() == p.getCodProduto())) {
					if (produto.getCodProduto() == 0) {
						produtoDao.salvar(produto);
					} else {
						produtoDao.atualizar(produto);
					}
					UtilFaces.salvoComSucesso("Produto");
					
					// Salvar Uso Produto
					try {
						for (UsoProduto usoProduto : usoProdutosSelecionados) {
							if (produto_UsoDao.buscar(produto, usoProduto) == null) {
								Produto_Uso produto_uso = new Produto_Uso();
								produto_uso.setProduto(produto);
								produto_uso.setUsoProduto(usoProduto);
								produto_UsoDao.salvar(produto_uso);		
							}
						}
						// Remover do banco os que foram desmarcados
						usoProdutos = usoProdutoDao.listar();
						usoProdutos.removeAll(usoProdutosSelecionados);
						for (UsoProduto usoProduto : usoProdutos) {
							Produto_Uso produto_uso = produto_UsoDao.buscar(produto, usoProduto); 
							if (produto_uso != null) {
								produto_UsoDao.excluir(produto_uso);
							}
						}
						UtilFaces.salvoComSucesso("Uso Produto");				
				
					} catch (Exception e) {
						UtilFaces.falhaAoSalvar(e);
					}					
				} else {
					UtilFaces.mostrarAlerta("Atenção", "Há um produto com este nome");
				}
			}
		} catch (Exception e) {
			UtilFaces.falhaAoSalvar(e);
		} finally {
			carregar();
		}
	}

	@Override
	public void excluir() {
		try {
			List<Produto_Uso> produto_usos = produto_UsoDao.listarPorProduto(produto);
			for (Produto_Uso produto_Uso : produto_usos) {
				produto_UsoDao.excluir(produto_Uso);
			}
			//		
			produtoDao.excluir(produto);
			UtilFaces.excluidoComSucesso();
		} catch (Exception e) {
			UtilFaces.falhaAoExcluir(e);
		} finally {
			carregar();
		}
	}

	@Override
	public void novo() {
		carregar();
	}

	@Override
	public void selecionarLinha(SelectEvent event) {
		try {
			produto = (Produto) event.getObject();
			fabricanteSelecionado = produto.getFabricante();
			List<Produto_Uso> produto_usos = produto_UsoDao.listarPorProduto(produto);
			usoProdutosSelecionados = new ArrayList<UsoProduto>();
			for (Produto_Uso produto_Uso : produto_usos) {
				usoProdutosSelecionados.add(produto_Uso.getUsoProduto());
			}
		} catch (Exception e) {
			UtilFaces.mostrarExcecao(e);
		}
	}
	
	public List<Produto> listarProdutos(String nome){
		try {
			produtos = new ArrayList<Produto>();
			produtos = produtoDao.listarPorNome();
			return produtos;
		} catch (Exception e) {
			UtilFaces.falhaAoConsultar(e);
			return null;
		}
	}	
	
	public void selecionarFabricante(SelectEvent event){
		fabricanteSelecionado = (Empresa) event.getObject();
		produto.setFabricante(fabricanteSelecionado);
	}
	
    public List<Empresa> buscarEmpresa(String nome) {
    	try {
    		fabricantes = new ArrayList<Empresa>();
			fabricantes = empresaDao.listarPorRazaoSocialOuFantasia(nome, false);
			return fabricantes;
		} catch (Exception e) {
			UtilFaces.falhaAoConsultar(e);
			return null;
		}
    }
    
    public void selecionarProdutoSuperior(SelectEvent event){
    	Produto produtoSuperior = (Produto) event.getObject();
    	produto.setProdutoSuperior(produtoSuperior);
    }
    
    public List<Produto> buscarProdutoSuperior(String nome){
    	try {
    		return produtoDao.consultarProdutoSuperior(nome);
		} catch (Exception e) {
			UtilFaces.falhaAoConsultar(e);
			return null;
		}
    }

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public Produto getProdutoSelecionado() {
		return produtoSelecionado;
	}

	public void setProdutoSelecionado(Produto produtoSelecionado) {
		this.produtoSelecionado = produtoSelecionado;
	}

	public List<Produto> getProdutos() {
		return produtos;
	}

	public void setProdutos(List<Produto> produtos) {
		this.produtos = produtos;
	}

	public Empresa getFabricanteSelecionado() {
		return fabricanteSelecionado;
	}

	public void setFabricanteSelecionado(Empresa fabricanteSelecionado) {
		this.fabricanteSelecionado = fabricanteSelecionado;
	}

	public List<Empresa> getFabricantes() {
		return fabricantes;
	}

	public void setFabricantes(List<Empresa> fabricantes) {
		this.fabricantes = fabricantes;
	}

	public List<UsoProduto> getUsoProdutos() {
		return usoProdutos;
	}

	public void setUsoProdutos(List<UsoProduto> usoProdutos) {
		this.usoProdutos = usoProdutos;
	}

	public List<UsoProduto> getUsoProdutosSelecionados() {
		return usoProdutosSelecionados;
	}

	public void setUsoProdutosSelecionados(List<UsoProduto> usoProdutosSelecionados) {
		this.usoProdutosSelecionados = usoProdutosSelecionados;
	}
	
}