package br.com.farmk.controle;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.primefaces.event.SelectEvent;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.annotation.SessionScope;

import br.com.farmk.entidade.Cidade;
import br.com.farmk.entidade.Empresa;
import br.com.farmk.entidade.Endereco;
import br.com.farmk.entidade.Estado;
import br.com.farmk.entidade.Pessoa;
import br.com.farmk.persistencia.CidadeDao;
import br.com.farmk.persistencia.EnderecoDao;
import br.com.farmk.persistencia.EstadoDao;
import br.com.farmk.util.UtilFaces;

@Controller
@SessionScope
public class EnderecoControl implements Crud, Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private EnderecoDao enderecoDao;
	
	@Inject
	private EstadoDao estadoDao;
	private Estado estado = new Estado();
	private List<Estado> estados = new ArrayList<Estado>();
	
	@Inject
	private CidadeDao cidadeDao;
	private Cidade cidade = new Cidade();
	private List<Cidade> cidades = new ArrayList<Cidade>();
	
	private Endereco endereco = new Endereco();
	
	@Override
	@PostConstruct
	public void carregar() {
		try {
			endereco = new Endereco();
			estado = new Estado();
			cidade = new Cidade();
			estados = estadoDao.listarPorNome();
			cidades = cidadeDao.listarPorNome();
		} catch (Exception e) {
			UtilFaces.falhaAoCarregar(e);
		}
	}
	
	@Override
	public void salvar(){
		try {
			endereco.setCidade(cidade);
			if (endereco.getCodEndereco() == 0) {
				enderecoDao.salvar(endereco);
			} else {
				enderecoDao.atualizar(endereco);
			}
			UtilFaces.salvoComSucesso("Endereço");
		} catch (Exception e) {
			UtilFaces.mostrarAlerta("Não foi possível incluir o endereço");
		}
	}
	
	@Override
	public void excluir(){
		try {
			enderecoDao.excluir(endereco);
		} catch (Exception e) {
			UtilFaces.falhaAoExcluir(e);
		}
	}

	@Override
	public void novo() {
		estado = new Estado();
		cidade = new Cidade();
		endereco = new Endereco();
	}

	@Override
	public void selecionarLinha(SelectEvent event) {
		
	}
	
	public void buscarPorPessoa(Pessoa pessoa){
		try {
			estado = new Estado();
			cidade = new Cidade();
			endereco = pessoa.getEndereco();
			if (endereco != null) {
				if (endereco.getCidade() != null) {
					cidade = endereco.getCidade();
					estado = cidade.getEstado();
					cidades = cidadeDao.listar(estado);
				}
			}
		} catch (Exception e) {
			UtilFaces.falhaAoCarregar(e);
		}
	}
	
	public void buscarPorEmpresa(Empresa empresa){
		try {
			estado = new Estado();
			cidade = new Cidade();
			endereco = empresa.getEndereco();
			if (endereco != null) {
				if (endereco.getCidade() != null) {
					cidade = endereco.getCidade();
					estado = cidade.getEstado();
					cidades = cidadeDao.listar(estado);
				}
			}
		} catch (Exception e) {
			UtilFaces.falhaAoCarregar(e);
		}
	}
	
	public void selecionarEstado(){
		if(estado != null && !estado.equals("")){
			try {
				cidades = cidadeDao.listar(estado);
			} catch (Exception e) {
				UtilFaces.falhaAoCarregar(e);
			}
		} else{
			cidades = cidadeDao.listar();
			cidade = new Cidade();
		}
	}
	
	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public List<Estado> getEstados() {
		return estados;
	}

	public void setEstados(List<Estado> estados) {
		this.estados = estados;
	}

	public List<Cidade> getCidades() {
		return cidades;
	}

	public void setCidades(List<Cidade> cidades) {
		this.cidades = cidades;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}
}