package br.com.farmk.controle;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

import org.primefaces.event.SelectEvent;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.annotation.SessionScope;

import br.com.farmk.entidade.Empresa;
import br.com.farmk.entidade.Moeda;
import br.com.farmk.entidade.Orcamento;
import br.com.farmk.entidade.Orcamento_Pessoa;
import br.com.farmk.entidade.Pessoa;
import br.com.farmk.entidade.Planejamento;
import br.com.farmk.entidade.Produto;
import br.com.farmk.persistencia.EmpresaDao;
import br.com.farmk.persistencia.MoedaDao;
import br.com.farmk.persistencia.OrcamentoDao;
import br.com.farmk.persistencia.Orcamento_PessoaDao;
import br.com.farmk.persistencia.PessoaDao;
import br.com.farmk.util.UtilFaces;
import br.com.farmk.util.UtilLog;
import br.com.farmk.util.UtilUsuario;

@Controller
@SessionScope
public class OrcamentoControl implements Crud, Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private OrcamentoDao orcamentoDao;
	private Orcamento orcamento = new Orcamento();
	private Orcamento orcamentoSelecionado = new Orcamento();
	private List<Orcamento> orcamentos = new ArrayList<Orcamento>();
	
	@Inject
	private Orcamento_PessoaDao orcamento_PessoaDao;
	
	@Inject
	private PessoaDao pessoaDao;
	private List<Pessoa> responsaveis = new ArrayList<Pessoa>();
	private List<Pessoa> responsaveisSelecionados = new ArrayList<Pessoa>();
	
	@Inject
	private EmpresaDao empresaDao;
	private Empresa clienteSelecionado = new Empresa();
	private List<Empresa> clientes = new ArrayList<Empresa>();
	
	@Inject
	private MoedaDao moedaDao;
	private Moeda moedaBRL = new Moeda();
	
	@Inject
	private PlanejamentoControl planejamentoControl;
	
	@Inject
	private PlanejamentoProdutoControl planejamentoProdutoControl;
	
	@Inject
	private UtilUsuario utilUsuario;

	@Override
	@PostConstruct
	public void carregar() {
		try {
			orcamentos = new ArrayList<Orcamento>();
			orcamentos = orcamentoDao.listarPorEmpresas(utilUsuario.pegarEmpresasPermitidas());
			orcamento = new Orcamento();
			orcamento.setDataCriacao(new Date());
			
			responsaveis = new ArrayList<Pessoa>();
			responsaveis = pessoaDao.listarPorNome();
			responsaveisSelecionados = new ArrayList<Pessoa>();
			
			clienteSelecionado = new Empresa();
			
			moedaBRL = moedaDao.buscarReal();
			if (moedaBRL == null) {
				UtilFaces.mostrarAlerta("Cadastre uma Moeda com nome Real");
			} else {
				orcamento.setMoeda(moedaBRL);
			}
		} catch (Exception e) {
			UtilLog.getLogger().error(OrcamentoControl.class, e);
			UtilFaces.falhaAoCarregar(e);
		}
	}
	
	@Transactional(value=TxType.REQUIRES_NEW)
	public void salvar(Orcamento orcamento){
		try {
			if (orcamento.getCodOrcamento() == 0) {
				orcamentoDao.salvar(orcamento);
			} else {
				orcamentoDao.atualizar(orcamento);
			}
		} catch (Exception e) {
			UtilLog.getLogger().error(OrcamentoControl.class, e);
			UtilFaces.falhaAoSalvar(e);
		}
	}

	@Override
	public void salvar() {
	}
	
	public void salvarResponsaveis(Orcamento orcamento) {
		try {
			if (responsaveisSelecionados != null) {
				for (Pessoa responsavel : responsaveisSelecionados) {
					if (orcamento_PessoaDao.buscar(orcamento, responsavel) == null) {
						Orcamento_Pessoa orcamento_pessoa = new Orcamento_Pessoa();
						orcamento_pessoa.setOrcamento(orcamento);
						orcamento_pessoa.setResponsavel(responsavel);
						orcamento_PessoaDao.salvar(orcamento_pessoa);
					}
				}
				// Excluir do banco os que foram removidos da tela
				responsaveis = pessoaDao.listarPorNome();
				responsaveis.removeAll(responsaveisSelecionados);
				for (Pessoa responsavel : responsaveis) {
					Orcamento_Pessoa orcamento_pessoa = orcamento_PessoaDao.buscar(orcamento, responsavel);
					if (orcamento_pessoa != null) {
						orcamento_PessoaDao.excluir(orcamento_pessoa);
					}
				}
			}
			UtilFaces.salvoComSucesso("Planejamento");
		} catch (Exception e) {
			UtilLog.getLogger().error(OrcamentoControl.class, e);
			UtilFaces.falhaAoSalvar(e);
		}
	}

	@Override
	public void excluir() {
		try {
			orcamentoDao.excluir(orcamento);
			UtilFaces.excluidoComSucesso();
		} catch (Exception e) {
			UtilLog.getLogger().error(OrcamentoControl.class, e);
			UtilFaces.falhaAoExcluir(e);
		} finally {
			carregar();
		}
	}
	
	public void excluirEmCascata(Orcamento orcamento) {
		try {
			orcamentoDao.excluirEmCascata(orcamento);
			UtilFaces.excluidoComSucesso();
		} catch (Exception e) {
			UtilLog.getLogger().error(OrcamentoControl.class, e);
			UtilFaces.falhaAoExcluir(e);
		} finally {
			carregar();
		}
	}

	@Override
	public void novo() {
		try {
			carregar();
			planejamentoControl.novo();
			planejamentoProdutoControl.novo();			
		} catch (Exception e) {
			UtilLog.getLogger().error(OrcamentoControl.class, e);
			UtilFaces.falhaAoCarregar(e);
		}
	}

	@Override
	public void selecionarLinha(SelectEvent event) {
		try {
			orcamento = (Orcamento) event.getObject();
			clienteSelecionado = orcamento.getCliente();
			List<Orcamento_Pessoa> orcamento_pessoas = orcamento_PessoaDao.listarPorOrcamento(orcamento);
			responsaveisSelecionados = new ArrayList<Pessoa>();
			for (Orcamento_Pessoa orcamento_pessoa : orcamento_pessoas) {
				responsaveisSelecionados.add(orcamento_pessoa.getResponsavel());
			}
			
			// Carregar planejamento
			List<Planejamento> planejamentos = planejamentoControl.listarPorOrcamento(orcamento);
			orcamento.setPlanejamentos(planejamentos);
			
			// Carregar planejamentoProdutos
			planejamentoProdutoControl.listarPorPlanejamento(planejamentoControl.getPlanejamento());
		} catch (Exception e) {
			UtilLog.getLogger().error(OrcamentoControl.class, e);
			UtilFaces.mostrarExcecao(e);
		}
	}
	
	public void selecionarCliente(SelectEvent event){
		clienteSelecionado = (Empresa) event.getObject();
		orcamento.setCliente(clienteSelecionado);
	}
	
	public List<Empresa> buscarEmpresa(String nome) {
		try {
			clientes = new ArrayList<>();
			clientes = empresaDao.listarPorRazaoSocialOuFantasia(nome, false);
			return clientes;
		} catch (Exception e) {
			UtilLog.getLogger().error(OrcamentoControl.class, e);
			UtilFaces.falhaAoConsultar(e);
			return null;
		}
	}
	
	public void buscarPorPlanejamento(Planejamento planejamento){
		try {
			if ((planejamento != null) && (planejamento.getOrcamento() != null)) {
				orcamento = planejamento.getOrcamento();
				clienteSelecionado = orcamento.getCliente();
				List<Orcamento_Pessoa> orcamento_pessoas = orcamento_PessoaDao.listarPorOrcamento(orcamento);
				responsaveisSelecionados = new ArrayList<Pessoa>();
				for (Orcamento_Pessoa orcamento_pessoa : orcamento_pessoas) {
					responsaveisSelecionados.add(orcamento_pessoa.getResponsavel());
				}
				adicionarPlanejamento();
			}
		} catch (Exception e) {
			UtilLog.getLogger().error(OrcamentoControl.class, e);
			UtilFaces.mostrarExcecao(e);
		}
	}
	
	private void adicionarPlanejamento(){
		Planejamento planejamentoNovo = new Planejamento();
		Produto culturaNova = new Produto();
		culturaNova.setNome("+ cultura");
		planejamentoNovo.setCultura(culturaNova);
		orcamento.addPlanejamento(planejamentoNovo);
	}
	
	public void mostrarAlerta(){
		if ((orcamento.getCotacaoMoeda() == null) || (orcamento.getCotacaoMoeda().compareTo(BigDecimal.ZERO) == 0)) {
			UtilFaces.mostrarAlerta("Atenção", "Informe o PTAX");
		}
	}
	
	/*
	 * gets e sets
	 */

	public Orcamento getOrcamento() {
		return orcamento;
	}

	public void setOrcamento(Orcamento orcamento) {
		this.orcamento = orcamento;
	}

	public Orcamento getOrcamentoSelecionado() {
		return orcamentoSelecionado;
	}

	public void setOrcamentoSelecionado(Orcamento orcamentoSelecionado) {
		this.orcamentoSelecionado = orcamentoSelecionado;
	}

	public List<Orcamento> getOrcamentos() {
		return orcamentos;
	}

	public void setOrcamentos(List<Orcamento> orcamentos) {
		this.orcamentos = orcamentos;
	}

	public List<Pessoa> getResponsaveis() {
		return responsaveis;
	}

	public void setResponsaveis(List<Pessoa> responsaveis) {
		this.responsaveis = responsaveis;
	}

	public List<Pessoa> getResponsaveisSelecionados() {
		return responsaveisSelecionados;
	}

	public void setResponsaveisSelecionados(List<Pessoa> responsaveisSelecionados) {
		this.responsaveisSelecionados = responsaveisSelecionados;
	}

	public Empresa getClienteSelecionado() {
		return clienteSelecionado;
	}

	public void setClienteSelecionado(Empresa clienteSelecionado) {
		this.clienteSelecionado = clienteSelecionado;
	}

	public List<Empresa> getClientes() {
		return clientes;
	}

	public void setClientes(List<Empresa> clientes) {
		this.clientes = clientes;
	}

	public Moeda getMoedaBRL() {
		return moedaBRL;
	}

	public void setMoedaBRL(Moeda moedaBRL) {
		this.moedaBRL = moedaBRL;
	}
}