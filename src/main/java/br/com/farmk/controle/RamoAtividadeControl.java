package br.com.farmk.controle;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.primefaces.event.SelectEvent;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.annotation.SessionScope;

import br.com.farmk.entidade.RamoAtividade;
import br.com.farmk.persistencia.RamoAtividadeDao;
import br.com.farmk.util.UtilFaces;

@Controller
@SessionScope
public class RamoAtividadeControl implements Crud, Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private RamoAtividadeDao ramoAtividadeDao;
	private RamoAtividade ramoAtividade = new RamoAtividade();
	private RamoAtividade ramoAtividadeSelecionado = new RamoAtividade();
	private List<RamoAtividade> ramoAtividades = new ArrayList<RamoAtividade>();

	@Override
	@PostConstruct
	public void carregar() {
		try {
			ramoAtividade = new RamoAtividade();
			ramoAtividades = new ArrayList<RamoAtividade>();
			ramoAtividades = ramoAtividadeDao.listarPorNome();
		} catch (Exception e) {
			UtilFaces.falhaAoCarregar(e);
		}
	}

	@Override
	public void salvar() {
		try {
			if (ramoAtividade.getCodRamoAtividade() == 0) {
				ramoAtividadeDao.salvar(ramoAtividade);
			} else {
				ramoAtividadeDao.atualizar(ramoAtividade);
			}
			UtilFaces.salvoComSucesso("Ramo Atividade");
		} catch (Exception e) {
			UtilFaces.falhaAoSalvar(e);
		} finally {
			carregar();
		}
	}

	@Override
	public void excluir() {
		try {
			ramoAtividadeDao.excluir(ramoAtividade);
			UtilFaces.excluidoComSucesso("Ramo atividade");
		} catch (Exception e) {
			UtilFaces.falhaAoExcluir(e);
		} finally {
			carregar();
		}
	}

	@Override
	public void novo() {
		carregar();
	}

	@Override
	public void selecionarLinha(SelectEvent event) {
		ramoAtividade = (RamoAtividade) event.getObject();
	}
	
	public List<RamoAtividade> listarPorNome(String nome){
		try {
			return ramoAtividadeDao.listarPorNome(nome);			
		} catch (Exception e) {
			UtilFaces.falhaAoConsultar(e);
			return null;
		}
	}
	
	
	
	/*
	 * gets e sets
	 */

	public RamoAtividade getRamoAtividade() {
		return ramoAtividade;
	}

	public void setRamoAtividade(RamoAtividade ramoAtividade) {
		this.ramoAtividade = ramoAtividade;
	}

	public RamoAtividade getRamoAtividadeSelecionado() {
		return ramoAtividadeSelecionado;
	}

	public void setRamoAtividadeSelecionado(RamoAtividade ramoAtividadeSelecionado) {
		this.ramoAtividadeSelecionado = ramoAtividadeSelecionado;
	}

	public List<RamoAtividade> getRamoAtividades() {
		return ramoAtividades;
	}

	public void setRamoAtividades(List<RamoAtividade> ramoAtividades) {
		this.ramoAtividades = ramoAtividades;
	}
}