package br.com.farmk.controle;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.primefaces.event.SelectEvent;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.annotation.SessionScope;

import br.com.farmk.entidade.Safra;
import br.com.farmk.persistencia.SafraDao;
import br.com.farmk.util.UtilFaces;

@Controller
@SessionScope
public class SafraControl implements Crud, Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private SafraDao safraDao;
	private Safra safra = new Safra();
	private Safra safraSelecionada = new Safra();
	private List<Safra> safras = new ArrayList<Safra>();

	@Override
	@PostConstruct
	public void carregar() {
		try {
			safras = new ArrayList<Safra>();
			safras = safraDao.listarPorNome();
			safra = new Safra();
		} catch (Exception e) {
			UtilFaces.falhaAoCarregar(e);
		}
	}

	@Override
	public void salvar() {
		try {
			if (safra.getCodSafra() == 0) {
				safraDao.salvar(safra);			
			} else {
				safraDao.atualizar(safra);
			}
			UtilFaces.salvoComSucesso("Safra");
		} catch (Exception e) {
			UtilFaces.falhaAoSalvar(e);
		} finally {
			carregar();
		}
	}

	@Override
	public void excluir() {
		try {
			safraDao.excluir(safra);
			UtilFaces.excluidoComSucesso();
		} catch (Exception e) {
			UtilFaces.falhaAoExcluir(e);
		} finally {
			carregar();
		}
	}

	@Override
	public void novo() {
		carregar();
	}

	@Override
	public void selecionarLinha(SelectEvent event) {
		safra = (Safra) event.getObject();
	}

	public Safra getSafra() {
		return safra;
	}

	public void setSafra(Safra safra) {
		this.safra = safra;
	}

	public Safra getSafraSelecionada() {
		return safraSelecionada;
	}

	public void setSafraSelecionada(Safra safraSelecionada) {
		this.safraSelecionada = safraSelecionada;
	}

	public List<Safra> getSafras() {
		return safras;
	}

	public void setSafras(List<Safra> safras) {
		this.safras = safras;
	}
}