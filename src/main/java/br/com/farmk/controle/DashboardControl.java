package br.com.farmk.controle;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.annotation.SessionScope;

import br.com.farmk.entidade.Empresa;
import br.com.farmk.entidade.Orcamento;
import br.com.farmk.entidade.Planejamento;
import br.com.farmk.entidade.PlanejamentoProduto;
import br.com.farmk.entidade.Produto;
import br.com.farmk.entidade.Safra;
import br.com.farmk.entidade.UsoProduto;
import br.com.farmk.persistencia.OrcamentoDao;
import br.com.farmk.persistencia.PlanejamentoDao;
import br.com.farmk.persistencia.PlanejamentoProdutoDao;
import br.com.farmk.util.UtilFaces;

@Controller
@SessionScope
public class DashboardControl implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private EmpresaControl empresaControl;
	private Empresa empresa;
	private List<Empresa> empresas = new ArrayList<Empresa>();
	
	@Inject
	private OrcamentoDao orcamentoDao;
	private List<Orcamento> orcamentos = new ArrayList<Orcamento>();
	
	@Inject
	private PlanejamentoProdutoDao planejamentoProdutoDao;
	
	@Inject
	private SafraControl safraControl;
	private Safra safra;
	private Produto cultura;
	
	@Inject
	private PlanejamentoControl planejamentoControl;
	
	@Inject
	private PlanejamentoDao planejamentoDao;
	private List<Planejamento> planejamentos = new ArrayList<Planejamento>();;
	
	private BarChartModel totaisPorCultura = new BarChartModel();
	private BarChartModel acumuladoEntreSafras = new BarChartModel();
	private BarChartModel receitaDespesaPorCultura = new BarChartModel();
	
	private final String VERMELHO = "ff0000";
	private final String AZUL = "0099ff";
	private final String VERDE = "00cc00";
	private final String AMARELO = "ffff00";
	private final String MARROM = "663300";
	
	@PostConstruct
	public void carregar(){
		try {
			empresaControl.carregar();
			empresas = empresaControl.getEmpresas();
			if ((empresas != null) && (!empresas.isEmpty())) {
				orcamentos = orcamentoDao.listarPorEmpresas(empresas);				
			}
			
			safraControl.carregar();
			planejamentoControl.carregar();

			// Apenas vai gerar o gráfico se tiver selecionado Empresa, Safra e Cultura
			totaisPorCultura = new BarChartModel();
			acumuladoEntreSafras = new BarChartModel();
			receitaDespesaPorCultura = new BarChartModel();
			if ((empresa != null) && (safra != null) && (cultura != null)) {
				planejamentos = planejamentoDao.listarPorEmpresaSafraCultura(empresa, safra, cultura);		
				planejamentos.forEach(p -> {
					try {
						p.setPlanejamentoProdutos(planejamentoProdutoDao.listarPorPlanejamento(p));
					} catch (Exception e) {
						UtilFaces.falhaAoCarregar(e);
					}
				});
				totaisPorCultura = montarGraficoTotaisPorClasse();
				receitaDespesaPorCultura = montarGraficoReceitaDespesas();
			}
		} catch (Exception e) {
			UtilFaces.falhaAoCarregar(e);
		}
	}
	
	public BarChartModel montarGraficoTotaisPorClasse() {
		try {
			BarChartModel model = new BarChartModel();
			BigDecimal totalPorCategoria = BigDecimal.ZERO;
			//
			for (Planejamento planejamento : planejamentos) {
				CalculoPlanejamento calculo = new CalculoPlanejamento().calcularTudo(
						planejamento.getOrcamento(),
						planejamento.getPlanejamentoProdutos());
				
				List<UsoProduto> categorias = new ArrayList<UsoProduto>();
				for (PlanejamentoProduto	planejamentoProduto : planejamento.getPlanejamentoProdutos()) {
					UsoProduto categoria = planejamentoProduto.getProduto().getProduto_usos().get(0).getUsoProduto();
					if (!categorias.contains(categoria)) {
						categorias.add(categoria);
					}
				}
				
				for (UsoProduto categoria : categorias) {
					totalPorCategoria = calculo.getTotalPorCategoria().get(categoria);
					ChartSeries classe = new ChartSeries(categoria.getNome());
					classe.set("Classes", totalPorCategoria);
					model.addSeries(classe);

				}
			} 

			String cores = VERDE + ", " + AMARELO + ", " +  AZUL + ", " + VERMELHO + ", " + MARROM;
			model.setSeriesColors(cores);
			model.setNegativeSeriesColors(VERMELHO);
			model.setTitle("Totais por classe");
			model.setLegendPosition("ne");
			Axis yAxis = model.getAxis(AxisType.Y);
			yAxis.setTickFormat("R$ %'.2f");
			return model;			
		} catch (Exception e) {
			UtilFaces.mostrarExcecao("Falha ao Montar Gráfico", e);
			return null;
		}
	}
	
	public BarChartModel montarGraficoReceitaDespesas() {
		try {
			BarChartModel model = new BarChartModel();
			ChartSeries receitas = new ChartSeries("Receitas (R$)");
			ChartSeries despesas = new ChartSeries("Despesas (R$)");
			ChartSeries margem = new ChartSeries("Margem (R$)");
			for (Planejamento planejamento : planejamentos) {
				CalculoPlanejamento calculo = new CalculoPlanejamento().calcularTudo(
						planejamento.getOrcamento(), 
						planejamento.getPlanejamentoProdutos());
				receitas.set(planejamento.getOrcamento().getSafra().getNome(), calculo.getReceitaTotalBrl());
				despesas.set(planejamento.getOrcamento().getSafra().getNome(), calculo.getTotalOperacionalEInsumosBrl());
				margem.set(planejamento.getOrcamento().getSafra().getNome(), calculo.getReceitaLiquidaBrl());
			}
			model.addSeries(receitas);
			model.addSeries(despesas);
			model.addSeries(margem);
			String cores = AZUL + ", " + VERMELHO + ", " + VERDE;
			model.setSeriesColors(cores);
			model.setNegativeSeriesColors(cores);
			model.setTitle("Receita x Despesa");
			model.setLegendPosition("ne");
			Axis yAxis = model.getAxis(AxisType.Y);
			yAxis.setTickFormat("R$%'.2f");
			return model;			
		} catch (Exception e) {
			UtilFaces.mostrarExcecao("Falha ao Montra Gráfico", e);
			return null;
		}
	}
	
	/*
	 * gets e sets 
	 */
	
	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	public Safra getSafra() {
		return safra;
	}

	public void setSafra(Safra safra) {
		this.safra = safra;
	}

	public Produto getCultura() {
		return cultura;
	}

	public void setCultura(Produto cultura) {
		this.cultura = cultura;
	}

	public List<Empresa> getEmpresas() {
		return empresas;
	}

	public void setEmpresas(List<Empresa> empresas) {
		this.empresas = empresas;
	}

	public List<Orcamento> getOrcamentos() {
		return orcamentos;
	}

	public void setOrcamentos(List<Orcamento> orcamentos) {
		this.orcamentos = orcamentos;
	}

	public List<Planejamento> getPlanejamentos() {
		return planejamentos;
	}

	public void setPlanejamentos(List<Planejamento> planejamentos) {
		this.planejamentos = planejamentos;
	}

	public BarChartModel getTotaisPorCultura() {
		return totaisPorCultura;
	}

	public BarChartModel getAcumuladoEntreSafras() {
		return acumuladoEntreSafras;
	}

	public void setAcumuladoEntreSafras(BarChartModel acumuladoEntreSafras) {
		this.acumuladoEntreSafras = acumuladoEntreSafras;
	}

	public void setTotaisPorCultura(BarChartModel totaisPorCultura) {
		this.totaisPorCultura = totaisPorCultura;
	}

	public BarChartModel getReceitaDespesaPorCultura() {
		return receitaDespesaPorCultura;
	}

	public void setReceitaDespesaPorCultura(BarChartModel receitaDespesaPorCultura) {
		this.receitaDespesaPorCultura = receitaDespesaPorCultura;
	}
}
