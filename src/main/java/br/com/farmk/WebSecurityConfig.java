package br.com.farmk;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private DataSource dataSource;
    
	@Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .authorizeRequests()
                .antMatchers("/login.jsf", "/javax.faces.resource/**").permitAll()
        		//.antMatchers("/pessoa.jsf").hasAnyRole("ADMINISTRADOR", "CADASTRO")
                .anyRequest().authenticated()
                .and()
            .formLogin()
                .loginPage("/login.jsf")
                .loginProcessingUrl("/login")
                .usernameParameter("username")
                .passwordParameter("password")
                .successForwardUrl("/index.jsf")
                .defaultSuccessUrl("/index.jsf", true)
                .and()
            .logout()
            	.logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
            	.logoutSuccessUrl("/login.jsf")
            	.and()
            .exceptionHandling().accessDeniedPage("/acessoNegado.jsf")
            	.and()
            .csrf().disable();
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth
            .inMemoryAuthentication()
                .withUser("admin").password("98765").roles("ADMINISTRADOR", "USER");
    }
    
	@Autowired
	public void configAuthentication(AuthenticationManagerBuilder auth) throws Exception {
		String consultaPermissao = "SELECT u.login, r.nomeregra FROM usuario u "
				+ "INNER JOIN permissao p "
				+ "ON p.codperfilusuario = u.codperfil "
				+ "INNER JOIN regra r "
				+ "ON r.codregra = p.codregra "
				+ "WHERE u.login = ?";
		//
		auth
			.jdbcAuthentication()
				.dataSource(dataSource)
				.passwordEncoder(passwordEncoder())
				.usersByUsernameQuery("SELECT login, senha, ativo FROM usuario WHERE login=?")
				.authoritiesByUsernameQuery(consultaPermissao);
	}
    
	@Bean
	public PasswordEncoder passwordEncoder(){
		return new BCryptPasswordEncoder();
	}
}
