package br.com.farmk.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.hibernate.exception.ConstraintViolationException;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;

public class UtilFaces {
	
	public static FacesContext context;
	private static ServletContext scontext;
	
	public static void carregarContext(){
		context = FacesContext.getCurrentInstance();
	}
	
	public static void mostrarExcecao(Exception e){
		try {
			carregarContext();
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro", e.getMessage()));			
		} catch (Exception e2) {
		}
	}
	
	public static void mostrarExcecao(String titulo, Exception e){
		try {
			carregarContext();
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, titulo, e.getMessage()));			
		} catch (Exception e2) {
			mostrarExcecao(e2);
		}
	}
	
	public static void mostrarAlerta(String message){
		carregarContext();
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Atenção", message));
	}
	
	public static void mostrarAlerta(String titulo, String message){
		carregarContext();
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, titulo, message));
	}
	
	public static void mostrarMensagem(String titulo){
		carregarContext();
		context.addMessage(null, new FacesMessage(titulo, ""));
	}
	
	public static void salvoComSucesso(){
		carregarContext();
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Salvo com sucesso", ""));
	}
	
	public static void salvoComSucesso(String titulo){
		carregarContext();
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, titulo, "Salvo com sucesso"));
	}	
	
	public static void excluidoComSucesso(){
		carregarContext();
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Excluido com sucesso", ""));
	}
	
	public static void excluidoComSucesso(String titulo){
		carregarContext();
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, titulo, "Excluido com sucesso"));
	}	
	
	public static void falhaAoCarregar(Exception e){
		carregarContext();
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Não foi possível carregar", e.getMessage()));
	}
	
	public static void falhaAoConsultar(Exception e){
		carregarContext();
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Não foi possível consultar", e.getMessage()));
	}
	
	public static void falhaAoSalvar(Exception e){
		carregarContext();
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Não foi possivel salvar", e.getMessage()));
	}
	
	public static void falhaAoExcluir(Exception e){
		carregarContext();
		String mensagem = "";
		if (e.getCause().getClass().equals(ConstraintViolationException.class)) {
			mensagem = "Podem haver cadastros que dependem deste";			
		} else {
			mensagem = e.getMessage();
		}
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Não foi possível excluir", mensagem));
	}
	
	public static void incluidoComSucesso(String titulo){
		carregarContext();
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, titulo, "Incluído com sucesso"));
	}		
	
	//-----------------------------------------------------------------------------------------------------------//
	
	public static void gerarArquivoPDF(String nomeRelatorio, String nomeSaida, Map<String, Object> parametros, List<?> lista){
		try {
			scontext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
			compilarRelatorio(nomeRelatorio);
			String arquivo = scontext.getRealPath("/relatorios/" + nomeRelatorio + ".jasper");
			// JasperRunManager.runReportToPdfFile(arquivo, "C:\\"+nomeRelatorio+".pdf", parametros, new JRBeanCollectionDataSource(lista));
			
			JasperPrint jasperPrint = JasperFillManager.fillReport(arquivo, parametros, 					
					new JRBeanCollectionDataSource(lista));	
			//
		    FacesContext fc = FacesContext.getCurrentInstance();
		    ExternalContext ec = fc.getExternalContext();
		    
			String pdfName = "/"+nomeRelatorio+".pdf";
			String pdfPath = ec.getRealPath(pdfName);
			JasperExportManager.exportReportToPdfFile(jasperPrint, pdfPath);

		    ec.responseReset(); // Some JSF component library or some Filter might have set some headers in the buffer beforehand. We want to get rid of them, else it may collide.
		    ec.setResponseContentType(ec.getMimeType(pdfPath)); // Check http://www.iana.org/assignments/media-types for all types. Use if necessary ExternalContext#getMimeType() for auto-detection based on filename.
		    // ec.setResponseContentLength(contentLength); // Set it with the file size. This header is optional. It will work if it's omitted, but the download progress will be unknown.
		    ec.setResponseHeader("Content-Disposition", "attachment; filename=\"" + nomeSaida+".pdf\""); // The Save As popup magic is done here. You can give it any file name you want, this only won't work in MSIE, it will use current request URL as file name instead.
		    
		    InputStream input = new FileInputStream(pdfPath);
		    OutputStream output = ec.getResponseOutputStream();
		    IOUtils.copy(input, output);
		    
		    fc.responseComplete(); // Important! Otherwise JSF will attempt to render the response which obviously will fail since it's already written with a file and closed.
		} catch (Exception e) {
			UtilFaces.mostrarExcecao("Falha ao gerar relatório " + nomeRelatorio, e);
		}			
	}	
	
	private static JasperReport compilarRelatorio(String fileName) throws JRException{
		File reportFile = new File(scontext.getRealPath("/relatorios/" + fileName + ".jasper"));
        // If compiled file is not found, then compile XML template
        if (!reportFile.exists()) {
        	JasperCompileManager.compileReportToFile(
        			scontext.getRealPath("/relatorios/" + fileName + ".jrxml"), 
        			scontext.getRealPath("/relatorios/" + fileName + ".jasper"));
        }
        JasperReport jasperReport = (JasperReport) JRLoader.loadObjectFromFile(reportFile.getPath());
        return jasperReport;
    }
}
