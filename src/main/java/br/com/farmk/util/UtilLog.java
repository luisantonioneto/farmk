package br.com.farmk.util;

import java.io.Serializable;

import org.apache.log4j.Logger;

public class UtilLog implements Serializable {

	private static final long serialVersionUID = 1L;

	private static Logger logger = Logger.getRootLogger();
	
	public static Logger getLogger() {
		return logger;
	}
}
