package br.com.farmk.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import br.com.farmk.entidade.Empresa;
import br.com.farmk.entidade.EmpresasPermitidas;
import br.com.farmk.entidade.Permissao;
import br.com.farmk.entidade.Regra;
import br.com.farmk.entidade.Usuario;
import br.com.farmk.persistencia.EmpresaDao;
import br.com.farmk.persistencia.EmpresasPermitidasDao;
import br.com.farmk.persistencia.PermissaoDao;
import br.com.farmk.persistencia.RegraDao;
import br.com.farmk.persistencia.UsuarioDao;

@Component
public class UtilUsuario implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Usuario usuarioLogado;

	@Inject
	private UsuarioDao usuarioDao;
	
	@Inject
	private PermissaoDao permissaoDao;
	
	@Inject
	private RegraDao regraDao;
	
	@Inject 
	private EmpresasPermitidasDao empresasPermitidasDao;
	
	@Inject
	private EmpresaDao empresaDao;

	public Usuario pegarUsuarioLogado() throws Exception{
		if (SecurityContextHolder.getContext().getAuthentication() != null) {
			Object usuarioLogado = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			String username;
			if (usuarioLogado  instanceof UserDetails ) {
			   username = ( (UserDetails)usuarioLogado).getUsername();
			} else {
			   username = usuarioLogado .toString();
			}
			return usuarioDao.buscarPorNome(username);			
		} else {
			return null;
		}
	}
	
	public List<Regra> pegarRegrasUsuarioLogado() throws Exception{
		List<Permissao> permissoes = permissaoDao.listar(usuarioLogado);
		List<Regra> regras = new ArrayList<Regra>();
		for (Permissao permissao : permissoes) {
			regras.add(permissao.getRegra());
		}
		return regras;
	}
	
	public Regra pegarRegraAdministrador() throws Exception{
		return regraDao.buscarPorNome("Administrador");
	}
	
	public List<Empresa> pegarEmpresasPermitidas() throws Exception{
		usuarioLogado = pegarUsuarioLogado();
		if (usuarioLogado != null) {
			List<Empresa> empresas = new ArrayList<Empresa>();
			if (pegarRegrasUsuarioLogado().contains(pegarRegraAdministrador())) {
				empresas = empresaDao.listar();
			} else {
				List<EmpresasPermitidas> empresasPermitidas = empresasPermitidasDao.listarPorUsuario(usuarioLogado);
				empresasPermitidas = empresasPermitidasDao.listarPorUsuario(usuarioLogado);
				for (EmpresasPermitidas empresaPermitida : empresasPermitidas) {
					empresas.add(empresaPermitida.getEmpresa());
				}				
			}
			return empresas;			
		} else {
			return new ArrayList<Empresa>();
		}
	}
}
