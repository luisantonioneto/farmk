package br.com.farmk.persistencia;

import java.util.List;

import br.com.farmk.entidade.PerfilUsuario;
import br.com.farmk.entidade.Permissao;
import br.com.farmk.entidade.Regra;
import br.com.farmk.entidade.Usuario;

public interface PermissaoDao extends PersistenciaDao<Permissao> {
	
	public Permissao buscarPorNome(String nome) throws Exception;
	
	public Permissao buscarPorObjeto(Permissao entity) throws Exception;
	
	public Permissao buscarPorPerfilERegra(PerfilUsuario perfil, Regra regra) throws Exception;

	public List<Permissao> listar(Usuario usuario) throws Exception;
	
	public List<Permissao> listar(List<Usuario> usuarios) throws Exception;
	
	public List<Permissao> listar(PerfilUsuario perfil) throws Exception;
	
	public List<Regra> listarRegrasPorPerfil(PerfilUsuario perfil) throws Exception;
}
