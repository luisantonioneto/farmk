package br.com.farmk.persistencia;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import br.com.farmk.entidade.Regra;

@Repository
public class RegraDaoJpa extends PersistenciaDaoJpa<Regra> implements RegraDao {
	
	@PersistenceContext
	private EntityManager em;

	@Override
	public Regra buscarPorNome(String nome) throws Exception {
		try {
			String consulta = "SELECT r FROM Regra r "
					+ "WHERE UPPER(r.nome) LIKE :nome";
			TypedQuery<Regra> query = em.createQuery(consulta, Regra.class);
			query.setParameter("nome", nome.toUpperCase() + '%');
			return query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		} catch (Exception e) {
			throw new Exception(e);
		}
	}

}
