package br.com.farmk.persistencia;

import br.com.farmk.entidade.TipoEmpresa;

public interface TipoEmpresaDao extends PersistenciaDao<TipoEmpresa> {

	public TipoEmpresa buscarPorNome(String nome) throws Exception;
	
	public TipoEmpresa buscarPorObjeto(TipoEmpresa tipoEmpresa) throws Exception;
}
