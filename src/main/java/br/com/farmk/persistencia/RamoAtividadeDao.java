package br.com.farmk.persistencia;

import java.util.List;

import br.com.farmk.entidade.RamoAtividade;

public interface RamoAtividadeDao extends PersistenciaDao<RamoAtividade> {

	List<RamoAtividade> listarPorNome(String nome) throws Exception;

}
