package br.com.farmk.persistencia;

import java.util.List;

import br.com.farmk.entidade.Tecnologia;

public interface TecnologiaDao extends PersistenciaDao<Tecnologia> {
	
	public List<Tecnologia> listarPorNome(String nome) throws Exception;
	
	public Tecnologia buscarTipoAreaTotal();

}
