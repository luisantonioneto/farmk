package br.com.farmk.persistencia;

import br.com.farmk.entidade.Moeda;

public interface MoedaDao extends PersistenciaDao<Moeda> {

	Moeda buscarReal() throws Exception;
	
	Moeda buscarDolar() throws Exception;
}
