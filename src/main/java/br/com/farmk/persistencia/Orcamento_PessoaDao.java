package br.com.farmk.persistencia;

import java.util.List;

import br.com.farmk.entidade.Orcamento;
import br.com.farmk.entidade.Orcamento_Pessoa;
import br.com.farmk.entidade.Pessoa;

public interface Orcamento_PessoaDao extends PersistenciaDao<Orcamento_Pessoa> {

	public List<Orcamento_Pessoa> listarPorOrcamento(Orcamento orcamento) throws Exception;
	
	public Orcamento_Pessoa buscar(Orcamento orcamento, Pessoa responsavel) throws Exception;
}
