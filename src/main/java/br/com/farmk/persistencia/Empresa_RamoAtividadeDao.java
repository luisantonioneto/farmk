package br.com.farmk.persistencia;

import java.util.List;

import br.com.farmk.entidade.Empresa;
import br.com.farmk.entidade.Empresa_RamoAtividade;
import br.com.farmk.entidade.RamoAtividade;

public interface Empresa_RamoAtividadeDao extends PersistenciaDao<Empresa_RamoAtividade> {
	
	public List<Empresa_RamoAtividade> listarPorEmpresa(Empresa empresa) throws Exception;
	
	public Empresa_RamoAtividade buscar(Empresa empresa, RamoAtividade ramoAtividade) throws Exception;

}
