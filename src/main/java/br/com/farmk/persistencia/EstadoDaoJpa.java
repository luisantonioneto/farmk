package br.com.farmk.persistencia;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import br.com.farmk.entidade.Estado;

@Repository
public class EstadoDaoJpa extends PersistenciaDaoJpa<Estado> implements EstadoDao {
	
	@PersistenceContext
	private EntityManager em;
	
	@Override
	public Estado buscarPorNome(String nome) throws Exception{
		String consulta = "SELECT e FROM Estado e "
				+ "WHERE e.nome = :nome";
		TypedQuery<Estado> query = em.createQuery(consulta, Estado.class);
		query.setParameter("nome", nome);
		try {
			return query.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public Estado buscarPorObjeto(Estado estado) throws Exception {
		try {
			if(estado != null){
				if (em == null) {
					em = getEntityManager();
				}
				return em.find(Estado.class, estado.getCodEstado());
			} else {
				return null;
			}
		} catch (Exception e) {
			throw new Exception(e);
		}
	}
}
