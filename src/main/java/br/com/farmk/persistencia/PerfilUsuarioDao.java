package br.com.farmk.persistencia;

import br.com.farmk.entidade.PerfilUsuario;

public interface PerfilUsuarioDao extends PersistenciaDao<PerfilUsuario> {

}
