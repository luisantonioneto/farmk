package br.com.farmk.persistencia;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import br.com.farmk.entidade.Produto;
import br.com.farmk.entidade.UsoProduto;

@Repository
public class ProdutoDaoJpa extends PersistenciaDaoJpa<Produto> implements ProdutoDao {
	
	@PersistenceContext
	private EntityManager em;

	@Override
	public List<Produto> listarPorUsoProduto(UsoProduto usoProduto) throws Exception {
		try {
			String consulta = "SELECT p FROM Produto p "
					+ "JOIN p.produto_usos pu "
					+ "JOIN pu.usoProduto u "
					+ "WHERE u = :usoProduto "
					+ "ORDER BY p.nome";
			TypedQuery<Produto> query = em.createQuery(consulta, Produto.class);
			query.setParameter("usoProduto", usoProduto);
			return query.getResultList();
		} catch (Exception e) {
			throw new Exception(e);
		}
	}

	@Override
	public List<Produto> consultarProdutoSuperior(String nome) throws Exception {
		String consulta = "SELECT p FROM Produto p "
				+ "WHERE UPPER(p.nome) LIKE :nome "
				+ "AND p.produtoSuperior IS NULL "
				+ "ORDER BY p.nome";
		TypedQuery<Produto> query = em.createQuery(consulta, Produto.class);
		query.setParameter("nome", nome.toUpperCase() + "%");
		return query.getResultList();
	}

	@Override
	public List<Produto> consultarPorNome(String nome) throws Exception {
		String consulta = "SELECT p FROM Produto p "
				+ "WHERE UPPER(p.nome) LIKE :nome "
				+ "ORDER BY p.nome";
		TypedQuery<Produto> query = em.createQuery(consulta, Produto.class);
		query.setParameter("nome", nome.toUpperCase() + "%");
		return query.getResultList();
	}

	@Override
	public Produto consultarPorNomeExato(String nome) throws Exception {
		try {
			String consulta = "SELECT p FROM Produto p "
					+ "WHERE UPPER(p.nome) LIKE :nome ";
			TypedQuery<Produto> query = em.createQuery(consulta, Produto.class);
			query.setParameter("nome", nome.toUpperCase());
			return query.getSingleResult();			
		} catch (NoResultException e) {
			return null;
		} catch (Exception e) {
			throw new Exception(e);
		}
	}

}
