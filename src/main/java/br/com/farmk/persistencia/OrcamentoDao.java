package br.com.farmk.persistencia;

import java.util.List;

import br.com.farmk.entidade.Empresa;
import br.com.farmk.entidade.Orcamento;
import br.com.farmk.entidade.Safra;

public interface OrcamentoDao extends PersistenciaDao<Orcamento> {

	List<Orcamento> listarPorEmpresas(List<Empresa> empresas) throws Exception;
	
	List<Orcamento> listarPorSafra(Safra safra) throws Exception;
	
	void excluirEmCascata(Orcamento orcamento) throws Exception;
}
