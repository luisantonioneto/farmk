package br.com.farmk.persistencia;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import br.com.farmk.entidade.Tecnologia;

@Repository
public class TecnologiaDaoJpa extends PersistenciaDaoJpa<Tecnologia> implements TecnologiaDao {
	
	@PersistenceContext
	private EntityManager em;

	@Override
	public List<Tecnologia> listarPorNome(String nome) throws Exception {
		String consulta = "SELECT t FROM Tecnologia t "
				+ "WHERE UPPER(t.nome) LIKE :nome ";
		TypedQuery<Tecnologia> query = em.createQuery(consulta, Tecnologia.class);
		query.setParameter("nome", nome.toUpperCase() + "%");
		return query.getResultList();
	}

	@Override
	public Tecnologia buscarTipoAreaTotal() {
		String consulta = "SELECT t FROM Tecnologia t "
				+ "WHERE UPPER(t.nome) LIKE :nome ";
		TypedQuery<Tecnologia> query = em.createQuery(consulta, Tecnologia.class);
		query.setParameter("nome", "Total".toUpperCase());
		return query.getSingleResult();
	}

}
