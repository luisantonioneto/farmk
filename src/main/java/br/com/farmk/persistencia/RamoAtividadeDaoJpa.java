package br.com.farmk.persistencia;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import br.com.farmk.entidade.RamoAtividade;

@Repository
public class RamoAtividadeDaoJpa extends PersistenciaDaoJpa<RamoAtividade> implements RamoAtividadeDao {

	@PersistenceContext
	private EntityManager em;
	
	@Override
	public List<RamoAtividade> listarPorNome(String nome) throws Exception {
		if (nome != null) {
			try {
				String consulta = "SELECT ra FROM RamoAtividade ra "
						+ "WHERE UPPER(ra.nome) LIKE :nome "
						+ "ORDER BY ra.nome";
				TypedQuery<RamoAtividade> query = em.createQuery(consulta, RamoAtividade.class);
				query.setParameter("nome", nome.toUpperCase() + '%');
				return query.getResultList();
			} catch (Exception e) {
				throw new Exception(e);
			}
		} else {
			return null;
		}
	}

}
