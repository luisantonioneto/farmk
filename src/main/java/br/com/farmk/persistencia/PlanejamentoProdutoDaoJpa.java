package br.com.farmk.persistencia;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import br.com.farmk.entidade.Planejamento;
import br.com.farmk.entidade.PlanejamentoProduto;

@Repository
public class PlanejamentoProdutoDaoJpa extends PersistenciaDaoJpa<PlanejamentoProduto> implements PlanejamentoProdutoDao {
	
	@PersistenceContext
	private EntityManager em;

	@Override
	public List<PlanejamentoProduto> listarPorPlanejamento(Planejamento planejamento) throws Exception {
		String consulta = "SELECT p FROM PlanejamentoProduto p "
				+ "WHERE p.planejamento = :planejamento ";
		TypedQuery<PlanejamentoProduto> query = em.createQuery(consulta, PlanejamentoProduto.class);
		query.setParameter("planejamento", planejamento);
		return query.getResultList();
	}

}
