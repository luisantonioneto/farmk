package br.com.farmk.persistencia;

import br.com.farmk.entidade.Endereco;

public interface EnderecoDao extends PersistenciaDao<Endereco> {

	public Endereco buscarPorObjeto(Endereco endereco) throws Exception;
}
