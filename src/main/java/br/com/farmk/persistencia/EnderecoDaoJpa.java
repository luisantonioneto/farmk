package br.com.farmk.persistencia;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import br.com.farmk.entidade.Endereco;

@Repository
public class EnderecoDaoJpa extends PersistenciaDaoJpa<Endereco> implements EnderecoDao {
	
	@PersistenceContext
	private EntityManager em;
	
	@Override
	@Transactional
	public Endereco salvar(Endereco endereco) throws Exception {
		try {
			if(endereco.getCodEndereco() == 0){
				em.persist(endereco);
			} else {
				em.merge(endereco);
			}
			return endereco;
		} catch (Exception e) {
			throw new Exception(e);
		}
	}	

	@Override
	public Endereco buscarPorObjeto(Endereco endereco) throws Exception {
		try {
			if (endereco != null){
				return em.find(Endereco.class, endereco.getCodEndereco());				
			} else {
				return null;
			}
		} catch (Exception e) {
			throw new Exception(e);
		}
	}
}
