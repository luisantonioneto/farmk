package br.com.farmk.persistencia;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

import org.springframework.stereotype.Repository;

import br.com.farmk.entidade.Empresa;
import br.com.farmk.entidade.Orcamento;
import br.com.farmk.entidade.Planejamento;
import br.com.farmk.entidade.PlanejamentoProduto;
import br.com.farmk.entidade.Produto;
import br.com.farmk.entidade.Safra;

@Repository
public class PlanejamentoDaoJpa extends PersistenciaDaoJpa<Planejamento> implements PlanejamentoDao {
	
	@PersistenceContext
	private EntityManager em;
	
	@Inject
	private PlanejamentoProdutoDao planejamentoProdutoDao;

	@Override
	@Transactional(value=TxType.REQUIRES_NEW)
	public List<Planejamento> listar() {
		String consulta = "SELECT p FROM Planejamento p "
				+ "ORDER BY p.codPlanejamento DESC";
		TypedQuery<Planejamento> query = em.createQuery(consulta, Planejamento.class);
		return query.getResultList();
	}

	@Override
	public List<Planejamento> listarPorEmpresas(List<Empresa> empresas) throws Exception {
		String consulta = "SELECT p FROM Planejamento p "
				+ "WHERE p.orcamento.cliente IN :empresas "
				+ "ORDER BY p.codPlanejamento DESC";
		TypedQuery<Planejamento> query = em.createQuery(consulta, Planejamento.class);
		query.setParameter("empresas", empresas);
		try {
			return query.getResultList();
		} catch (NoResultException e){
			return null;
		} catch (Exception e) {
			throw new Exception(e);
		}
	}

	@Override
	public List<Planejamento> listarPorEmpresaSafraCultura(Empresa empresa, Safra safra, Produto cultura) throws Exception {
		try {
			String consulta = "SELECT p FROM Planejamento p "
					+ "WHERE p.cultura = :cultura "
					+ "AND p.orcamento.safra = :safra "
					+ "AND p.orcamento.cliente = :cliente";
			TypedQuery<Planejamento> query = em.createQuery(consulta, Planejamento.class);
			query.setParameter("safra", safra);
			query.setParameter("cultura", cultura);
			query.setParameter("cliente", empresa);
			return query.getResultList();
		} catch (NoResultException e){
			return null;
		} catch (Exception e) {
			throw new Exception(e);
		}
	}

	@Override
	public List<Planejamento> listarPorOrcamento(Orcamento orcamento) throws Exception {
		try {
			String consulta = "SELECT p FROM Planejamento p "
					+ "WHERE p.orcamento = :orcamento";
			TypedQuery<Planejamento> query = em.createQuery(consulta, Planejamento.class);
			query.setParameter("orcamento", orcamento);
			return query.getResultList();
		} catch (NoResultException e){
			return null;
		} catch (Exception e) {
			throw new Exception(e);
		}
	}

	@Override
	@Transactional(value=TxType.REQUIRES_NEW)
	public void excluirEmCascata(Planejamento planejamento) throws Exception {
		try {
			List<PlanejamentoProduto> planejamentoProdutos = planejamentoProdutoDao.listarPorPlanejamento(planejamento);
			for (PlanejamentoProduto p : planejamentoProdutos) {
				planejamentoProdutoDao.excluir(p);
			}
			excluir(planejamento);
		} catch (Exception e) {
			throw new Exception(e);
		}
	}
}