package br.com.farmk.persistencia;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import br.com.farmk.entidade.TipoEmpresa;

@Repository
public class TipoEmpresaDaoJpa extends PersistenciaDaoJpa<TipoEmpresa> implements TipoEmpresaDao {
	
	@PersistenceContext
	private EntityManager em;
	
	@Override
	public TipoEmpresa buscarPorNome(String nome) throws Exception{
		String consulta = "SELECT e FROM TipoEmpresa e "
				+ "WHERE e.nome = :nome";
		TypedQuery<TipoEmpresa> query = em.createQuery(consulta, TipoEmpresa.class);
		query.setParameter("nome", nome);
		try {
			return query.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public TipoEmpresa buscarPorObjeto(TipoEmpresa tipoEmpresa) throws Exception {
		try {
			if(tipoEmpresa != null){
				if (em == null) {
					em = getEntityManager();
				}
				return em.find(TipoEmpresa.class, tipoEmpresa.getCodTipoEmpresa());
			} else {
				return null;
			}
		} catch (Exception e) {
			throw new Exception(e);
		}
	}
}
