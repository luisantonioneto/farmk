package br.com.farmk.persistencia;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

import org.springframework.stereotype.Repository;

import br.com.farmk.entidade.Empresa;
import br.com.farmk.entidade.Orcamento;
import br.com.farmk.entidade.Orcamento_Pessoa;
import br.com.farmk.entidade.Planejamento;
import br.com.farmk.entidade.Safra;

@Repository
public class OrcamentoDaoJpa extends PersistenciaDaoJpa<Orcamento> implements OrcamentoDao {

	@PersistenceContext
	private EntityManager em;
	
	@Inject
	private PlanejamentoDao planejamentoDao;
	
	@Inject
	private Orcamento_PessoaDao orcamento_pessoaDao;
	
	@Override
	public List<Orcamento> listarPorEmpresas(List<Empresa> empresas) throws Exception {
		try {
			String consulta = "SELECT o FROM Orcamento o "
					+ "WHERE o.cliente IN (:clientes)";
			TypedQuery<Orcamento> query = em.createQuery(consulta, Orcamento.class);
			query.setParameter("clientes", empresas);
			return query.getResultList();
		} catch (NoResultException e){
			return null;
		} catch (Exception e) {
			throw new Exception(e);
		}
	}

	@Override
	public List<Orcamento> listarPorSafra(Safra safra) throws Exception {
		try {
			String consulta = "SELECT o FROM Orcamento o "
					+ "WHERE o.safra = :safra";
			TypedQuery<Orcamento> query = em.createQuery(consulta, Orcamento.class);
			query.setParameter("safra", safra);
			return query.getResultList();
		} catch (NoResultException e) {
			return null;
		} catch (Exception e) {
			throw new Exception(e);
		}
	}

	@Override
	@Transactional(value=TxType.REQUIRES_NEW)
	public void excluirEmCascata(Orcamento orcamento) throws Exception {
		try {
			List<Planejamento> planejamentos = planejamentoDao.listarPorOrcamento(orcamento);
			for (Planejamento planejamento : planejamentos) {
				planejamentoDao.excluirEmCascata(planejamento);
			}
			
			List<Orcamento_Pessoa> orcamento_pessoas = orcamento_pessoaDao.listarPorOrcamento(orcamento);
			for (Orcamento_Pessoa orcamento_Pessoa : orcamento_pessoas) {
				orcamento_pessoaDao.excluir(orcamento_Pessoa);
			}
			
			excluir(orcamento);
		} catch (Exception e) {
			throw new Exception(e);
		}
	}
}