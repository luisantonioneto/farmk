package br.com.farmk.persistencia;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

import org.springframework.stereotype.Repository;

import br.com.farmk.entidade.Pessoa;
import br.com.farmk.entidade.Usuario;

@Repository
public class UsuarioDaoJpa extends PersistenciaDaoJpa<Usuario> implements UsuarioDao {
	
	@PersistenceContext
	private EntityManager em;

	@Override
	@SuppressWarnings("unchecked")	
	public List<Usuario> listar() {
		try {
			return em.createQuery("SELECT u FROM Usuario u ORDER BY u.pessoa.nome").getResultList();			
		} catch (Exception e) {
			return null;
		}
	}
	
	@Override
	public List<Usuario> listar(String login) throws Exception {
		if(login != null){
			String consulta = "SELECT u FROM Usuario u "
					+ "WHERE u.login LIKE :login"
					+ "AND u.ativo = true";
			TypedQuery<Usuario> query = em.createQuery(consulta, Usuario.class);
			try {
				return query.getResultList();
			} catch (Exception e) {
				throw new Exception(e);
			}
		} else{
			return null;
		}
	}	
	
	@Override
	@Transactional(value=TxType.REQUIRES_NEW)
	public List<Usuario> listar(boolean ativo) throws Exception {
		String consulta = "SELECT u FROM Usuario u "
				+ "WHERE u.ativo = :ativo";
		
		return em.createQuery(consulta, Usuario.class)
				.setParameter("ativo", ativo)
				.getResultList();
	}

	@Override
	public Usuario buscarPorNome(String login) throws Exception {
		String consulta = "SELECT u FROM Usuario u "
				+ "WHERE u.login = :login";
		TypedQuery<Usuario> query = em.createQuery(consulta, Usuario.class);
		query.setParameter("login", login);
		try {
			return query.getSingleResult();
		} catch (NoResultException e) {
			return null;				
		} catch (Exception e) {
			throw new Exception(e);
		}
	}

	@Override
	public Usuario buscarPorObjeto(Usuario usuario) throws Exception {
		try {
			if(usuario != null){
				return em.find(Usuario.class, usuario.getCodUsuario());
			} else{
				return null;
			}
		} catch (Exception e) {
			throw new Exception(e);
		}
	}

	@Override
	public Usuario buscarPorObjeto(Pessoa pessoa) throws Exception {
		if(pessoa != null){
			String consulta = "SELECT u FROM Usuario u "
					+ "WHERE u.pessoa = :pessoa";
			TypedQuery<Usuario> query = em.createQuery(consulta, Usuario.class);
			query.setParameter("pessoa", pessoa);
			try {
				return query.getSingleResult();
			} catch (NoResultException e) {
				return null;				
			} catch (Exception e) {
				throw new Exception(e);
			}
		} else{
			return null;			
		}
	}

	@Override
	public Usuario consultarLoginExiste(Usuario usuario) throws Exception {
		try {
			String consulta = "SELECT u FROM Usuario u "
					+ "WHERE u.login = :login ";
			TypedQuery<Usuario> query = em.createQuery(consulta, Usuario.class);
			query.setParameter("login", usuario.getLogin());
			return query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		} catch (Exception e) {
			throw new Exception(e);
		}
	}
}
