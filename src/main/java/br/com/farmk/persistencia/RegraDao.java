package br.com.farmk.persistencia;

import br.com.farmk.entidade.Regra;

public interface RegraDao extends PersistenciaDao<Regra> {
	
	public Regra buscarPorNome(String nome) throws Exception;

}
