package br.com.farmk.persistencia;

import java.util.List;

import br.com.farmk.entidade.Empresa;
import br.com.farmk.entidade.Orcamento;
import br.com.farmk.entidade.Planejamento;
import br.com.farmk.entidade.Produto;
import br.com.farmk.entidade.Safra;

public interface PlanejamentoDao extends PersistenciaDao<Planejamento> {
	
	public List<Planejamento> listarPorEmpresas(List<Empresa> empresas) throws Exception;
	
	public List<Planejamento> listarPorEmpresaSafraCultura(Empresa empresa, 
			Safra safra, Produto cultura) throws Exception;
	
	public List<Planejamento> listarPorOrcamento(Orcamento orcamento) throws Exception;
	
	public void excluirEmCascata(Planejamento planejamento) throws Exception;
}
