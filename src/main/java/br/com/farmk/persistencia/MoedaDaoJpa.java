package br.com.farmk.persistencia;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import br.com.farmk.entidade.Moeda;

@Repository
public class MoedaDaoJpa extends PersistenciaDaoJpa<Moeda> implements MoedaDao {

	@PersistenceContext
	private EntityManager em;
	
	@Override
	public Moeda buscarReal() throws Exception {
		String consulta = "SELECT m FROM Moeda m "
				+ "WHERE UPPER(m.nome) LIKE :nome "
				+ "OR m.simbolo LIKE :simbolo";
		TypedQuery<Moeda> query = em.createQuery(consulta, Moeda.class);
		query.setParameter("nome", "REAL");
		query.setParameter("simbolo", "R$");
		try {
			return query.getSingleResult();
		} catch (NoResultException e){
			return null;
		} catch (Exception e) {
			throw new Exception(e);
		}
	}

	@Override
	public Moeda buscarDolar() throws Exception {
		String consulta = "SELECT m FROM Moeda m "
				+ "WHERE UPPER(m.nome) LIKE :nome "
				+ "OR m.simbolo LIKE :simbolo";
		TypedQuery<Moeda> query = em.createQuery(consulta, Moeda.class);
		query.setParameter("nome", "DOLAR");
		query.setParameter("simbolo", "US$");
		try {
			return query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		} catch (Exception e) {
			throw new Exception(e);
		}
	}

}
