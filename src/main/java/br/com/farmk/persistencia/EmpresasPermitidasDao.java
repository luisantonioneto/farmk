package br.com.farmk.persistencia;

import java.util.List;

import br.com.farmk.entidade.Empresa;
import br.com.farmk.entidade.EmpresasPermitidas;
import br.com.farmk.entidade.Usuario;

public interface EmpresasPermitidasDao extends PersistenciaDao<EmpresasPermitidas> {

	List<Empresa> listarEmpresasPorUsuario(Usuario usuario) throws Exception;
	
	List<EmpresasPermitidas> listarPorUsuario(Usuario usuario) throws Exception;
	
	EmpresasPermitidas buscarPorEmpresaEUsuario(Empresa empresa, Usuario usuario) throws Exception;

}
