package br.com.farmk.persistencia;

import java.util.List;

import br.com.farmk.entidade.Planejamento;
import br.com.farmk.entidade.PlanejamentoProduto;

public interface PlanejamentoProdutoDao extends PersistenciaDao<PlanejamentoProduto> {
	
	public List<PlanejamentoProduto> listarPorPlanejamento(Planejamento planejamento) throws Exception;

}
