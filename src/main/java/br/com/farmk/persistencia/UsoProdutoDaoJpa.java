package br.com.farmk.persistencia;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import br.com.farmk.entidade.UsoProduto;

@Repository
public class UsoProdutoDaoJpa extends PersistenciaDaoJpa<UsoProduto> implements UsoProdutoDao {
	
	@PersistenceContext
	private EntityManager em;

	@Override
	public UsoProduto buscarCultura() throws Exception {
		String consulta = "SELECT u FROM UsoProduto u "
				+ "WHERE UPPER(u.nome) LIKE :nome";
		TypedQuery<UsoProduto> query = em.createQuery(consulta, UsoProduto.class);
		query.setParameter("nome", "CULTURA");
		try {
			return query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		} catch (Exception e) {
			throw new Exception(e);
		}
	}

	@Override
	public UsoProduto buscarSemente() throws Exception {
		String consulta = "SELECT u FROM UsoProduto u "
				+ "WHERE UPPER(u.nome) LIKE :nome";
		TypedQuery<UsoProduto> query = em.createQuery(consulta, UsoProduto.class);
		query.setParameter("nome", "SEMENTES");
		try {
			return query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		} catch (Exception e) {
			throw e;
		}
	}

}
