package br.com.farmk.persistencia;

import java.util.List;

import br.com.farmk.entidade.Produto;
import br.com.farmk.entidade.UsoProduto;

public interface ProdutoDao extends PersistenciaDao<Produto> {
	
	public List<Produto> listarPorUsoProduto(UsoProduto usoProduto) throws Exception;
	
	public List<Produto> consultarProdutoSuperior(String nome) throws Exception;
	
	public List<Produto> consultarPorNome(String nome) throws Exception;
	
	public Produto consultarPorNomeExato(String nome) throws Exception;

}
