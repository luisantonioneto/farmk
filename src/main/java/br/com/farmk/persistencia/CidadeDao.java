package br.com.farmk.persistencia;

import java.util.List;

import br.com.farmk.entidade.Cidade;
import br.com.farmk.entidade.Estado;

public interface CidadeDao extends PersistenciaDao<Cidade> {
	
	public Cidade buscarPorNome(String nome) throws Exception;
	
	public List<Cidade> listar(Estado estado) throws Exception;
	
	public Cidade buscarPorObjeto(Cidade cidade) throws Exception;
}
