package br.com.farmk.persistencia;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import br.com.farmk.entidade.Empresa;
import br.com.farmk.entidade.Empresa_RamoAtividade;
import br.com.farmk.entidade.RamoAtividade;

@Repository
public class Empresa_RamoAtividadeDaoJpa extends PersistenciaDaoJpa<Empresa_RamoAtividade> implements Empresa_RamoAtividadeDao {
			
	@PersistenceContext
	private EntityManager em;			

	@Override
	public List<Empresa_RamoAtividade> listarPorEmpresa(Empresa empresa) throws Exception {
		if (empresa != null) {
			String consulta = "SELECT er FROM Empresa_RamoAtividade er "
					+ "WHERE er.empresa = :empresa";
			TypedQuery<Empresa_RamoAtividade> query = em.createQuery(consulta, Empresa_RamoAtividade.class);
			query.setParameter("empresa", empresa);
			try {
				return query.getResultList();
			} catch (NoResultException e){
				return new ArrayList<Empresa_RamoAtividade>();
			} catch (Exception e) {
				throw new Exception(e);
			}
		}
		return new ArrayList<Empresa_RamoAtividade>();
	}

	@Override
	public Empresa_RamoAtividade buscar(Empresa empresa, RamoAtividade ramoAtividade) throws Exception {
		if (empresa != null && ramoAtividade != null) {
			String consulta = "SELECT er FROM Empresa_RamoAtividade er "
					+ "WHERE er.empresa = :empresa "
					+ "AND er.ramoAtividade = :ramoAtividade";
			TypedQuery<Empresa_RamoAtividade> query = em.createQuery(consulta, Empresa_RamoAtividade.class);
			query.setParameter("empresa", empresa);
			query.setParameter("ramoAtividade", ramoAtividade);
			try {
				return query.getSingleResult();
			} catch (NoResultException e){
				return null;
			} catch (Exception e) {
				throw new Exception(e);
			}
		}
		return null;
	}

}
