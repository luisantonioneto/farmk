package br.com.farmk.persistencia;

import java.util.List;

import br.com.farmk.entidade.Empresa;

public interface EmpresaDao extends PersistenciaDao<Empresa> {
	
	public List<Empresa> listar(String razaoSocial) throws Exception;
	
	public List<Empresa> listarPorNomeFantasia(String nomeFantasia) throws Exception;
	
	public List<Empresa> listarPorRazaoSocialOuFantasia(String nome) throws Exception;
	
	public List<Empresa> listarPorRazaoSocialOuFantasia(String nome, boolean incluirInativos) throws Exception;
	
	public List<Empresa> listarGenerico(String nome) throws Exception;

}
