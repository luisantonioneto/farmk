package br.com.farmk.persistencia;

import br.com.farmk.entidade.UsoProduto;

public interface UsoProdutoDao extends PersistenciaDao<UsoProduto> {
	
	UsoProduto buscarCultura() throws Exception;
	
	UsoProduto buscarSemente() throws Exception;

}
