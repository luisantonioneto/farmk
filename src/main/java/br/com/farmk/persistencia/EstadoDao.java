package br.com.farmk.persistencia;

import br.com.farmk.entidade.Estado;

public interface EstadoDao extends PersistenciaDao<Estado> {

	public Estado buscarPorNome(String nome) throws Exception;
	
	public Estado buscarPorObjeto(Estado estado) throws Exception;
}
