package br.com.farmk.persistencia;

import br.com.farmk.entidade.UnidadeMedida;

public interface UnidadeMedidaDao extends PersistenciaDao<UnidadeMedida> {

}
