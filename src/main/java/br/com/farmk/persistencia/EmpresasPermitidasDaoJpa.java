package br.com.farmk.persistencia;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import br.com.farmk.entidade.Empresa;
import br.com.farmk.entidade.EmpresasPermitidas;
import br.com.farmk.entidade.Usuario;

@Repository
public class EmpresasPermitidasDaoJpa extends PersistenciaDaoJpa<EmpresasPermitidas> implements EmpresasPermitidasDao {

	@PersistenceContext
	private EntityManager em;
	
	@Override
	public List<EmpresasPermitidas> listarPorUsuario(Usuario usuario) throws Exception{
		String consulta = "SELECT ep FROM EmpresasPermitidas ep "
				+ "WHERE ep.usuario = :usuario ";
		TypedQuery<EmpresasPermitidas> query = em.createQuery(consulta, EmpresasPermitidas.class);
		query.setParameter("usuario", usuario);
		try {
			return query.getResultList();
		} catch (NoResultException e){
			return null;
		} catch (Exception e) {
			throw new Exception(e);
		}
	}
	
	@Override
	public List<Empresa> listarEmpresasPorUsuario(Usuario usuario) throws Exception {
		String consulta = "SELECT ep.empresa FROM EmpresasPermitidas ep "
				+ "WHERE ep.usuario = :usuario ";
		TypedQuery<Empresa> query = em.createQuery(consulta, Empresa.class);
		query.setParameter("usuario", usuario);
		try {
			return query.getResultList();
		} catch (NoResultException e){
			return null;
		} catch (Exception e) {
			throw new Exception(e);
		}	}	

	@Override
	public EmpresasPermitidas buscarPorEmpresaEUsuario(Empresa empresa, Usuario usuario) throws Exception {
		String consulta = "SELECT ep FROM EmpresasPermitidas ep "
				+ "WHERE ep.empresa = :empresa "
				+ "AND ep.usuario = :usuario";
		TypedQuery<EmpresasPermitidas> query = em.createQuery(consulta, EmpresasPermitidas.class);
		query.setParameter("empresa", empresa);
		query.setParameter("usuario", usuario);
		try {
			return query.getSingleResult();
		} catch (NoResultException e){
			return null;
		} catch (Exception e) {
			throw new Exception(e);
		}
	}
}