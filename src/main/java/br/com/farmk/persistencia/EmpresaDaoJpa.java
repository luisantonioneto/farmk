package br.com.farmk.persistencia;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import br.com.farmk.entidade.Empresa;

@Repository
public class EmpresaDaoJpa extends PersistenciaDaoJpa<Empresa> implements EmpresaDao {
	
	@PersistenceContext
	private EntityManager em;

	@Override	
	public Empresa salvar(Empresa empresa) throws Exception{
		try {
			if (buscarPorRazaoSocial(empresa) == null){
				if(empresa.getCodEmpresa() == 0){
					em.persist(empresa);
				} else{
					em.merge(empresa);
				}
				return empresa;
			} else{
				throw new Exception("Já existe uma empresa com esta razão social");
			}			
		} catch (Exception e) {
			throw new Exception(e);
		}
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Empresa> listar() { 
		try {
			return em.createQuery("SELECT e FROM Empresa e ORDER BY e.razaoSocial").getResultList();
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<Empresa> listar(String razaoSocial) throws Exception {
		if(razaoSocial != ""){
			String consulta = "SELECT e FROM Empresa e "
					+ "WHERE e.razaoSocial LIKE :razaoSocial "
					+ "ORDER BY e.razaoSocial";
			TypedQuery<Empresa> query = em.createQuery(consulta, Empresa.class);
			query.setParameter("razaoSocial", razaoSocial + '%');
			try {
				return query.getResultList();
			} catch (Exception e) {
				throw new Exception(e);
			}
		} else{
			return null;
		}
	}	

	@Override
	public List<Empresa> listarPorNomeFantasia(String nomeFantasia)
			throws Exception {
		if (nomeFantasia != "") {
			String consulta = "SELECT e FROM Empresa e "
					+ "WHERE e.nomeFantasia LIKE :nomeFantasia "
					+ "ORDER BY e.nomeFantasia";
			TypedQuery<Empresa> query = em.createQuery(consulta, Empresa.class);
			query.setParameter("nomeFantasia", nomeFantasia + '%');
			try {
				return query.getResultList();
			} catch (Exception e) {
				throw new Exception(e);
			}
		} else {
			return null;
		}
	}
	
	@Override
	public List<Empresa> listarPorRazaoSocialOuFantasia(String nome) throws Exception {
		if (nome != "") {
			String consulta = "SELECT e FROM Empresa e "
					+ "WHERE e.razaoSocial LIKE :razaoSocial "
					+ "OR e.nomeFantasia LIKE :nomeFantasia "
					+ "ORDER BY e.razaoSocial, e.nomeFantasia";
			TypedQuery<Empresa> query = em.createQuery(consulta, Empresa.class);
			query.setParameter("razaoSocial", "%" + nome + "%");
			query.setParameter("nomeFantasia", "%" + nome + "%");
			try {
				return query.getResultList();
			} catch (Exception e) {
				throw new Exception(e);
			}
		} else {
			return null;
		}
	}
	
	@Override
	public List<Empresa> listarPorRazaoSocialOuFantasia(String nome, boolean incluirInativos) throws Exception {
		String consulta;		
		if (!incluirInativos) {
			consulta = "SELECT e FROM Empresa e "
					+ "WHERE e.ativo = TRUE "
					+ "AND (UPPER(e.razaoSocial) LIKE :razaoSocial "
					+ "OR UPPER(e.nomeFantasia) LIKE :nomeFantasia) "
					+ "ORDER BY e.razaoSocial, e.nomeFantasia";				
		} else {
			consulta = "SELECT e FROM Empresa e "
					+ "WHERE UPPER(e.razaoSocial) LIKE :razaoSocial "
					+ "OR UPPER(e.nomeFantasia) LIKE :nomeFantasia "
					+ "ORDER BY e.razaoSocial, e.nomeFantasia";								
		}
		try {
			TypedQuery<Empresa> query = em.createQuery(consulta, Empresa.class);
			query.setParameter("razaoSocial", "%" + nome.toUpperCase() + "%");
			query.setParameter("nomeFantasia", "%" + nome.toUpperCase() + "%");			
			return query.getResultList();
		} catch (Exception e) {
			throw new Exception(e);
		}
	}	
	
	@Override
	public List<Empresa> listarGenerico(String nome) throws Exception {
		if (nome != "") {
			TypedQuery<Empresa> query;
			int codEmpresa;
			try {
				codEmpresa = Integer.valueOf(nome);				
			} catch (Exception e) {
				codEmpresa = 0;
			}
			//
			if (nome.length() >= 3) {
				String consulta = "SELECT e FROM Empresa e "
						+ "WHERE e.razaoSocial LIKE :razaoSocial "
						+ "OR e.nomeFantasia LIKE :nomeFantasia "
						+ "OR e.cnpj LIKE :cnpj "
						+ "OR e.codEmpresa = :codEmpresa "					
						+ "ORDER BY e.razaoSocial, e.nomeFantasia";
				query = em.createQuery(consulta, Empresa.class);
				query.setParameter("razaoSocial", "%" + nome + "%");
				query.setParameter("nomeFantasia", "%" + nome + "%");
				query.setParameter("cnpj", nome + "%");
				query.setParameter("codEmpresa", codEmpresa);
			} else {
				String consulta = "SELECT e FROM Empresa e "
						+ "WHERE e.cnpj LIKE :cnpj "
						+ "OR e.codEmpresa = :codEmpresa "					
						+ "ORDER BY e.razaoSocial, e.nomeFantasia";
				query = em.createQuery(consulta, Empresa.class);
				query.setParameter("cnpj", nome + "%");
				query.setParameter("codEmpresa", codEmpresa);				
			}
			try {
				return query.getResultList();
			} catch (Exception e) {
				throw new Exception(e);
			}
		} else {
			return null;
		}
	}	
	
	public Empresa buscarPorRazaoSocial(Empresa empresa){
		String consulta = "SELECT e FROM Empresa e "
				+ "WHERE e.razaoSocial = :razaoSocial "
				+ "AND e.codEmpresa <> :codEmpresa";
		TypedQuery<Empresa> query = em.createQuery(consulta, Empresa.class); 
		query.setParameter("razaoSocial", empresa.getRazaoSocial());
		query.setParameter("codEmpresa", empresa.getCodEmpresa());
		try {
			return query.getSingleResult();	
		} catch (Exception e) {
			return null;
		}
	}
	}