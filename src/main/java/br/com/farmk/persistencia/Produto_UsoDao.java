package br.com.farmk.persistencia;

import java.util.List;

import br.com.farmk.entidade.Produto;
import br.com.farmk.entidade.Produto_Uso;
import br.com.farmk.entidade.UsoProduto;

public interface Produto_UsoDao extends PersistenciaDao<Produto_Uso> {

	public List<Produto_Uso> listarPorProduto(Produto produto) throws Exception;
	
	public Produto_Uso buscar(Produto produto, UsoProduto usoProduto) throws Exception;
}
