package br.com.farmk.persistencia;

import java.util.List;

import br.com.farmk.entidade.Pessoa;
import br.com.farmk.entidade.Usuario;

public interface UsuarioDao extends PersistenciaDao<Usuario> {

	public Usuario buscarPorObjeto(Pessoa pessoa) throws Exception;
	
	public Usuario buscarPorObjeto(Usuario usuario) throws Exception;
	
	public Usuario buscarPorNome(String nome) throws Exception;
	
	public Usuario consultarLoginExiste(Usuario usuario) throws Exception;
	
	public List<Usuario> listar(String login) throws Exception;
	
	public List<Usuario> listar(boolean ativo) throws Exception;
	
	
}
