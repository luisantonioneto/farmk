package br.com.farmk.persistencia;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import br.com.farmk.entidade.Produto;
import br.com.farmk.entidade.Produto_Uso;
import br.com.farmk.entidade.UsoProduto;

@Repository
public class Produto_UsoDaoJpa extends PersistenciaDaoJpa<Produto_Uso> implements Produto_UsoDao {
	
	@PersistenceContext
	private EntityManager em;

	@Override
	public List<Produto_Uso> listarPorProduto(Produto produto) throws Exception {
		if (produto != null) {
			String consulta = "SELECT pu FROM Produto_Uso pu "
					+ "WHERE pu.produto = :produto";
			TypedQuery<Produto_Uso> query = em.createQuery(consulta, Produto_Uso.class);
			query.setParameter("produto", produto);
			try {
				return query.getResultList();
			} catch (Exception e) {
				throw new Exception(e);
			}
		}
		return null;
	}

	@Override
	public Produto_Uso buscar(Produto produto, UsoProduto usoProduto) throws Exception {
		if (produto != null && usoProduto != null) {
			String consulta = "SELECT pu FROM Produto_Uso pu "
					+ "WHERE pu.produto = :produto "
					+ "AND pu.usoProduto = :usoProduto ";
			TypedQuery<Produto_Uso> query = em.createQuery(consulta, Produto_Uso.class);
			query.setParameter("produto", produto);
			query.setParameter("usoProduto", usoProduto);
			try {
				return query.getSingleResult();
			} catch (NoResultException e) {
				return null;
			} catch (Exception e) {
				throw new Exception(e);
			}
		}
		return null;
	}

}
