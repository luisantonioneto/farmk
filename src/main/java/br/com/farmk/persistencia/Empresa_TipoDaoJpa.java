package br.com.farmk.persistencia;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import br.com.farmk.entidade.Empresa;
import br.com.farmk.entidade.Empresa_Tipo;
import br.com.farmk.entidade.TipoEmpresa;

@Repository
public class Empresa_TipoDaoJpa extends PersistenciaDaoJpa<Empresa_Tipo> implements Empresa_TipoDao {

	@PersistenceContext
	private EntityManager em;
	
	@Override
	public List<Empresa_Tipo> listarPorEmpresa(Empresa empresa) throws Exception {
		if (empresa != null) {
			String consulta = "SELECT et FROM Empresa_Tipo et "
					+ "WHERE et.empresa = :empresa";
			TypedQuery<Empresa_Tipo> query = em.createQuery(consulta, Empresa_Tipo.class);
			query.setParameter("empresa", empresa);
			try {
				return query.getResultList();
			} catch (NoResultException e){
				return new ArrayList<Empresa_Tipo>();
			} catch (Exception e) {
				throw new Exception(e);
			}
		}
		return new ArrayList<Empresa_Tipo>();		
	}

	@Override
	public Empresa_Tipo buscar(Empresa empresa, TipoEmpresa tipoEmpresa) throws Exception {
		if (empresa != null && tipoEmpresa != null) {
			String consulta = "SELECT et FROM Empresa_Tipo et "
					+ "WHERE et.empresa = :empresa "
					+ "AND et.tipoEmpresa = :tipoEmpresa";
			TypedQuery<Empresa_Tipo> query = em.createQuery(consulta, Empresa_Tipo.class);
			query.setParameter("empresa", empresa);
			query.setParameter("tipoEmpresa", tipoEmpresa);
			try {
				return query.getSingleResult();
			} catch (NoResultException e){
				return null;
			} catch (Exception e) {
				throw new Exception(e);
			}
		}
		return null;
	}

}
