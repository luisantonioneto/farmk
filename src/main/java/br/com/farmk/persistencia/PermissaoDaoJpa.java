package br.com.farmk.persistencia;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import br.com.farmk.entidade.PerfilUsuario;
import br.com.farmk.entidade.Permissao;
import br.com.farmk.entidade.Regra;
import br.com.farmk.entidade.Usuario;

@Repository
public class PermissaoDaoJpa extends PersistenciaDaoJpa<Permissao> implements PermissaoDao{
	
	@PersistenceContext
	private EntityManager em;

	@Override
	@SuppressWarnings("unchecked")		
	public List<Permissao> listar() {
		return em.createQuery("SELECT p FROM Permissao p").getResultList();
	}
	
	@Override
	public List<Permissao> listar(Usuario usuario) throws Exception {
		String consulta = "SELECT p FROM Permissao p "
				+ "WHERE p.perfilUsuario = :perfilUsuario";
		TypedQuery<Permissao> query = em.createQuery(consulta, Permissao.class);
		query.setParameter("perfilUsuario", usuario.getPerfil());
		try {
			return query.getResultList();
		} catch (Exception e) {
			throw new Exception(e);
		}
	}
	
	@Override
	public List<Permissao> listar(List<Usuario> usuarios) throws Exception {
		String consulta = "SELECT p FROM Permissao p "
				+ "WHERE p.usuario in :usuarios";
		TypedQuery<Permissao> query = em.createQuery(consulta, Permissao.class);
		query.setParameter("usuarios", usuarios);
		try {
			return query.getResultList();
		} catch (Exception e) {
			throw new Exception(e);
		}
	}

	@Override
	public Permissao buscarPorNome(String nome) throws Exception {
		String consulta = "SELECT p FROM Permissao p "
				+ "WHERE p.role LIKE :nome";
		TypedQuery<Permissao> query = em.createQuery(consulta, Permissao.class);
		try {
			return query.getSingleResult();
		} catch (Exception e) {
			throw new Exception(e);
		}
	}

	@Override
	public Permissao buscarPorObjeto(Permissao permissao) throws Exception {
		try {
			if(permissao != null){
				return em.find(Permissao.class, permissao.getCodPermissao());
			} else{
				return null;
			}
		} catch (Exception e) {
			throw new Exception(e);
		}
	}

	@Override
	public List<Permissao> listar(PerfilUsuario perfil) throws Exception {
		String consulta = "SELECT p FROM Permissao p "
				+ "WHERE p.perfilUsuario = :perfil ";
		TypedQuery<Permissao> query = em.createQuery(consulta, Permissao.class);
		query.setParameter("perfil", perfil);
		try {
			return query.getResultList();
		} catch (Exception e) {
			throw new Exception(e);
		}
	}

	@Override
	public Permissao buscarPorPerfilERegra(PerfilUsuario perfil, Regra regra) throws Exception {
		String consulta = "SELECT p FROM Permissao p "
				+ "WHERE p.perfilUsuario = :perfil "
				+ "AND p.regra = :regra";
		TypedQuery<Permissao> query = em.createQuery(consulta, Permissao.class);
		query.setParameter("perfil", perfil);
		query.setParameter("regra", regra);
		try {
			return query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		} catch (Exception e) {
			throw new Exception(e);
		}
	}

	@Override
	public List<Regra> listarRegrasPorPerfil(PerfilUsuario perfil) throws Exception {
		String consulta = "SELECT p.regra FROM Permissao p "
				+ "WHERE p.perfilUsuario = :perfil";
		TypedQuery<Regra> query = em.createQuery(consulta, Regra.class);
		query.setParameter("perfil", perfil);
		try {
			return query.getResultList();
		} catch (NoResultException e){
			return null;
		} catch (Exception e) {
			throw new Exception(e);
		}
	}
}
