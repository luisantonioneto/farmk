package br.com.farmk.persistencia;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import br.com.farmk.entidade.Cidade;
import br.com.farmk.entidade.Estado;

@Repository
public class CidadeDaoJpa extends PersistenciaDaoJpa<Cidade> implements CidadeDao {
	
	@PersistenceContext
	private EntityManager em;
	
	@Override
	public List<Cidade> listar(Estado estado) throws Exception {
		String consulta = "SELECT c FROM Cidade c "
				+ "WHERE c.estado = :estado "
				+ "ORDER BY c.nome";
		TypedQuery<Cidade> query = em.createQuery(consulta, Cidade.class);
		query.setParameter("estado", estado);
		try {
			return query.getResultList();
		} catch (Exception e) {
			return null;
		}
	}
	
	@Override
	public Cidade buscarPorNome(String nome) throws Exception {
		String consulta = "SELECT c FROM Cidade c "
				+ "WHERE c.nome LIKE :nome";
		TypedQuery<Cidade> query = em.createQuery(consulta, Cidade.class);
		query.setParameter("nome", nome);
		try {
			return query.getSingleResult();
		} catch (Exception e) {
			return null;			
		}
	}

	@Override
	public Cidade buscarPorObjeto(Cidade cidade) throws Exception {
		try {
			if (cidade != null){
				if (em == null) {
					em = getEntityManager();
				}
				return em.find(Cidade.class, cidade.getCodCidade());	
			} else {
				return null;
			}
		} catch (Exception e) {
			throw new Exception(e);
		}
	}
}
