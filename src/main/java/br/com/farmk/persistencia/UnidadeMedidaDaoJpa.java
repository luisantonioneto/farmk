package br.com.farmk.persistencia;

import org.springframework.stereotype.Repository;

import br.com.farmk.entidade.UnidadeMedida;

@Repository
public class UnidadeMedidaDaoJpa extends PersistenciaDaoJpa<UnidadeMedida> implements UnidadeMedidaDao {

}
