package br.com.farmk.persistencia;

import java.util.List;

import br.com.farmk.entidade.Pessoa;

public interface PessoaDao extends PersistenciaDao<Pessoa> {

	public Pessoa buscarPorCpf(Pessoa pessoa);
	
	public List<Pessoa> listar(String nome) throws Exception;
}
