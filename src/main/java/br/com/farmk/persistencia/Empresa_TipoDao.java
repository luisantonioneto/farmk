package br.com.farmk.persistencia;

import java.util.List;

import br.com.farmk.entidade.Empresa;
import br.com.farmk.entidade.Empresa_Tipo;
import br.com.farmk.entidade.TipoEmpresa;

public interface Empresa_TipoDao extends PersistenciaDao<Empresa_Tipo>{

	public List<Empresa_Tipo> listarPorEmpresa(Empresa empresa) throws Exception;
	
	public Empresa_Tipo buscar(Empresa empresa, TipoEmpresa tipoEmpresa) throws Exception;
}
