package br.com.farmk.persistencia;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import br.com.farmk.entidade.Orcamento;
import br.com.farmk.entidade.Orcamento_Pessoa;
import br.com.farmk.entidade.Pessoa;

@Repository
public class Orcamento_PessoaDaoJpa extends PersistenciaDaoJpa<Orcamento_Pessoa> implements Orcamento_PessoaDao {
	
	@PersistenceContext
	private EntityManager em;
	
	@Override
	public List<Orcamento_Pessoa> listarPorOrcamento(Orcamento orcamento) throws Exception {
		if (orcamento != null) {
			String consulta = "SELECT op FROM Orcamento_Pessoa op "
					+ "WHERE op.orcamento = :orcamento";
			TypedQuery<Orcamento_Pessoa> query = em.createQuery(consulta, Orcamento_Pessoa.class);
			query.setParameter("orcamento", orcamento);
			try {
				return query.getResultList();
			} catch (Exception e) {
				throw new Exception(e);
			}
		}
		return null;
	}	

	@Override
	public Orcamento_Pessoa buscar(Orcamento orcamento, Pessoa responsavel) throws Exception {
		if (orcamento != null && responsavel != null) {
			String consulta = "SELECT op FROM Orcamento_Pessoa op "
					+ "WHERE op.orcamento = :orcamento "
					+ "AND op.responsavel = :responsavel";
			TypedQuery<Orcamento_Pessoa> query = em.createQuery(consulta, Orcamento_Pessoa.class);
			query.setParameter("orcamento", orcamento);
			query.setParameter("responsavel", responsavel);
			try {
				return query.getSingleResult();
			} catch (NoResultException e) {
				return null;
			} catch (Exception e){
				throw new Exception(e);
			}
		}
		return null;
	}
}