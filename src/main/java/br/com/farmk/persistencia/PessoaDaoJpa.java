package br.com.farmk.persistencia;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import br.com.farmk.entidade.Pessoa;

@Repository
public class PessoaDaoJpa extends PersistenciaDaoJpa<Pessoa> implements PessoaDao {
	
	@PersistenceContext
	private EntityManager em;

	@Override
	@Transactional
	public Pessoa salvar(Pessoa pessoa) throws Exception {
		Pessoa pessoaAIncluir = new Pessoa();
		try {
			pessoaAIncluir = buscarPorCpf(pessoa);
			if((pessoaAIncluir == null) &&
					(pessoa.getCodPessoa() == 0)){
				em.persist(pessoa);
			} else{
				em.merge(pessoa);
			}
			return pessoa;
		} catch (Exception e) {
			throw new Exception(e);
		}
 	}

	@Override
	@SuppressWarnings("unchecked")	
	public List<Pessoa> listar() {
		try {
			return em.createQuery("SELECT p FROM Pessoa p ORDER BY p.nome").getResultList();			
		} catch (Exception e) {
			return null;
		}
	}
		
	@Override
	public List<Pessoa> listar(String nome) throws Exception{
		if(nome != null){
			try {
				String consulta = "SELECT p FROM Pessoa p "
						+ "WHERE UPPER(p.nome) LIKE :nome "
						+ "ORDER BY p.nome";
				TypedQuery<Pessoa> query = em.createQuery(consulta, Pessoa.class);
				query.setParameter("nome", nome.toUpperCase() + '%');
				return query.getResultList();
			} catch (Exception e) {
				throw new Exception(e);
			}
		} else {
			return null;	
		}
	}
	
	@Override	
	public Pessoa buscarPorCpf(Pessoa pessoa){
		if (!(pessoa.getCpf() == null)){
			String consulta = "SELECT p FROM Pessoa p "
					+ "WHERE p.cpf = :cpf "
					+ "AND p.codPessoa <> :codPessoa";
			TypedQuery<Pessoa> query = em.createQuery(consulta, Pessoa.class);
			query.setParameter("cpf", pessoa.getCpf());
			query.setParameter("codPessoa", pessoa.getCodPessoa());
			try {
				return query.getSingleResult();
			} catch (Exception e) {
				return null;
			}
		} else {
			return null;
		}
	}
}
