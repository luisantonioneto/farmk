package br.com.farmk.entidade;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="tipoarea")
public class Tecnologia implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="codtipoarea")
	private int codTecnologia;
	private String nome;
	
	@OneToOne
	@JoinColumn(name="codunidademedida")
	private UnidadeMedida unidadeMedida;

	public int getCodTecnologia() {
		return codTecnologia;
	}

	public void setCodTecnologia(int codTecnologia) {
		this.codTecnologia = codTecnologia;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public UnidadeMedida getUnidadeMedida() {
		return unidadeMedida;
	}

	public void setUnidadeMedida(UnidadeMedida unidadeMedida) {
		this.unidadeMedida = unidadeMedida;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + codTecnologia;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tecnologia other = (Tecnologia) obj;
		if (codTecnologia != other.codTecnologia)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Tecnologia [codTecnologia=" + codTecnologia + ", nome=" + nome + ", unidadeMedida=" + unidadeMedida + "]";
	}
}
