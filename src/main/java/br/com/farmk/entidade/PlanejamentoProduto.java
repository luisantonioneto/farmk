package br.com.farmk.entidade;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="planejamento_produto",
	uniqueConstraints=
		@UniqueConstraint(columnNames={"codplanejamento", "codproduto"})
		)
public class PlanejamentoProduto implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private int codigo;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="codplanejamento")
	private Planejamento planejamento;
	
	@ManyToOne
	@JoinColumn(name="codproduto")
	private Produto produto;
	
	private BigDecimal area;
	
	private BigDecimal quantidade;

	@ManyToOne
	@JoinColumn(name="codmoeda")
	private Moeda moeda;
	
	@OneToMany(mappedBy="planejamentoProduto", cascade=CascadeType.ALL)
	@JsonIgnore
	private List<AplicacaoTecnologia> aplicacaoesTecnologia;
	
	private BigDecimal preco;
	
	private String observacao;
	
	@Transient
	private BigDecimal qtdTotal;
	
	@Transient
	private BigDecimal custoPorArea;
	
	@Transient
	private BigDecimal custoTotal;
	
	@Transient
	private Moeda moedaCotacao;
	
	@Transient 
	private BigDecimal precoMoedaCotacao;
	
	@Transient
	private BigDecimal custoTotalMoedaCotacao;
	
	@Transient
	private BigDecimal areaAplicacao;
	
	/*
	 * gets e sets
	 */

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public Planejamento getPlanejamento() {
		return planejamento;
	}

	public void setPlanejamento(Planejamento planejamento) {
		this.planejamento = planejamento;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public BigDecimal getArea() {
		return area;
	}

	public void setArea(BigDecimal area) {
		this.area = area;
	}

	public BigDecimal getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(BigDecimal quantidade) {
		this.quantidade = quantidade;
	}
	
	public Moeda getMoeda() {
		return moeda;
	}

	public void setMoeda(Moeda moeda) {
		this.moeda = moeda;
	}

	public BigDecimal getPreco() {
		return preco;
	}

	public void setPreco(BigDecimal preco) {
		this.preco = preco;
	}

	public BigDecimal getQtdTotal() {
		return qtdTotal;
	}

	public void setQtdTotal(BigDecimal qtdTotal) {
		this.qtdTotal = qtdTotal;
	}

	public BigDecimal getCustoPorArea() {
		return custoPorArea;
	}

	public void setCustoPorArea(BigDecimal custoPorArea) {
		this.custoPorArea = custoPorArea;
	}

	public BigDecimal getCustoTotal() {
		return custoTotal;
	}

	public void setCustoTotal(BigDecimal custoTotal) {
		this.custoTotal = custoTotal;
	}

	public BigDecimal getPrecoMoedaCotacao() {
		return precoMoedaCotacao;
	}

	public void setPrecoMoedaCotacao(BigDecimal precoMoedaCotacao) {
		this.precoMoedaCotacao = precoMoedaCotacao;
	}

	public BigDecimal getCustoTotalMoedaCotacao() {
		return custoTotalMoedaCotacao;
	}

	public void setCustoTotalMoedaCotacao(BigDecimal custoTotalMoedaCotacao) {
		this.custoTotalMoedaCotacao = custoTotalMoedaCotacao;
	}
	
	public Moeda getMoedaCotacao() {
		return moedaCotacao;
	}

	public void setMoedaCotacao(Moeda moedaCotacao) {
		this.moedaCotacao = moedaCotacao;
	}
	
	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	
	public BigDecimal getAreaAplicacao() {
		return areaAplicacao;
	}

	public void setAreaAplicacao(BigDecimal areaAplicacao) {
		this.areaAplicacao = areaAplicacao;
	}

	public List<AplicacaoTecnologia> getAplicacaoesTecnologia() {
		return aplicacaoesTecnologia;
	}

	public void setAplicacaoesTecnologia(List<AplicacaoTecnologia> aplicacaoesTecnologia) {
		this.aplicacaoesTecnologia = aplicacaoesTecnologia;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + codigo;
		result = prime * result + ((produto == null) ? 0 : produto.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlanejamentoProduto other = (PlanejamentoProduto) obj;
		if (codigo != other.codigo)
			return false;
		if (produto == null) {
			if (other.produto != null)
				return false;
		} else if (!produto.equals(other.produto))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "PlanejamentoProduto [codigo=" + codigo + ", planejamento=" + planejamento + ", produto=" + produto
				+ ", area=" + area + ", quantidade=" + quantidade + ", moeda=" + moeda + ", aplicacaoesTecnologia="
				+ aplicacaoesTecnologia + ", preco=" + preco + ", observacao=" + observacao + ", qtdTotal=" + qtdTotal
				+ ", custoPorArea=" + custoPorArea + ", custoTotal=" + custoTotal + ", moedaCotacao=" + moedaCotacao
				+ ", precoMoedaCotacao=" + precoMoedaCotacao + ", custoTotalMoedaCotacao=" + custoTotalMoedaCotacao
				+ ", areaAplicacao=" + areaAplicacao + "]";
	}
	
}