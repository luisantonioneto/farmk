package br.com.farmk.entidade;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name="produto_uso",
	uniqueConstraints=
		@UniqueConstraint(columnNames={"codproduto", "codusoproduto"})
)
public class Produto_Uso implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private int codigo;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="codproduto", nullable=false)	
	private Produto produto;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="codusoproduto", nullable=false)
	private UsoProduto usoProduto;


	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public UsoProduto getUsoProduto() {
		return usoProduto;
	}

	public void setUsoProduto(UsoProduto usoProduto) {
		this.usoProduto = usoProduto;
	}	
}