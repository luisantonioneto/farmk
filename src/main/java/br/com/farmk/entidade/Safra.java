package br.com.farmk.entidade;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="safra")
public class Safra implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="codsafra")
	private int codSafra;
	private String nome;
	@Column(name="datainicial")
	private Date dataInicial;
	@Column(name="datafinal")
	private Date dataFinal;
	public int getCodSafra() {
		return codSafra;
	}
	public void setCodSafra(int codSafra) {
		this.codSafra = codSafra;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Date getDataInicial() {
		return dataInicial;
	}
	public void setDataInicial(Date dataInicial) {
		this.dataInicial = dataInicial;
	}
	public Date getDataFinal() {
		return dataFinal;
	}
	public void setDataFinal(Date dataFinal) {
		this.dataFinal = dataFinal;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + codSafra;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Safra other = (Safra) obj;
		if (codSafra != other.codSafra)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Safra [codSafra=" + codSafra + ", nome=" + nome + ", dataInicial=" + dataInicial + ", dataFinal="
				+ dataFinal + "]";
	}
}