package br.com.farmk.entidade;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="orcamento")
public class Orcamento implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="codorcamento")
	private int codOrcamento;
	
	@Column(name="datacricacao")
	private Date dataCriacao;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="codsafra")
	private Safra safra;	
	
	@ManyToOne
	@JoinColumn(name="codmoeda")
	private Moeda moeda;
	
	@Column(name="cotacaomoeda")
	private BigDecimal cotacaoMoeda;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="codcliente")
	private Empresa cliente;
	
	@OneToMany(mappedBy="orcamento",fetch=FetchType.EAGER)
	private List<Planejamento> planejamentos;
	
	/*
	 * gets e sets
	 */
	public void addPlanejamento(Planejamento planejamento){
		planejamentos.add(planejamento);
	}
	
	public int getCodOrcamento() {
		return codOrcamento;
	}
	public void setCodOrcamento(int codOrcamento) {
		this.codOrcamento = codOrcamento;
	}
	public Date getDataCriacao() {
		return dataCriacao;
	}
	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}
	public Empresa getCliente() {
		return cliente;
	}
	public void setCliente(Empresa cliente) {
		this.cliente = cliente;
	}
	public Moeda getMoeda() {
		return moeda;
	}
	public void setMoeda(Moeda moeda) {
		this.moeda = moeda;
	}
	public BigDecimal getCotacaoMoeda() {
		return cotacaoMoeda;
	}
	public void setCotacaoMoeda(BigDecimal cotacaoMoeda) {
		this.cotacaoMoeda = cotacaoMoeda;
	}
	public List<Planejamento> getPlanejamentos() {
		return planejamentos;
	}
	public void setPlanejamentos(List<Planejamento> planejamentos) {
		this.planejamentos = planejamentos;
	}
	public Safra getSafra() {
		return safra;
	}
	public void setSafra(Safra safra) {
		this.safra = safra;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + codOrcamento;
		result = prime * result + ((planejamentos == null) ? 0 : planejamentos.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Orcamento other = (Orcamento) obj;
		if (codOrcamento != other.codOrcamento)
			return false;
		if (planejamentos == null) {
			if (other.planejamentos != null)
				return false;
		} else if (!planejamentos.equals(other.planejamentos))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Orcamento [codOrcamento=" + codOrcamento + ", dataCriacao=" + dataCriacao + ", safra=" + safra
				+ ", moeda=" + moeda + ", cotacaoMoeda=" + cotacaoMoeda + ", cliente=" + cliente + ", planejamentos="
				+ planejamentos + "]";
	}
}