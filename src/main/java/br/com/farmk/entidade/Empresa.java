package br.com.farmk.entidade;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Empresa implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="codempresa")
	private int codEmpresa;
	@Column(name="nomefantasia")
	private String nomeFantasia;
	@Column(name="razaosocial")
	private String razaoSocial;
	@Column(length=14)
	private String cnpj;
	@Column(name="telefonecomercial")
	private String telefoneComercial;
	@Column(name="telefonecelular")
	private String telefoneCelular;
	private String email;
	private String observacao;
	private boolean ativo;

	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="codendereco")
	private Endereco endereco;
	
	@OneToMany(mappedBy="empresaSuperior")
	private List<Empresa> empresas;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="codempresasuperior")
	private Empresa empresaSuperior;
	
	// gets e sets

	public int getCodEmpresa() {
		return codEmpresa;
	}

	public List<Empresa> getEmpresas() {
		return empresas;
	}


	public void setCodEmpresa(int codEmpresa) {
		this.codEmpresa = codEmpresa;
	}

	public String getNomeFantasia() {
		return nomeFantasia;
	}

	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}
	
	public Empresa getEmpresaSuperior() {
		return empresaSuperior;
	}

	public void setEmpresaSuperior(Empresa empresaSuperior) {
		this.empresaSuperior = empresaSuperior;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public void setEmpresas(List<Empresa> empresas) {
		this.empresas = empresas;
	}
	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public String getTelefoneComercial() {
		return telefoneComercial;
	}
	public void setTelefoneComercial(String telefoneComercial) {
		this.telefoneComercial = telefoneComercial;
	}
	public String getTelefoneCelular() {
		return telefoneCelular;
	}
	public void setTelefoneCelular(String telefoneCelular) {
		this.telefoneCelular = telefoneCelular;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}	
	
	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + codEmpresa;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Empresa other = (Empresa) obj;
		if (codEmpresa != other.codEmpresa)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Empresa [codEmpresa=" + codEmpresa + ", nomeFantasia=" + nomeFantasia + ", razaoSocial=" + razaoSocial
				+ ", cnpj=" + cnpj + ", telefoneComercial=" + telefoneComercial + ", telefoneCelular=" + telefoneCelular
				+ ", email=" + email + ", observacao=" + observacao + ", ativo=" + ativo + ", endereco=" + endereco
				+ ", empresas=" + empresas + ", empresaSuperior=" + empresaSuperior + "]";
	}	
}