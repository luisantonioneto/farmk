package br.com.farmk.entidade;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="unidademedida")
public class UnidadeMedida implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="codunidademedida")
	private int codUnidadeMedida;
	private String nome;
	private String sigla;
	private BigDecimal peso;
	
	public int getCodUnidadeMedida() {
		return codUnidadeMedida;
	}
	public void setCodUnidadeMedida(int codUnidadeMedida) {
		this.codUnidadeMedida = codUnidadeMedida;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getSigla() {
		return sigla;
	}
	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
	public BigDecimal getPeso() {
		return peso;
	}
	public void setPeso(BigDecimal peso) {
		this.peso = peso;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + codUnidadeMedida;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UnidadeMedida other = (UnidadeMedida) obj;
		if (codUnidadeMedida != other.codUnidadeMedida)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "UnidadeMedida [codUnidadeMedida=" + codUnidadeMedida + ", nome=" + nome + ", sigla=" + sigla + ", peso="
				+ peso + "]";
	}
}