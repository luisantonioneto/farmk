package br.com.farmk.entidade;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Moeda implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="codmoeda")
	private int codMoeda;
	
	private String nome;
	
	private String simbolo;

	public int getCodMoeda() {
		return codMoeda;
	}

	public void setCodMoeda(int codMoeda) {
		this.codMoeda = codMoeda;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSimbolo() {
		return simbolo;
	}

	public void setSimbolo(String simbolo) {
		this.simbolo = simbolo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + codMoeda;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Moeda other = (Moeda) obj;
		if (codMoeda != other.codMoeda)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Moeda [codMoeda=" + codMoeda + ", nome=" + nome + ", simbolo=" + simbolo + "]";
	}
}