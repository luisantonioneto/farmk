package br.com.farmk.entidade;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(uniqueConstraints=
		@UniqueConstraint(columnNames={"codorcamento", "codcultura"})
)
public class Planejamento implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="codplanejamento")
	private int codPlanejamento;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="codcultura")
	private Produto cultura;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="codorcamento")
	private Orcamento orcamento;
	
	@OneToMany(mappedBy="planejamento", fetch=FetchType.LAZY)
	private List<PlanejamentoProduto> planejamentoProdutos;
	
	@ManyToOne
	@JoinColumn(name="codmoedaprecocultura")
	private Moeda moedaPrecoCultura;
	
	@Column(name="precocultura")
	private BigDecimal precoCultura;
	
	@ManyToOne
	@JoinColumn(name="codmoedaoutroscustos")
	private Moeda moedaOutrosCustos;
	
	@Column(name="outroscustos")
	private BigDecimal outrosCustos;
	
	@Column(name="produtividadeesperada")
	private BigDecimal produtividadeEsperada;
	
	/*
	 * gets e sets
	 */

	public int getCodPlanejamento() {
		return codPlanejamento;
	}

	public void setCodPlanejamento(int codPlanejamento) {
		this.codPlanejamento = codPlanejamento;
	}

	public Produto getCultura() {
		return cultura;
	}

	public void setCultura(Produto cultura) {
		this.cultura = cultura;
	}

	public Orcamento getOrcamento() {
		return orcamento;
	}

	public void setOrcamento(Orcamento orcamento) {
		this.orcamento = orcamento;
	}
	
	public Moeda getMoedaPrecoCultura() {
		return moedaPrecoCultura;
	}

	public void setMoedaPrecoCultura(Moeda moedaPrecoCultura) {
		this.moedaPrecoCultura = moedaPrecoCultura;
	}

	public BigDecimal getPrecoCultura() {
		return precoCultura;
	}

	public void setPrecoCultura(BigDecimal precoCultura) {
		this.precoCultura = precoCultura;
	}

	public Moeda getMoedaOutrosCustos() {
		return moedaOutrosCustos;
	}

	public void setMoedaOutrosCustos(Moeda moedaOutrosCustos) {
		this.moedaOutrosCustos = moedaOutrosCustos;
	}

	public BigDecimal getOutrosCustos() {
		return outrosCustos;
	}

	public void setOutrosCustos(BigDecimal outrosCustos) {
		this.outrosCustos = outrosCustos;
	}

	public BigDecimal getProdutividadeEsperada() {
		return produtividadeEsperada;
	}

	public void setProdutividadeEsperada(BigDecimal produtividadeEsperada) {
		this.produtividadeEsperada = produtividadeEsperada;
	}

	public List<PlanejamentoProduto> getPlanejamentoProdutos() {
		return planejamentoProdutos;
	}

	public void setPlanejamentoProdutos(List<PlanejamentoProduto> planejamentoProdutos) {
		this.planejamentoProdutos = planejamentoProdutos;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + codPlanejamento;
		result = prime * result + ((orcamento == null) ? 0 : orcamento.hashCode());
		result = prime * result + ((planejamentoProdutos == null) ? 0 : planejamentoProdutos.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Planejamento other = (Planejamento) obj;
		if (codPlanejamento != other.codPlanejamento)
			return false;
		if (orcamento == null) {
			if (other.orcamento != null)
				return false;
		} else if (!orcamento.equals(other.orcamento))
			return false;
		if (planejamentoProdutos == null) {
			if (other.planejamentoProdutos != null)
				return false;
		} else if (!planejamentoProdutos.equals(other.planejamentoProdutos))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Planejamento [codPlanejamento=" + codPlanejamento + ", cultura=" + cultura + ", orcamento=" + orcamento
				+ ", planejamentoProdutos=" + planejamentoProdutos + ", moedaPrecoCultura=" + moedaPrecoCultura
				+ ", precoCultura=" + precoCultura + ", moedaOutrosCustos=" + moedaOutrosCustos + ", outrosCustos="
				+ outrosCustos + ", produtividadeEsperada=" + produtividadeEsperada + "]";
	}
}