package br.com.farmk.entidade;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ramoatividade")
public class RamoAtividade implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="codramoatividade")
	private int codRamoAtividade;
	
	private String nome;

	public int getCodRamoAtividade() {
		return codRamoAtividade;
	}

	public void setCodRamoAtividade(int codRamoAtividade) {
		this.codRamoAtividade = codRamoAtividade;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + codRamoAtividade;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RamoAtividade other = (RamoAtividade) obj;
		if (codRamoAtividade != other.codRamoAtividade)
			return false;
		return true;
	}
}
