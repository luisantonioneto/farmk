package br.com.farmk.entidade;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="produto")
public class Produto implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="codproduto")
	private int codProduto;
	private String nome;	
	
	@Column(name="principioativo")
	private String principioAtivo;
	
	@OneToOne
	@JoinColumn(name="unidademedida")
	private UnidadeMedida unidadeMedida;
	
	@OneToMany(mappedBy="produtoSuperior")
	private List<Produto> produtos;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="codprodutosuperior")
	private Produto produtoSuperior;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="codfabricante")
	private Empresa fabricante;
	
	@OneToMany(mappedBy="produto", fetch=FetchType.EAGER)
	private List<Produto_Uso> produto_usos;
	
	@OneToOne
	@JoinColumn(name="codtipoarea")
	private Tecnologia tecnologia;	
	
	@Transient
	private UsoProduto categoria;	

	public int getCodProduto() {
		return codProduto;
	}

	public void setCodProduto(int codProduto) {
		this.codProduto = codProduto;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public UnidadeMedida getUnidadeMedida() {
		return unidadeMedida;
	}

	public void setUnidadeMedida(UnidadeMedida unidadeMedida) {
		this.unidadeMedida = unidadeMedida;
	}

	public List<Produto> getProdutos() {
		return produtos;
	}

	public void setProdutos(List<Produto> produtos) {
		this.produtos = produtos;
	}

	public Produto getProdutoSuperior() {
		return produtoSuperior;
	}

	public void setProdutoSuperior(Produto produtoSuperior) {
		this.produtoSuperior = produtoSuperior;
	}

	public Empresa getFabricante() {
		return fabricante;
	}

	public void setFabricante(Empresa fabricante) {
		this.fabricante = fabricante;
	}

	public List<Produto_Uso> getProduto_usos() {
		return produto_usos;
	}

	public void setProduto_usos(List<Produto_Uso> produto_usos) {
		this.produto_usos = produto_usos;
	}

	public UsoProduto getCategoria() {
		return categoria;
	}

	public void setCategoria(UsoProduto categoria) {
		this.categoria = categoria;
	}

	public Tecnologia getTecnologia() {
		return tecnologia;
	}

	public void setTecnologia(Tecnologia tecnologia) {
		this.tecnologia = tecnologia;
	}

	public String getPrincipioAtivo() {
		return principioAtivo;
	}

	public void setPrincipioAtivo(String principioAtivo) {
		this.principioAtivo = principioAtivo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + codProduto;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Produto other = (Produto) obj;
		if (codProduto != other.codProduto)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Produto [codProduto=" + codProduto + ", nome=" + nome + ", principioAtivo=" + principioAtivo
				+ ", unidadeMedida=" + unidadeMedida + ", produtos=" + produtos + ", fabricante=" + fabricante 
				+ ", produto_usos=" + produto_usos + ", tecnologia=" + tecnologia + ", categoria=" + categoria + "]";
	}
}
