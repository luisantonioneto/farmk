package br.com.farmk.entidade;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="usoproduto")
public class UsoProduto implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="codusoproduto")
	private int codUsoProduto;
	private String nome;
	@Column(name="ehcategoria")
	private boolean ehCategoria;
	
	public int getCodUsoProduto() {
		return codUsoProduto;
	}
	public void setCodUso(int codUso) {
		this.codUsoProduto = codUso;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public boolean isEhCategoria() {
		return ehCategoria;
	}
	public void setEhCategoria(boolean ehCategoria) {
		this.ehCategoria = ehCategoria;
	}
	public void setCodUsoProduto(int codUsoProduto) {
		this.codUsoProduto = codUsoProduto;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + codUsoProduto;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UsoProduto other = (UsoProduto) obj;
		if (codUsoProduto != other.codUsoProduto)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "UsoProduto [codUsoProduto=" + codUsoProduto + ", nome=" + nome + ", ehCategoria=" + ehCategoria + "]";
	}
}
