package br.com.farmk.entidade;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(uniqueConstraints=
		@UniqueConstraint(columnNames={"codperfilusuario", "codregra"}))
public class Permissao implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="codpermissao")
	private int codPermissao;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="codperfilusuario")
	private PerfilUsuario perfilUsuario;

	@ManyToOne
	@JoinColumn(name="codregra")
	private Regra regra;
	
	/*
	 * gets e sets
	 */

	public int getCodPermissao() {
		return codPermissao;
	}

	public void setCodPermissao(int codPermissao) {
		this.codPermissao = codPermissao;
	}

	public PerfilUsuario getPerfilUsuario() {
		return perfilUsuario;
	}

	public void setPerfilUsuario(PerfilUsuario perfilUsuario) {
		this.perfilUsuario = perfilUsuario;
	}

	public Regra getRegra() {
		return regra;
	}

	public void setRegra(Regra regra) {
		this.regra = regra;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + codPermissao;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Permissao other = (Permissao) obj;
		if (codPermissao != other.codPermissao)
			return false;
		return true;
	}
}
