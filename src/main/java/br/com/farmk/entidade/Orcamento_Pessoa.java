package br.com.farmk.entidade;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name="orcamento_pessoa",
	uniqueConstraints=
		@UniqueConstraint(columnNames={"codorcamento", "codresponsavel"})
)
public class Orcamento_Pessoa implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="codorcamento_pessoa")
	private int codOrcamento_Pessoa;
	
	@ManyToOne
	@JoinColumn(name="codorcamento", nullable=false)
	private Orcamento orcamento;
	
	@ManyToOne
	@JoinColumn(name="codresponsavel")
	private Pessoa responsavel;

	public int getCodOrcamento_Pessoa() {
		return codOrcamento_Pessoa;
	}

	public void setCodOrcamento_Pessoa(int codOrcamento_Pessoa) {
		this.codOrcamento_Pessoa = codOrcamento_Pessoa;
	}

	public Orcamento getOrcamento() {
		return orcamento;
	}

	public void setOrcamento(Orcamento orcamento) {
		this.orcamento = orcamento;
	}

	public Pessoa getResponsavel() {
		return responsavel;
	}

	public void setResponsavel(Pessoa responsavel) {
		this.responsavel = responsavel;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + codOrcamento_Pessoa;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Orcamento_Pessoa other = (Orcamento_Pessoa) obj;
		if (codOrcamento_Pessoa != other.codOrcamento_Pessoa)
			return false;
		return true;
	}
}