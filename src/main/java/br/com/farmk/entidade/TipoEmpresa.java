package br.com.farmk.entidade;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tipoempresa")
public class TipoEmpresa implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="codtipoempresa")
	private int codTipoEmpresa;
	private String nome;
		
	public int getCodTipoEmpresa() {
		return codTipoEmpresa;
	}
	public void setCodTipoEmpresa(int codTipoEmpresa) {
		this.codTipoEmpresa = codTipoEmpresa;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + codTipoEmpresa;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoEmpresa other = (TipoEmpresa) obj;
		if (codTipoEmpresa != other.codTipoEmpresa)
			return false;
		return true;
	}
	
}
