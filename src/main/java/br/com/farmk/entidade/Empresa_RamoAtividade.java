package br.com.farmk.entidade;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name="empresa_ramoatividade",
uniqueConstraints=
	@UniqueConstraint(columnNames={"codramoatividade", "codempresa"})
)
public class Empresa_RamoAtividade implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private int codigo;

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="codramoatividade", nullable=false)
	private RamoAtividade ramoAtividade;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="codempresa", nullable=false)
	private Empresa empresa;

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public RamoAtividade getRamoAtividade() {
		return ramoAtividade;
	}

	public void setRamoAtividade(RamoAtividade ramoAtividade) {
		this.ramoAtividade = ramoAtividade;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + codigo;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Empresa_RamoAtividade other = (Empresa_RamoAtividade) obj;
		if (codigo != other.codigo)
			return false;
		return true;
	}
}
