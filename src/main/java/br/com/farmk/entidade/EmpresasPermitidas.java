package br.com.farmk.entidade;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name="empresaspermitidas", 
		uniqueConstraints=
			@UniqueConstraint(columnNames={"codusuario", "codempresa"}))
public class EmpresasPermitidas implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="codempresaspermitidas")
	private int codEmpresasPermitidas;
	
	@ManyToOne
	@JoinColumn(name="codusuario")
	private Usuario usuario;
	
	@ManyToOne
	@JoinColumn(name="codempresa")
	private Empresa empresa;

	public int getCodEmpresasPermitidas() {
		return codEmpresasPermitidas;
	}

	public void setCodEmpresasPermitidas(int codEmpresasPermitidas) {
		this.codEmpresasPermitidas = codEmpresasPermitidas;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + codEmpresasPermitidas;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmpresasPermitidas other = (EmpresasPermitidas) obj;
		if (codEmpresasPermitidas != other.codEmpresasPermitidas)
			return false;
		return true;
	}
}
