package br.com.farmk.entidade;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="perfilusuario")
public class PerfilUsuario implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="codperfilusuario")
	private int codPerfilUsuario;

	private String nome;
	
	@OneToMany(mappedBy="perfilUsuario", fetch=FetchType.EAGER)
	private List<Permissao> permissoes;
	
	/*
	 * gets e sets
	 */
	
	public int getCodPerfilUsuario() {
		return codPerfilUsuario;
	}

	public void setCodPerfilUsuario(int codPerfilUsuario) {
		this.codPerfilUsuario = codPerfilUsuario;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Permissao> getPermissoes() {
		return permissoes;
	}

	public void setPermissoes(List<Permissao> permissoes) {
		this.permissoes = permissoes;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + codPerfilUsuario;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PerfilUsuario other = (PerfilUsuario) obj;
		if (codPerfilUsuario != other.codPerfilUsuario)
			return false;
		return true;
	}
}