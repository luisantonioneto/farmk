function handleLoginRequest(xhr, status, args) {
    if(args.validationFailed || !args.loggedIn) {
    	$('login.jsf').fadeOut();
    }
    else {
        $('index.jsf').fadeOut();
    }
}