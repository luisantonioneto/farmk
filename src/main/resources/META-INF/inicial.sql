-- Inserir Regra Administrador
INSERT INTO regra (codregra, nome, nomeregra) VALUES (1, 'Administrador', 'ROLE_ADMINISTRADOR');
INSERT INTO regra (nome, nomeregra) VALUES ('Cadastro', 'ROLE_CADASTRO');
INSERT INTO regra (nome, nomeregra) VALUES ('Empresa', 'ROLE_EMPRESA');
INSERT INTO regra (nome, nomeregra) VALUES ('Lavoura', 'ROLE_LAVOURA');
INSERT INTO regra (nome, nomeregra) VALUES ('Localidade', 'ROLE_LOCALIDADE');
INSERT INTO regra (nome, nomeregra) VALUES ('Pessoa', 'ROLE_PESSOA');
INSERT INTO regra (nome, nomeregra) VALUES ('Usuário', 'ROLE_USUARIO');

-- Inserir PerfilUsuario
INSERT INTO perfilusuario (codperfilusuario, nome) VALUES (1, 'Administrador');

-- Inserir Permissao
INSERT INTO permissao (codperfilusuario, codregra) VALUES (1, 1);

-- Inserir Pessoa 
INSERT INTO pessoa (codpessoa, nome) VALUES (1, 'Administrador');

-- Inserir Usuário
INSERT INTO usuario (codusuario, codpessoa, codperfil, login, ativo) VALUES (1, 1, 1, 'admin', true);

-- Inserir Uso Produto
INSERT INTO usoproduto (nome, ehcategoria) VALUES ('Cultura', false);
INSERT INTO usoproduto (nome, ehcategoria) VALUES ('Adubação de Solo', true);
INSERT INTO usoproduto (nome, ehcategoria) VALUES ('Adubação Foliar', true);
INSERT INTO usoproduto (nome, ehcategoria) VALUES ('Corretivos de Solo', true);
INSERT INTO usoproduto (nome, ehcategoria) VALUES ('Fungicidas', true);
INSERT INTO usoproduto (nome, ehcategoria) VALUES ('Herbicidas', true);
INSERT INTO usoproduto (nome, ehcategoria) VALUES ('Inseticidas', true);
INSERT INTO usoproduto (nome, ehcategoria) VALUES ('Sementes', true);
INSERT INTO usoproduto (nome, ehcategoria) VALUES ('Tratamento de Sementes', true);

-- Inserir Moeda
INSERT INTO moeda (nome, simbolo) VALUES ('Real', 'R$');
INSERT INTO moeda (nome, simbolo) VALUES ('Dólar', 'US$');

-- Inserir Unidade Medida
INSERT INTO unidademedida (codunidademedida, nome, sigla) VALUES (1, 'Hectare', 'ha');
INSERT INTO unidademedida (nome, sigla) VALUES ('Kilo', 'Kg');
INSERT INTO unidademedida (nome, sigla) VALUES ('Litro', 'Lt');
INSERT INTO unidademedida (nome, sigla, peso) VALUES ('Saca 20 Kg', 'sc', 20);
INSERT INTO unidademedida (nome, sigla, peso) VALUES ('Saca 40 Kg', 'sc', 40);

-- Inserir Tipo Area
INSERT INTO tipoarea (nome, codunidademedida) VALUES ('Plantio', 1);
INSERT INTO tipoarea (nome, codunidademedida) VALUES ('Convencional', 1);
INSERT INTO tipoarea (nome, codunidademedida) VALUES ('Total', 1);