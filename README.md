# README #

Primeiros passos para executar a aplicação Farmk.

### O que é necessário? ###

* IDE: Eclipse Mars 2
* Banco de Dados: PostgreSQL 9.6
* * Nome do banco de dados: farmk
* * Usuário: postgres
* * Senha: 123456
* Tomcat 8.0.37

### Instruções ###
1. Faça o clone do projeto
2. Crie o banco de dados com o nome "farmk
3. Execute o script que está em: /src/main/resources/META-INF/inicial.sql
4. Adicione o projeto no Tomcat e execute.

### Acessar o sistema ###
1. Acesse a URL: http://localhost:PORTA_DO_TOMCAT/farmk
2. Usuário: admin
3. Senha: 98765